const del = require('del');
const gulp = require('gulp');
const browserSync = require('browser-sync');
const runSequence = require('run-sequence');
const loadPlugins = require('gulp-load-plugins');

const $ = loadPlugins();
const server = browserSync.create();

// require('./src/config/server/environment/server');

gulp.task('img', () =>
  gulp.src('./src/web/resources/img/**/*')
    .pipe(gulp.dest('./public/assets/img')));

gulp.task('fonts', () =>
  gulp.src('./src/web/resources/fonts/*')
    .pipe(gulp.dest('./public/assets/fonts')));

gulp.task('css:concat', () => {
  return gulp.src([
    './public/assets/css/index81.css',
    './public/assets/css/new-menu.css',
    './public/assets/css/new-search.css',
    './public/assets/css/scrollable-articles.css',
  ])
  .pipe($.sourcemaps.init())
  .pipe($.concat('app.css'))
  .pipe($.sourcemaps.write('.'))
  .pipe(gulp.dest('./public/assets/css'));
});

gulp.task('css', () =>
  gulp.src('./src/web/resources/css/**/*.css')
    .pipe($.changed('./public/assets/css'))
    .pipe($.sourcemaps.init())
    .pipe($.minifier({
      minify: true,
      collapseWhitespace: true,
      minifyCSS: true
    }))
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('./public/assets/css')));

gulp.task('sass', () =>
  gulp.src('./src/web/resources/scss/*.scss')
    .pipe($.changed('./public/assets/css'))
    .pipe($.sourcemaps.init())
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: [
        'last 4 version',
        'ie 9',
        'ios 5',
      ]
    }))
    .pipe($.minifier({
      minify: true,
      collapseWhitespace: true,
      minifyCSS: true
    }))
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('./public/assets/css')));

gulp.task('sass:watch', () => {
  gulp.watch('./src/web/resources/scss/**/*.scss', [
    'sass',
  ]);
});

gulp.task('copy', () => {
  runSequence(
    'clean', [
      'css', 'img', 'fonts',
    ],
    'css:concat'
  );
});

gulp.task('clean', () => {
  del.sync([
    './public/assets/css',
    './public/assets/img',
    './public/assets/fonts',
    './public/assets/js',
  ]);
});

gulp.task('watch', () => {
  server.init({
    ui: false,
    port: parseInt(process.env.PORT, 10) + 1,
    open: false,
    notify: true,
    reloadOnRestart: true,
    proxy: `${process.env.APP_HOSTNAME}:${process.env.PORT}`,
    files: [
      './public/assets/css/**/*.css',
      './public/assets/js/*.js',
    ]
  });

  gulp.watch('./src/web/resources/scss/**/*.scss', [
    'sass',
  ]);
});

gulp.task('default', [
  'clean',
], () => {
  runSequence([
    'sass',
    'copy',
  ]);
});