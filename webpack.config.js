const path = require('path');
const webpack = require('webpack');

module.exports = {
  target: 'web',
  devtool: 'eval',
  entry: {
    app: path.resolve('src/web/client/index.jsx')
  },
  output: {
    pathinfo: true,
    filename: '[name].js',
    publicPath: '/assets/js/',
    path: path.resolve('public/assets/js')
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      }
    ]
  },
  resolve: {
    extensions: [
      '.js', '.json', '.jsx'
    ]
  }
};