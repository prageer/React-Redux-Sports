import path from 'path';
import Express from 'express';

import SSR from './ssr';


const app = Express();

app.use('/public', Express.static(path.resolve('public')));
app.use(Express.static(path.resolve('public')));

app.set('view engine', 'pug');
app.set('views', path.resolve('src/web/server/views'));

app.use(SSR);

app.listen(3000, () => {
  console.log('App listening on port 3000');
});