import Thunk from 'redux-thunk';
import { applyMiddleware, createStore as createReduxStore } from 'redux';

import { pugGenericView } from '../pug';
import vavelApp from '../../client/reducers';
import { api } from '../../client/services';
import { dehydrate } from '../../client/utils';
import { errorViewMiddleware } from '../../client/middlewares/error';

// Obtiene el objeto de traducción de acuerdo al subdominio.
export const getEdition = (subdomain) => {
  return require(`../../client/editions/${subdomain}`);
};

export const renderFullPage = (vavelAppHTML, preloadedState) => {
  return pugGenericView({
    root: vavelAppHTML,
    state: dehydrate(preloadedState)
  });
};

export const createStore = () => createReduxStore(
  vavelApp,
  applyMiddleware(Thunk.withExtraArgument(api), errorViewMiddleware)
);