import React from 'react';
import { Provider } from 'react-redux';
import { match, RouterContext  } from 'react-router';

import routes from '../../client/routes/index';
import { fetchData } from '../../client/utils';
import { DEFAULT_SUBDOMAIN, WITH_SUBDOMAIN } from '../../rules';
import { getEdition, renderFullPage, createStore } from './helpers';
import {
  setEdition,
  setEditionName,
  setLocation,
  errorApp
} from '../../client/reducers/appReducer';


// We must load `renderToString` method after 'cache-components' module.
if (process.env.NODE_ENV === 'production') require('./cache-components');
const { renderToString } = require('react-dom/server');

const renderApp = (store, props) => {
  return renderToString(
    <Provider store={store}>
      <RouterContext {...props} />
    </Provider>
  );
};

const sendAppToBrowser = (store, props, res) => {
  const preloadedState = store.getState();
  const vavelAppHTML = renderApp(store, props);

  if (store.hasError) {
    res.status(404);
  }

  res.end(renderFullPage(vavelAppHTML, preloadedState));
};
const sendErrorAppToBrowser = (req, res, store, err) => {
  req.url = WITH_SUBDOMAIN ? '/404' : '/es/404'; // TODO: concat default language.
  store.dispatch(errorApp(err));

  // monkey patch the store because we need a sync
  // way to get the 'state' to handle in server.
  store.hasError = true;

  handleSSR(req, res, store, false);
};


const handleSSR = (req, res, store = null, loadStore = true) => {
  if (loadStore) store = createStore();
  const [ , posibleSubdomain, ...restUrl ] = req.url.split('/');

  let subdomain = posibleSubdomain;
  let edition;

  try {
    edition = getEdition(subdomain);
  } catch (err) {
    subdomain = DEFAULT_SUBDOMAIN;
    edition = getEdition(subdomain);
    // TODO: Activar error en store.
  }

  if (WITH_SUBDOMAIN) {
    req.url = '/' + restUrl.join('/');
  }

  store.dispatch(setLocation(req.url));
  store.dispatch(setEdition(edition));
  store.dispatch(setEditionName(edition.cultureShortcut));

  match({ routes, location: req.url }, (err, redirect, props) => {
    if (err) {
      sendErrorAppToBrowser(req, res, store, err);
    } else if (redirect) {
      const pathnameWithSearch = redirect.pathname + redirect.search;
      res.end('Redirecting to ' + pathnameWithSearch);
    } else if (props) {
      const { components, params, location } = props;

      fetchData(store, components, params, location.query)
        .then(() => {
          try {
            sendAppToBrowser(store, props, res);
          } catch (err) {
            sendErrorAppToBrowser(req, res, store, err);
          }
        })
        .catch((err) => {
          sendErrorAppToBrowser(req, res, store, err);
        });
    } else {
      const notFound = new Error('Ruta no encontrada');
      sendErrorAppToBrowser(req, res, store, notFound)
    }
  });
};

export default handleSSR;