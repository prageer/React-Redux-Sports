import SSRCaching from 'electrode-react-ssr-caching';

import CacheRuleConfig from '../../rules/cache-components';


console.log('Cache enable, configuring electrode...');

SSRCaching.enableCaching();
SSRCaching.setCachingConfig(CacheRuleConfig);