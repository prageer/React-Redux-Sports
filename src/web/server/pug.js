import { resolve } from 'path';
import { compileFile } from 'pug';


export const pugGenericView = compileFile(
  resolve('src/web/server/views/generic.pug'),
  { cache: true }
);