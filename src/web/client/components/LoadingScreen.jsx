import React from 'react';
import MDSpinner from 'react-md-spinner';


const LoadingScreen = () => {
  return (
    <div style={{ position: 'fixed', bottom: 40, right: 50, zIndex: 999 }}>
      <MDSpinner size={110} />
    </div>
  );
};

export default LoadingScreen;