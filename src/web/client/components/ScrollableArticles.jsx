import React, { Component } from 'react';

import { isBrowser } from '../utils';

import { Article } from './article';
import LoadingScreen from './LoadingScreen';


class ScrollableArticle extends Component {
  componentDidMount() {
    if (isBrowser) window.addEventListener('scroll', this.onScroll);
  }

  componentWillUnmount() {
    if (isBrowser) window.removeEventListener('scroll', this.onScroll);
  }

  onScroll = () => {
    const bottom = window.document.body.getBoundingClientRect().bottom;

    if (Math.floor(bottom) === window.innerHeight) {
      this.props.onScroll();
    }
  }

  render() {
    const { articles, isFetching } = this.props;
    let isFirstArticle = true;

    return (
      <div>
        <div>
          {
            articles.map((article, index) => {
              if (isFirstArticle) {
                isFirstArticle = false;

                return (
                  <Article
                    firstRendered
                    article={article.toJS()}
                    key={`${article.get('_id')}${index}`}
                  />
                );
              }

              return (
                <Article
                  firstRendered={false}
                  article={article.toJS()}
                  key={`${article.get('_id')}${index}`}
                />
              );
            })
          }
        </div>
        {
          !isFetching ?
            null :
            <LoadingScreen />
        }
      </div>
    )
  }
}

export default ScrollableArticle;