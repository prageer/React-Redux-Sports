import React, { Component } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

import {
  loadMiniArticles as loadMiniArticlesAsync,
  loadRating as loadRatingAsync,
  rateArticle
} from '../actions';
import { Str, getArticleUrl, route, ampUrl } from '../../../utils';

import AdSense from '../../AdSense';
import ArticleTopBar from './ArticleTopBar';
import ArticleTopContainer from './ArticleTopContainer';
import ArticleBody from './ArticleBody';
import LatestArticles from './LatestArticles';


class Article extends Component {
  componentDidMount() {
    const { loadMiniArticles, loadRating, article } = this.props;
    Promise.all([
      loadMiniArticles(article),
      loadRating(article)
    ]);
  }

  getMetaElements(a, tr, metaTitle) {
    return [
      { name: 'description', content: tr.homeDescription },
      { name: 'keywords', content: a.tags.length > 0 ? a.tags.map(t => t.name).join(', ') : '' },
      { name: 'author', content: a.author.name },
      { property: 'og:title', content: metaTitle },
      { property: 'og:description', content: (a.abstract ? a.abstract.slice(0, 60) : a.body.slice(0, 60)) },
    ];
  }

  getLinkElements(content) {
    const articlePathname = getArticleUrl(content);
    const articleUrl = route(articlePathname); // para el canonical.
    const articleEdition = articleUrl.subpathAsSubdomain(1, true);
    const articleAmpUrl = ampUrl(articlePathname, articleEdition);

    return [
      { rel: 'amphtml', href: articleAmpUrl },
    ];
  }

  render() {
    const article = this.props.article.content;
    const a = article;
    const tr = this.props.edition.toJS();

    const metaTitle = Str.format(tr.articleTitle, { title: a.title });
    const firstRendered = this.props.firstRendered;

    const rating = {
      name: article._id,
      currentRating: this.props.article.rating.currentRating,
      canRate: this.props.article.rating.canRate,
      onRate: (stars) => {
        this.props.onRate(stars, this.props.article);
      }
    };

    return (
      <div style={{ overflow: 'auto' }}>
        {
          !firstRendered ?
            null :
            <Helmet
              title={metaTitle}
              meta={this.getMetaElements(a, tr, metaTitle)}
              link={this.getLinkElements(a)}
            />
        }
        <ArticleTopBar article={a} relatedArticle={this.props.article.relatedArticle} />
        <div id="content_article" className="content_article">
          <article id="article" itemScope="http://schema.org/NewsArticle" itemType="http://schema.org/NewsArticle" className="myArticle">
            <ArticleTopContainer article={a} />
            <ArticleBody
              article={a}
              rating={rating}
              miniArticles={this.props.article.miniArticles}
            />
            <div className="clr" />
          </article>
          <div id="right-column">
            <div id="ad300" className="mgt10 mgb15">
              <AdSense
                width={300}
                height={250}
                dataAdSlot="1813578941"
              />
            </div>
            <LatestArticles article={this.props.article} tr={tr} />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (s) => {
  return {
    edition: s.getIn(['app', 'edition'])
  };
};

const mapDispatchToProps = (dispatch) => ({
  loadMiniArticles(article) {
    dispatch(loadMiniArticlesAsync(article));
  },
  loadRating(articleId) {
    dispatch(loadRatingAsync(articleId));
  },
  onRate(stars, article) {
    dispatch(rateArticle(stars, article));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Article);