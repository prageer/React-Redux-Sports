import React from 'react';

import { getImgUrl, getArticleUrl } from '../../../utils';

import Link from '../../Link';


function LatestArticleItem({ article }) {
  const articleUrl = getArticleUrl(article);

  return (
    <div className="clr pdt10 pdb10 brdr1ddd">
      <div className="articles-related-img fltl mgr5">
        <div className="articles-related-cut-img">
          <Link to={articleUrl}>
            {
              !article.image ?
                null :
                <img
                  alt=""
                  src={getImgUrl([
                    '1Thumb',
                    article.image,
                  ])}
                  width="110"
                />
            }
            <br />
          </Link>
        </div>
      </div>
      <h3 className="articles-related-title">
        <Link to={articleUrl} dangerouslySetInnerHTML={{ __html: article.title }} />
      </h3>
      <div className="clr pdb5" />
    </div>
  );
}

export default LatestArticleItem;