import React from 'react';

import { getTagUrl } from '../../../utils';

import Link from '../../Link';


const ArticleTags = ({ edition: tr, tags }) => (
  <footer className="article_tags clr mgt5 pdb15">
    {tags.length > 0 && <span className="more-news">{tr.latestArticlesTaggedAs}</span>}
    {tags.slice().map((tag, i) =>
      <h2 itemProp="keywords" key={`key_${i}`}>
        <Link
          to={getTagUrl(tag, tr.cultureShortcut)}
          title={tr.latestArticlesTaggedNews}
        >
          { tag.name }
        </Link>
      </h2>
    )}
    <div className="clr" />
  </footer>
);

export default ArticleTags;