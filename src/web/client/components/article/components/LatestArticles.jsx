import React from 'react';

import { getImgUrl } from '../../../utils';
import LatestArticleItem from './LatestArticleItem';


function LatestArticles({ tr, article }) {
  const titlePart = article.category ? article.category.name : null;

  const image = article.category ? article.category.image : null;
  const latestArticles = article.latestArticles ? article.latestArticles : [];

  return (
    <div>
      <div className="paddingDiv" />
      <div className="box-white-cat circle-corners10 mgt15">
        {
          !image ?
            null :
            <img
              alt=""
              src={getImgUrl([
                '100',
                image,
              ])}
              style={{ maxWidth: '40px', maxHeight: '40px', height: '100%', float: 'left', marginLeft: '20px', marginRight: '-5px' }}
            />
        }
        <h3 style={{ textTransform: 'capitalize' }} className="ubuntu_text fltl mgl15 black3">
          <span dangerouslySetInnerHTML={{ __html: titlePart }} />
          <span>&nbsp;</span>
          <span className="ft16 black9">{tr.news}</span>
        </h3>
      </div>
      <div className="box-white box-white-most-popular">
        {latestArticles.map((a, index) =>
          <LatestArticleItem article={a} key={'latest_' + index} />
        )}
      </div>
    </div>
  );
}

export default LatestArticles;