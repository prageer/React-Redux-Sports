import React, { Component } from 'react';
import StarRatingComponent from 'react-star-rating-component';

import { Rating, Obj } from '../../../utils';


class RatingArticle extends Component {
  onRate = (stars) => {
    this.props.onRate(stars);
  }

  render() {
    const props = this.props;
    if (Obj.isEmpty(props.currentRating)) return null;

    const { average, total } = Rating.parse(props.currentRating);

    return (
      <div style={{ display: 'block', height: 35, float: 'left' }}>
        <StarRatingComponent
          name={props.name}
          starCount={5}
          value={Math.round(average)}
          editing={props.canRate}
          onStarClick={this.onRate}
        />

        <span style={{ display: 'inline-block', padding: '0 15px' }}>
          <span>MEDIA: {average}</span>
        </span>

        <span style={{ display: 'inline-block' }}>
          <span>VOTES: {total}</span>
        </span>
      </div>
    );
  }
}

export default RatingArticle;