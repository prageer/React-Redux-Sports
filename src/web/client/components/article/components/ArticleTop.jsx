import React, { Component } from 'react';

import {
  isAuthorArticleZone,
  getArticleAuthorUrl,
  getArticleAuthorImageUrl,
  getArticleUrl,
  vvDateTimeFormat
} from '../../../utils';

import Link from '../../Link';


class ArticleTop extends Component {

  twitterLikeClick = () => {
    const a = this.props.article;
    const articleUrl = getArticleUrl(a);

    window.open('https://twitter.com/intent/tweet?original_referer=' + articleUrl + '&text=' + a.title + '&tw_p=tweetbutton&url=' + articleUrl + '&via=VAVELcom', 'name', 'width=600,height=500');
  }


  fbLikeClick = () => {
    const a = this.props.article;
    const articleUrl = getArticleUrl(a);
    const appId = 145634995501895;

    window.open('https://www.facebook.com/dialog/share?appId=' + appId + '&display=popup&quote=' + a.title + '&href=' + articleUrl, 'name', 'width=600,height=500');
  }

  googleLikeClick = () => {
    const articleUrl = getArticleUrl(this.props.article);
    window.open('https://plus.google.com/share?url=' + articleUrl, 'name', 'width=600,height=500');
  }

  render() {
    const a = this.props.article;
    const tr = this.props.edition;

    const createdAt = vvDateTimeFormat(a.created_at, tr.locale);
    const updatedAt = vvDateTimeFormat(a.updated_at, tr.locale);
    const isAuthorOnZone = isAuthorArticleZone(a);
    const authorUrl = getArticleAuthorUrl(a);

    return (
      <header className="article-header">
        <h1 itemProp="name headline" dangerouslySetInnerHTML={{ __html: a.title }} />
        <time itemProp="datePublished" dateTime={createdAt} className="date_article">{createdAt}</time>
        <meta content={updatedAt} />
        <h3 itemProp="description" className="abstract" dangerouslySetInnerHTML={{ __html: a.abstract }} />
        <div className="horizontal-rule" />
        <section className="author_bar">

          {!isAuthorOnZone && <div itemScope="" itemProp="author creator" itemType="http://schema.org/Person" className="author_bar_left fltl">
            <div className="author_bar_left1">
              <Link to={authorUrl} title={a.author.name}>
                <img
                  itemProp="image"
                  src={getArticleAuthorImageUrl([
                    '1Thumb',
                    a,
                    'circle',
                  ])}
                  alt={a.author.username}
                  height="63"
                  className="image"
                />
              </Link>
            </div>
            <div className="mgt10 mgb5">
              <Link to={authorUrl} title={a.author.name} className="autor2">
                <span itemProp="name" className="FFF"><strong>{a.author.name}</strong></span>
              </Link>
            </div>
            <div className="social_data">
              {a.author.twitter && <div className="fltl mgr5">
                <a href={`https://twitter.com/${a.author.twitter}`} target="_blank" rel="nofollow">
                  <img alt="Twitter" src="/public/assets/img/tw25.png" height="20" />
                </a>
              </div>}
              {a.author.plus && <a href={a.author.plus} target="_blank" rel="nofollow">
                <img alt="Google Plus" src="/public/assets/img/googleplus25.png" height="20" />
              </a>}
            </div>
          </div>}

          <aside className="fltr">
            <div itemProp="publisher" itemScope="" itemType="https://schema.org/Organization" className="fltl">
              <meta itemProp="name" content="VAVEL.com" />
              <div itemProp="logo" itemScope="" itemType="https://schema.org/ImageObject">
                <img alt="VAVEL Logo" src="https://img.vavel.com/b/vavel-logo-72.png" height="65" />
                <meta itemProp="url" content="https://img.vavel.com/b/vavel-logo-72.png" />
                <meta itemProp="width" content="65" />
                <meta itemProp="height" content="65" />
              </div>
            </div>

            <div className="fltl pdr5">
              <a target="popup" onClick={this.twitterLikeClick}>
                <span rel="nofollow" className="social_share social_tw">Twitter</span>
              </a>
            </div>
            <div className="fltl pdr5">
              <a target="popup" onClick={this.fbLikeClick} rel="nofollow">
                <span className="social_share social_fb">Facebook</span>
              </a>
            </div>
            <div className="fltl">
              <a target="popup" onClick={this.googleLikeClick}>
                <span rel="nofollow" className="social_share social_gplus">Google Plus</span>
              </a>
            </div>

          </aside>

        </section>
        <div className="horizontal-rule mgb20" />
      </header>
    );
  }
}

export default ArticleTop;