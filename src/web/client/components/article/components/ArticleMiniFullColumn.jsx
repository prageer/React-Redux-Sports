import React from 'react';
import PropTypes from 'prop-types';

import MiniColumn from './MiniColumn';


const ArticleMinifullColumn = ({ articles }) => (
  <div className="column160">
    {articles.map((article, index) =>
      <MiniColumn article={article} key={`mini_${index}`} />
    )}
  </div>
);

ArticleMinifullColumn.propTypes = {
  articles: PropTypes.array
};

export default ArticleMinifullColumn;