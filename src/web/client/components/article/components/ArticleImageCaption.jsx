import React from 'react';


function ArticleImageCaption({ article: a }) {
  return !a.image_caption ?
    null :
    (
      <figcaption
        className="image_caption_news"
        dangerouslySetInnerHTML={{ __html: a.image_caption || a.title }}
      />
    );
}

export default ArticleImageCaption;