import React from 'react';

import { getArticleAuthorImageUrl } from '../../../utils';

import AdSense from '../../AdSense';
import RelatedArticleItem from './RelatedArticleItem';
import Link from '../../Link';


function ArticleTopBar({ article: a, relatedArticle }) {
  const authorImage = getArticleAuthorImageUrl([
    '1Thumb',
    a,
    'default',
  ]);

  return (
    <div className="box-white-cat articles-related">
      <div className="submenu_960">
        <div className="fltl ad728">
          <AdSense dataAdSlot="2469580480" />
        </div>
        {a.isAuthorArticleZone && <div>

          <div style={{ width: '310px', borderRight: '#EEE solid 1px' }} className="articles-related-new300">
            <div style={{ maxHeight: '90px', marginRight: '20px', webkitBorderRadius: '10px', mozBorderRadius: '10px', borderRadius: '10px' }} className="author_bar_left1">
              <Link to={a.authorUrl} title={a.author.name} style={{ textAlign: 'center', float: 'left' }}>
                <img src={authorImage} alt={a.author.name} height="90" />
              </Link>
              <div style={{ maxHeight: '26px', overflow: 'hidden', float: 'left' }} className="mgt10 mgb10 mgl10">
                <Link to={a.authorUrl} title={a.author.name} style={{ fontFamily: "'Ubuntu Condensed', Arial, Helvetica, sans-serif", fontSize: '500', color: '#333' }}>{a.author.name}</Link>
              </div>
              <div className="fltl mgr10 mgl10">
                <a href={'https://twitter.com/' + a.author.twitter + '&amp;&amp; ' + (a.author.twitter.length ? a.author.twitter : 'VAVEL')} data-show-count="false" data-lang="en" data-size="large" className="twitter-follow-button" />
              </div>
            </div>
          </div>

          <div className="articles-related-new300">
            <div style={{ fontSize: '16px', lineHeight: '120%', fontFamily: "'Ubuntu Condensed', Arial, Helvetica, sans-serif", color: '#333', fontWeight: '400' }} className="author_bio">
              <div style={{ height: 77 }} className="www_url pdt10">
                {a.author.www && <a href={a.author.www} rel="nofollow" target="_blank"><b>{a.author.www}</b></a>}
              </div>
            </div>
          </div>

        </div>}
        {
          !relatedArticle ?
            null :
            <RelatedArticleItem r={relatedArticle} />
        }
      </div>
    </div>
  );
}

export default ArticleTopBar;