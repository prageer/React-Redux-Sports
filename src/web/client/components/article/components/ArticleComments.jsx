import React from 'react';

import { getArticleUrl } from '../../../utils';


const ArticleComments = ({ article }) => (
  <div>
    <div id="fb-root" />
    <div
      href={getArticleUrl(article)}
      data-width="100%"
      data-numposts="10"
      data-colorscheme="light"
      className="fb-comments"
    />
  </div>
);

export default ArticleComments;