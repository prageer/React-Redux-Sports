import React from 'react';
import PropTypes from 'prop-types';

import { getImgUrl } from '../../../utils';


const ArticleImage = ({ article: a }) => {
  const url = getImgUrl([
    '1',
    a.image,
  ]);

  let articleImageWidth;
  if (a.show_inarticle === 6) {
    articleImageWidth = 300;
  } else {
    articleImageWidth = a.show_inarticle === 3 ||
      a.show_inarticle === 20 ||
      a.show_inarticle === 2 ?
        610 :
        420;
  }

  return (
    <a href={url} title={a.title} target="_blank" rel="nofollow">
      <img src={url} title={a.title} alt={a.title} itemProp="image" width={articleImageWidth} className="article-image" />
    </a>
  );
};

ArticleImage.propTypes = {
  article: PropTypes.object.isRequired
};

export default ArticleImage;