import React from 'react';

import {
  getArticleUrl,
  getImgUrl,
  getArticleCategoryUrl,
  Str
} from '../../../utils';

import Link from '../../Link';


function RelatedArticleItem({ r }) {
  const rUrl = getArticleUrl(r);
  const imageUrl = getImgUrl([
    '1Thumb',
    r.image,
  ]);

  return (
    <div style={{ float: 'right', borderRight: 'none', paddingRight: '0px', marginRight: '0px', width: '205px' }} className="articles-related-new300">
      <div className="articles-related-img">
        <div className="articles-related-cut-img">
          <Link to={rUrl}>
            {
              !imageUrl ?
                null :
                <img src={imageUrl} alt={r.image_caption} width="63" />
            }
            <br />
          </Link>
        </div>
      </div>
      <h3 style={{ paddingTop: '3px' }} className="antetitulo">
        <Link
          to={getArticleCategoryUrl(r.category)}
          title={r.category.name}
        >
          {r.category.name}
        </Link>
      </h3>
      <h3 className="articles-related-title">
        <Link to={rUrl} dangerouslySetInnerHTML={{ __html: Str.limit(r.title, 70) }} />
      </h3>
    </div>
  );
}

export default RelatedArticleItem;