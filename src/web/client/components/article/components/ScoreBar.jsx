import React from 'react';
import PropTypes from 'prop-types';


const ScoreBar = ({ article: a }) => {
  const fontClass = (a.show_inarticle === 0 ? 'score_number300_1' : 'score_number') +
    (a.goles_visitante > a.goles_local ? ' grey_text' : '');
  const fontClass2 = (a.show_inarticle === 0 ? 'score_number300_2' : 'score_number') +
    (a.goles_local > a.goles_visitant ? 'grey_text' : '');

  return (
    <div>
      {a.equipo_local && <div id="score_bar" style={{ marginBottom: '0px' }}>
        <div className={a.show_inarticle === 0 ? 'score_team300' : 'score_team'} style={{ float: 'left' }} dangerouslySetInnerHTML={{ __html: a.equipo_local }} />
        {(a.goals || a.show_inarticle === 2) && <div style={{ float: 'left', width: '36%' }}>
          <div id="score_count" style={{ margin: '0 auto', width: '100%', textAlign: 'center' }}>
            <font className={fontClass} style={{ marginRight: '7px' }}>{a.goles_local}</font>
            <font className={fontClass2}>{a.goles_visitante}</font>
            <br />
          </div>
        </div>}
        <div className={a.show_inarticle === 0 ? 'score_team300' : 'score_team'} style={{ float: 'right' }} dangerouslySetInnerHTML={{ __html: a.equipo_visitante }} />
      </div>}
    </div>
  );
};

ScoreBar.propTypes = {
  article: PropTypes.object.isRequired
};

export default ScoreBar;