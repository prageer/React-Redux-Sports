import React from 'react';
import { connect } from 'react-redux';

import ArticleTop from './ArticleTop';


const ArticleTopContainer = ({ edition, article }) => (
  <ArticleTop edition={edition.toJS()} article={article} />
);

const mapStateToProps = (state) => {
  return { edition: state.getIn(['app', 'edition']) };
};

export default connect(
  mapStateToProps
)(ArticleTopContainer);