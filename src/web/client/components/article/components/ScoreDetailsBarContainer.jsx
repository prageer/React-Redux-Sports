import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import ScoreDetailsBar from './ScoreDetailsBar';


const ScoreDetailsBarContainer = ({ article, edition }) => (
  <ScoreDetailsBar article={article} tr={edition.toJS()} />
);

ScoreDetailsBarContainer.propTypes = {
  article: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
  edition: state.getIn(['app', 'edition'])
});

export default connect(
  mapStateToProps
)(ScoreDetailsBarContainer);