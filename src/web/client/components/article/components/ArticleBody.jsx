import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { isBrowser } from '../../../utils';

import ScoreBar from './ScoreBar';
import AdSense from '../../AdSense';
import ArticleTagsContainer from './ArticleTagsContainer';
import ArticleImage from './ArticleImage';
import ArticleImageCaption from './ArticleImageCaption';
import ScoreDetailsBarContainer from './ScoreDetailsBarContainer';
import ArticleComments from './ArticleComments';
import ArticleMiniFullColumn from './ArticleMiniFullColumn';
import RatingArticle from './RatingArticle';


class ArticleBody extends Component {
  componentDidMount() {
    if (window.twttr) {
      window.twttr.widgets.load();
    }
  }

  renderPart(a) {
    switch (a.show_inarticle) {
      case 3: return (
        <div>
          <figure className="article-image">
            {a.image && <ArticleImage article={a} />}
          </figure>
          <ArticleImageCaption article={a} />
          <ScoreBar article={a} />
          <ScoreDetailsBarContainer article={a} />
          <div className="mgb20 mgt20">
            <AdSense dataAdSlot="2996212489" width={'100%'} />
          </div>
          <section itemProp="articleBody" className="article-content" dangerouslySetInnerHTML={{ __html: a.body }} />
        </div>
      );

      case 20: return (
        <div>
          <figure>
            {a.image && <ArticleImage article={a} />}
          </figure>
          <ArticleImageCaption article={a} />
          <ScoreDetailsBarContainer article={a} />
          <div className="live_bg">
            <img alt="Live Loader" src="/public/assets/img/live-loader.gif" className="loader" />
            <img alt="VAVEL Live Small" src="/public/assets/img/vavel_live_small.png" className="logo_lives fltl" />
            <img alt="Live Match" src="/public/assets/img/live-match.png" className="live_event_img" />
          </div>
          <ScoreBar article={a} />
          <div className="ad-body mgb20 mgt20">
            <AdSense width={'100%'} dataAdSlot="8760766484" />
          </div>
          <section itemProp="articleBody" className="article-content article_text_live" dangerouslySetInnerHTML={{ __html: a.body }} />
        </div>
      );

      case 2: return (
        <div>
          <figure itemProp="image" itemScope="" itemType="http://schema.org/ImageObject">
            {a.image && <ArticleImage article={a} />}
          </figure>
          <ArticleImageCaption article={a} />
          <ScoreDetailsBarContainer article={a} />
          <div className="live_bg">
            <img alt="Live Loader" src="/public/assets/img/live-loader.gif" className="loader" />
            <img alt="VAVEL Live Small" src="/public/assets/img/vavel_live_small.png" className="logo_lives fltl" />
            <img alt="Live Match" src="/public/assets/img/live-match.png" className="live_event_img" />
          </div>
          <ScoreBar article={a} />
          <div className="ad-body mgb20 mgt20">
            <AdSense width={'100%'} dataAdSlot="8760766484" />
          </div>
          <section itemProp="articleBody" className="article_live" dangerouslySetInnerHTML={{ __html: a.body }} />
        </div>
      );

      case 6: return (
        <div>
          <ScoreBar article={a} />
          <figure className="image_article" itemScope="" itemType="http://schema.org/ImageObject">
            {a.image && <ArticleImage article={a} />}
            <div className="horizontal-rule" />
            <ScoreDetailsBarContainer article={a} />
          </figure>
          <div className="mgb20 mgt20">
            <AdSense dataAdSlot="2996212489" width={'100%'} />
          </div>
          <section itemProp="articleBody" className="article-content" dangerouslySetInnerHTML={{ __html: a.body }} />
        </div>
      );

      case 0: return (
        <div>
          <div className="column425">
            <figure className="article-image" itemProp="image" itemScope="" itemType="http://schema.org/ImageObject">
              {a.image && <ArticleImage article={a} />}
            </figure>
            <div className="mgb20 mgt20">
              <AdSense dataAdSlot="2996212489" width={'100%'} />
            </div>
            <ScoreBar article={a} />
            <ScoreDetailsBarContainer article={a} />
            <section itemProp="articleBody" className="article-content" dangerouslySetInnerHTML={{ __html: a.body }} />
            <AdSense dataAdSlot="2853833682" />
            {a.tags && <ArticleTagsContainer tags={a.tags} />}
            <ArticleComments article={a} />
          </div>
          {
            !this.props.miniArticles.length === 0 ?
              null :
              <ArticleMiniFullColumn articles={this.props.miniArticles} />
          }
        </div>
      );

      default: return null;
    }
  }

  render() {
    const a = this.props.article;

    return (
      <div>
        {this.renderPart(a)}
        {(a.show_inarticle !== 2 && a.show_inarticle !== 0) && <div itemProp="publisher" itemScope="" itemType="https://schema.org/Organization" className="fltr mgb15">
          <meta itemProp="name" content="VAVEL.com" />
          <meta itemProp="url" content="https://www.vavel.com" />
          <div itemProp="logo" itemScope="" itemType="https://schema.org/ImageObject">
            <img alt="VAVEL Logo" src="https://img.vavel.com/b/vavel-logo-72.png" height="65" />
            <meta itemProp="url" content="https://img.vavel.com/b/vavel-logo-72.png" />
            <meta itemProp="width" content="65" />
            <meta itemProp="height" content="65" />
          </div>
        </div>
        }

        <RatingArticle {...this.props.rating} />

        {a.show_inarticle !== 0 && <div>
          {a.tags && <ArticleTagsContainer tags={a.tags} />}
          <ArticleComments article={a} />
        </div>}

      </div>
    );
  }
}

ArticleBody.propTypes = {
  article: PropTypes.object.isRequired
};

export default ArticleBody;