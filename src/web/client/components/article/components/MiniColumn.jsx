import React from 'react';

import { getArticleUrl, getImgUrl } from '../../../utils';

import Link from '../../Link';


const MiniColumn = ({ article }) => {
  const { title } = article;
  const articleUrl = getArticleUrl(article);

  const imageUrl = getImgUrl([
    '210',
    article.image,
  ]);

  return (
    <div className="last_news160_related">
      <div className="last_news160_image">
        <div className="last_news160_image_content">
          <Link to={articleUrl}>
            <img src={imageUrl} alt={title} title={title} width="156" /><br />
          </Link>
        </div>
      </div>
      <h3><Link to={articleUrl} dangerouslySetInnerHTML={{ __html: title }} /></h3>
    </div>
  );
};

export default MiniColumn;