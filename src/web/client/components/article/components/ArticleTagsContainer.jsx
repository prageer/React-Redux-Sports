import React from 'react';
import { connect } from 'react-redux';

import ArticleTags from './ArticleTags';


const ArticleTagsContainer = ({ edition, tags }) => (
  <ArticleTags edition={edition.toJS()} tags={tags} />
);

const mapStateToProps = (state) => ({
  edition: state.getIn(['app', 'edition'])
});

export default connect(
  mapStateToProps
)(ArticleTagsContainer);