import React, { Component } from 'react';


const ScoreDetailsBar = ({ tr, article }) => {
  const a = article;
  const condition = a.show_inarticle === 2 || a.show_inarticle === 3;

  return (
    <div>
      {a.equipo_local && a.team1 && <div className="ficha_imagen">
        <div className={condition ? 'data_score' : ''}>
          <b><span dangerouslySetInnerHTML={{ __html: a.equipo_local }} />:&nbsp;</b>
          <span dangerouslySetInnerHTML={{ __html: a.team1 }} />
        </div>
        <div className={condition ? 'data_score2' : ''}>
          <b><span dangerouslySetInnerHTML={{ __html: a.equipo_visitante }} />:&nbsp;</b>
          <span dangerouslySetInnerHTML={{ __html: a.team2 }} />
        </div>
        {a.goals && <div className={condition ? 'data_score3' : ''}>
          <b><span>{tr.goals}</span>:&nbsp;</b>
          <span dangerouslySetInnerHTML={{ __html: a.goals }} />
        </div>}
        {a.referee && <div className={condition ? 'data_score3' : ''}>
          <b><span>{tr.referee}</span>:&nbsp;</b>
          <span dangerouslySetInnerHTML={{ __html: a.referee }} />
        </div>}
        {a.incidents && <div className={condition ? 'data_score4' : ''}>
          <b><span>{tr.incidents}</span>:&nbsp;</b>
          <span dangerouslySetInnerHTML={{ __html: a.incidents }} />
        </div>}
      </div>}
    </div>
  );
};

export default ScoreDetailsBar;