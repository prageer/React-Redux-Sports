import { createAction } from 'redux-actions';

import {
  REQUEST_MINI_ARTICLES,
  MINI_ARTICLES_RECEIVED,
  ERROR_MINI_ARTICLES,
  SET_MINI_ARTICLES,
  SET_LATEST_ARTICLES,
  SET_CATEGORY_ARTICLE,
  REQUEST_RATING_ARTICLE,
  RATING_ARTICLE_RECEIVED,
  ERROR_RATING_ARTICLE,
  SET_RATING_ARTICLE,
  REQUEST_RATE_ARTICLE,
  RATE_ARTICLE_RECEIVED,
  ERROR_RATE_ARTICLE
} from './actionTypes'

export const requestMiniArticles = createAction(REQUEST_MINI_ARTICLES);
export const miniArticlesReceived = createAction(MINI_ARTICLES_RECEIVED);
export const errorMiniArticles = createAction(ERROR_MINI_ARTICLES);
export const setMiniArticles = createAction(SET_MINI_ARTICLES);

export const setLatestArticles = createAction(SET_LATEST_ARTICLES);

export const setCategoryArticle = createAction(SET_CATEGORY_ARTICLE);

export const requestRatingArticle = createAction(REQUEST_RATING_ARTICLE);
export const ratingArticleReceived = createAction(RATING_ARTICLE_RECEIVED);
export const errorRatingArticle = createAction(ERROR_RATING_ARTICLE);

export const setRatingArticle = createAction(SET_RATING_ARTICLE);

export const requestRateArticle = createAction(REQUEST_RATE_ARTICLE);
export const rateArticleReceived = createAction(RATE_ARTICLE_RECEIVED);
export const errorRateArticle = createAction(ERROR_RATE_ARTICLE);

/**
 * Rate an article.
 *
 * @param {Object} stars Stars
 * @param {Object} content Article content.
 */
export const rateArticle = (stars, { content }) => {
  return function (dispatch, getState, api) {

    const article_id = content.article_id;

    dispatch(requestRateArticle({ article_id }));

    return api.addRatingArticle({ id: content._id, stars })
      .then((newRating) => {
        dispatch(setRatingArticle({ article_id, currentRating: newRating }));
        dispatch(rateArticleReceived({ article_id }));
      })
      .catch((err) => {
        console.log(`Error on rateArticle, id=${content._id}`);
        console.error(err);
        dispatch(errorRateArticle({ err, article_id }));
      });
  }
};

/**
 * Load rating for article.
 *
 * @param {Object} article Article.
 */
export const loadRating = ({ rating, content }) => {
  return function (dispatch, getState, api) {

    if (!rating.canRate) {
      alert("You can't rate anymore.");
      return;
    }
    const article_id = content.article_id;

    dispatch(requestRatingArticle({ article_id }));

    return api.addRatingArticle({ id: content._id })
      .then((newRating) => {
        dispatch(setRatingArticle({ article_id, currentRating: newRating }));
        dispatch(ratingArticleReceived({ article_id }));
      })
      .catch((err) => {
        console.warn(`Error on loadRating, id=${content._id}`);
        console.error(err);
        dispatch(errorRatingArticle({ err, article_id }));
      });
  }
};

/**
 * Load mini articles for article.
 *
 * @param {Object} content Articlef content.
 */
export const loadMiniArticles = ({ content }) => {
  return function (dispatch, getState, api) {

    const article_id = content.article_id;
    const categoryId = content.category.id;

    dispatch(requestMiniArticles({ article_id }));

    if (!categoryId) return;

    return api.getContentMiniArticles(categoryId, article_id)
      .then((miniArticles = []) => {
        dispatch(setMiniArticles({ miniArticles, article_id }));
        dispatch(miniArticlesReceived({ article_id }));
      })
      .catch((err) => {
        console.warn(`Error on loadMiniArticles, categoryId=${category.id}, article_id=${article_id}`);
        console.error(err);
        dispatch(errorMiniArticles({ err, article_id }));
      });
  }
};