import { fromJS, List as list } from 'immutable';

import { Rating } from '../../utils';
import {
  SET_MINI_ARTICLES,
  REQUEST_MINI_ARTICLES,
  MINI_ARTICLES_RECEIVED,
  ERROR_MINI_ARTICLES,
  SET_LATEST_ARTICLES,
  SET_CATEGORY_ARTICLE,
  REQUEST_RATING_ARTICLE,
  RATING_ARTICLE_RECEIVED,
  ERROR_RATING_ARTICLE,
  SET_RATING_ARTICLE,
  REQUEST_RATE_ARTICLE,
  RATE_ARTICLE_RECEIVED,
  ERROR_RATE_ARTICLE
} from './actionTypes';

export const initialState = fromJS({
  isFetchingMiniArticles: false,
  latestArticles: [],
  miniArticles: [],
  category: {},
  content: {},
  rating: {
    name: '',
    currentRating: {},
    canRate: true,
    isFetching: false,
    isRating: false
  }
});

export const reducer = (article = initialState, { type, payload }) => {
  switch (type) {
    case SET_MINI_ARTICLES:
      return article.set('miniArticles', list(fromJS(payload.miniArticles)));
      break;

    case REQUEST_MINI_ARTICLES:
      return article.set('isFetchingMiniArticles', true);
      break;

    case MINI_ARTICLES_RECEIVED:
      return article.set('isFetchingMiniArticles', false);
      break;

    case ERROR_MINI_ARTICLES:
      return article.set('isFetchingMiniArticles', false);
      break;

    case SET_LATEST_ARTICLES:
      return article.set('latestArticles', list(fromJS(payload.latestArticles)));
      break;

    case SET_CATEGORY_ARTICLE:
      return article.set('category', fromJS(payload.category));
      break;

    case SET_RATING_ARTICLE:
      return article.update('rating', (rating) => {
        return rating.merge({
          currentRating: payload.currentRating.data.rating,
          canRate: Rating.isAllowedToRate(payload.currentRating)
        });
      });
      break;

    case REQUEST_RATING_ARTICLE:
      return article.update('rating', (rating) => {
        return rating.set('isFetching', true);
      });
      break;

    case RATING_ARTICLE_RECEIVED:
      return article.update('rating', (rating) => {
        return rating.set('isFetching', false);
      });
      break;

    case ERROR_RATING_ARTICLE:
      return article.update('rating', (rating) => {
        return rating.set('isFetching', false);
      });
      break;

    case REQUEST_RATE_ARTICLE:
      return article.update('rating', (rating) => {
        return rating.set('isRating', true);
      });
      break;

    case RATE_ARTICLE_RECEIVED:
      return article.update('rating', (rating) => {
        return rating.set('isRating', false);
      });
      break;

    case ERROR_RATE_ARTICLE:
      return article.update('rating', (rating) => {
        return rating.set('isRating', false);
      });
      break;

    default:
      return article;
  }
};