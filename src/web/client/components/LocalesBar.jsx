import React, { Component } from 'react';

import LocalBarItem from './LocalBarItem';


class LocalesBar extends Component {

  ModaltoLoginBlock = () => {
    document.getElementById('id01').style.display = 'block';
  }

  ModaltoLoginNone = () => {
    document.getElementById('id01').style.display = 'none';
  }

  flagClick = () => {
    document.getElementById('editions-nav').classList.toggle('show');
  }

  render() {
    const { edition } = this.props;

    const flagName = edition.flag || edition.lang;
    const flagSrc = `/assets/img/flags/${flagName.toLowerCase()}.png`;

    return (
      <nav className="fltl">
        <img className="item"
          src={flagSrc}
          alt={edition.homeTitle}
          onClick={this.flagClick}
        />
        <ul id="editions-nav" className="dropdown-content">
          {
            edition.cultures.map((culture, index) =>
              <LocalBarItem
                onClick={this.flagClick}
                culture={culture}
                key={index}
              />)
          }
        </ul>
        <img className="item"
          alt="Login"
          onClick={this.ModaltoLoginBlock}
          src="/assets/img/user-icon.png"
        />
        <div id="id01" className="modal">
          <div className="modal-content">
            <header className="modal_block">
              <span onClick={this.ModaltoLoginNone} className="closebtn">&times;</span>
              <span>Login or Create your account</span>
            </header>
            <div className="w3-container">
              <p>Some text..</p>
              <p>Some text..</p>
            </div>
            <footer className="modal_block">
              <p>Modal Footer</p>
            </footer>
          </div>
        </div>
      </nav>
    );
  }
}

export default LocalesBar;