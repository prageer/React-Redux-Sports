import React, { Component } from 'react';


class AdSense extends Component {
  componentDidMount() {
    try {
      (window.adsbygoogle = window.adsbygoogle || []).push({});
    } catch (err) {
      console.warn('Warning trying to add an ad.');
      console.warn(err);
    }
  }

  render() {
    const { dataAdSlot, width, height } = this.props;

    return (
      <div>
        <ins
          style={{
            display: 'block',
            width: width || 'auto',
            height: height || 'auto'
          }}
          data-ad-format="auto"
          data-ad-client="ca-pub-4378161002089632"
          data-ad-slot={dataAdSlot}
          className="adsbygoogle"
        />
      </div>
    );
  }
}

export default AdSense;