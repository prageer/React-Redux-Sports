import React from 'react';

import Link from './Link';


const LocalBarItem = ({ culture, onClick }) => {
  const clsname = culture.class + ' flag fltl mgr10';

  return (
    <li>
      <Link to={culture.href}
        title={culture.title}
        onClick={onClick}
        className="black3"
      >
        <span className={clsname} />
        <span className="mgr5" dangerouslySetInnerHTML={{ __html: culture.title_short }} />
      </Link>
    </li>
  );
};

export default LocalBarItem;