import React, { Component } from 'react';

import { isBrowser } from '../utils';


class ToTopArrow extends Component {

  constructor(props) {
    super(props);

    this.state = {
      show: false
    };
  }

  componentWillMount() {
    if (isBrowser) {
      window.addEventListener('scroll', this.handleScroll);
    }
  }

  componentWillUnmount() {
    if (isBrowser) {
      window.removeEventListener('scroll', this.handleScroll);
    }
  }

  onClick = () => window.document.getElementById('sb-site').scrollIntoView();

  handleScroll = () => {
    if (window.scrollY > 200) {
      if (!this.state.show) this.setState({ show: true });
    } else if (this.state.show) {
      this.setState({ show: false });
    }
  }

  render() {
    return (
      <a
        id="toTop"
        className={this.state.show ? 'visible' : 'hidden'}
        onClick={this.onClick}
      />
    );
  }
}

export default ToTopArrow;