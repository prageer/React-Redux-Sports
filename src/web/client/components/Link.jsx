import React, { Component } from 'react';
import { Link as RLink } from 'react-router';

import { editions, route } from '../utils';


/**
 * Wrapper para manejar las URLs absolutas
 * ya que react-router las acepta.
 */
const Link = (props) => {
  return props.children ?
    <RLink {...props}>{props.children}</RLink> :
    <RLink {...props} />
};

export default Link;