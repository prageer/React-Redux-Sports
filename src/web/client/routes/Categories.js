import { prefixRoute, isServer, view } from '../utils';

import { authorBelongsToEdition } from './Author';


/**
 * Route for categories.
 */
export default {
  path: prefixRoute('/:category'),
  getComponent: view('Categories'),
  onEnter({ params }, replace, callback) {
    if (isServer) {
      authorBelongsToEdition(params.category, params.editionName)
        .then((data) => {
          if (data.pathname) replace(data.pathname);
          callback();
        })
        .catch((err) => {
          callback();
        });
    } else {
      callback();
    }
  }
};