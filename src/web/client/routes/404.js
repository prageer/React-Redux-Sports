import { prefixRoute, view } from '../utils';

/**
 * Ruta para el error.
 */
export default {
  path: prefixRoute('/404'),
  getComponent: view('404')
};