import { prefixRoute, isServer, Obj, view } from '../utils';
import { api } from '../services';
import { getSubdomain } from '../../rules/countryToSubdomain';


export const authorBelongsToEdition = (posibleAuthor, currentLanguage) => {
  return api.getAuthorArticles(posibleAuthor, 0, 1)
    .then((articles = []) => {
      const author = articles.length > 0 ?
        articles[0].author :
        {};

      const belongsData = {
        pathname: null,
        subdomainsEquals: null
      };

      if (!Obj.isEmpty(author)) {
        const subdomain = getSubdomain(author.country);
        belongsData.pathname = `/${subdomain}/author/${posibleAuthor}`;
        belongsData.subdomainsEquals = subdomain === getSubdomain(currentLanguage);
        console.log(`subdomain=${subdomain}, currentLanguage=${currentLanguage}, get=${getSubdomain(currentLanguage)}`);
      }

      return belongsData;
    });
};

/**
 * Route for author.
 */
export default {
  path: prefixRoute('/author/:author'),
  getComponent: view('Author'),
  onEnter({ params }, replace, callback) {
    if (isServer) {
      authorBelongsToEdition(params.author, params.editionName)
        .then((data) => {
          if (data.pathname && !data.subdomainsEquals) {
            replace(data.pathname);
          }
          callback();
        })
        .catch((err) => {
          callback();
        });
    } else {
      callback();
    }
  }
};