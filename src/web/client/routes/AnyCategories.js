import { view, prefixRoute } from '../utils';


/**
 * Route for any category.
 */
export default {
  path: prefixRoute('/:category/*'),
  getComponent: view('Categories')
};