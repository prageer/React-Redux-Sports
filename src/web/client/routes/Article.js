import { prefixRoute, extractArticleId, getArticleUrl, isBrowser, view } from '../utils';
import { api } from '../services';
import { WITH_SUBDOMAIN } from '../../rules';

export const onEnter = ({ params, location }, replace, callback) => {
  if (isBrowser) {
    callback();
  } else {
    api.getArticleById(extractArticleId(params.articleId))
      .then((article = {}) => {
        const articleEdition = article.edition;
        const currentEditionName = params.editionName;

        const currentUrl = WITH_SUBDOMAIN ?
          `/${currentEditionName}${location.pathname}` :
          location.pathname;
        const articleUrl = getArticleUrl(article);

        console.log('=========================================');
        let articleBelongToEdition;
        try {
          console.log(`articleEdition: ${articleEdition}`);
          console.log(`currentEditionName: ${currentEditionName}`);
          articleBelongToEdition = articleEdition.toLowerCase() === currentEditionName.toLowerCase();
        } catch (err) {
          articleBelongToEdition = false;
        }

        console.log(`articleEdition === currentEditionName => ${articleBelongToEdition}`);
        console.log(`currentUrl => ${currentUrl}`);
        console.log(`articleUrl => ${articleUrl}`);
        console.log('=========================================');

        if (currentUrl !== articleUrl || !articleBelongToEdition) {
          replace(articleUrl);
        }

        callback();
      })
  }
};

export const getComponent = view('ArticleView');

/**
 * Route for Article.
 */
export default {
  path: prefixRoute('/:category/:articleId.html'),
  onEnter,
  getComponent
};