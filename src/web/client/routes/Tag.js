import { prefixRoute, view } from '../utils';

/**
 * Route for tag.
 */
export default {
  path: prefixRoute('/tag/:tag'),
  getComponent: view('Tag')
};