import { prefixRoute } from '../utils';
import { onEnter, getComponent } from './Article';


/**
 * Route for Article.
 */
export default {
  path: prefixRoute('/:category/**/:articleId.html'),
  onEnter,
  getComponent
};