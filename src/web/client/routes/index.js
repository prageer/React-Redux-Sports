import AppContainer from '../views/AppContainer';

import { prefixRoute } from '../utils';


export default {
  path: prefixRoute('/'),
  component: AppContainer,
  getIndexRoute: (_, cb) => {
    cb(null, {
      component: require('../views/Articles').default
    });
  },
  childRoutes: [
    require('./404').default,
    require('./Author').default,
    require('./Tag').default,
    require('./Categories').default,
    require('./Article').default,
    require('./Article2').default, // https://martinfowler.com/bliki/TwoHardThings.html
    require('./AnyCategories').default
  ]
};