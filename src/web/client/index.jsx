import 'babel-core/register';
import 'babel-polyfill';

import React from 'react';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import PropTypes from 'prop-types';
import { fromJS } from 'immutable';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory, match, Router } from 'react-router';

import { api } from './services';
import vavelApp from './reducers';
import routes from './routes/index';
import { MOUNT_POINT } from '../rules';
import { fetchDataOnLocationMatch, rehydrate } from './utils';


const prelodadedState = rehydrate();
const store = createStore(
  vavelApp,
  fromJS(prelodadedState),
  process.env.NODE_ENV === 'production' ?
    applyMiddleware(thunk.withExtraArgument(api)) :
    applyMiddleware(thunk.withExtraArgument(api), logger)
);
/* Create enhanced history object for router */
const createSelectLocationState = () => {
  let prevRoutingState, prevRoutingStateJS;
  return (state) => {
    const routingState = state.get('routing'); // or state.routing
    if (typeof prevRoutingState === 'undefined' || prevRoutingState !== routingState) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }
    return prevRoutingStateJS;
  };
};
const history = syncHistoryWithStore(browserHistory, store, {
  selectLocationState: createSelectLocationState()
});

fetchDataOnLocationMatch(history, routes, match, store);

const renderApp = (store, props) => {
  render(
    <Provider store={store}>
      <Router {...props} />
    </Provider>,
    document.getElementById(MOUNT_POINT)
  );
};

const renderNotFound = () => {
  match({ history, routes, location: '/404' }, (err, redirect, props) => {
    renderApp(store, props);
  });
};

match({ history, routes }, (error, redirect, props) => {
  if (error) {
    renderNotFound();
  } else if (props) {
    try {
      renderApp(store, props);
    } catch (err) {
      renderNotFound();
    }
  } else {
    renderNotFound();
  }
});