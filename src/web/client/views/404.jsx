import React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';

import Link from '../components/Link';

function View404({ app }) {
  const vavelUrl = '/';

  return (
    <div>
      <Helmet title="VAVEL.com - Not found" />
      <div className="error-page">
        <div className="oops">404</div>
        <h1 className="pdb10" style={{ fontSize: 30, color: '#444' }}>Webpage not found</h1>
        <h2 className="fs16 grey-nine b">P&aacute;gina no encontrada</h2>
        <h2 className="fs16 grey-nine b">Page non trouv&eacute;e</h2>
        <h2 className="fs16 grey-nine b">Pagina non trovata</h2>
        <h2 className="fs16 grey-nine b mgb20 pdb15">P&aacute;gina n&atilde;o encontrada</h2>
        <h2 className="fs16 grey-nine b">{ app.get('description') }</h2>

        <span className="pd10 b fs54" style={{ backgroundColor: '#333' }}>
          <Link to={vavelUrl} style={{ color: '#fff', fontWeight: 'bold', cursor: 'pointer' }}>Go to VAVEL.com</Link>
        </span>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  app: state.get('app')
});

export default connect(mapStateToProps)(View404);