import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import Helmet from 'react-helmet';

import { GridArticles } from '../grid-articles';
import { loadArticles18 } from './articles/actions';


class Articles extends Component {

  static fetchData({ store }) {
    const app = store.getState().get('app');

    return store.dispatch(
      loadArticles18(app.getIn(['edition', 'categoryId']))
    );
  }

  updateNews = (total) => {
    this.props.loadArticles(this.props.categoryId, total);
  }

  getMetaElements(edition) {
    return [
      { name: 'description', content: edition.get('homeDescription') },
      { name: 'keywords', content: edition.get('homeKeywords') },
      { property: 'og:title', content: edition.get('homeTitle') },
      { property: 'og:description', content: edition.get('homeDescription') },
    ];
  }

  render() {
    const { articles, isFetching, edition } = this.props;

    const categoryId = edition.get('categoryId');

    return (
      <div>
        <Helmet
          title={edition.get('homeTitle')}
          meta={this.getMetaElements(edition)}
        />

        <GridArticles
          articles={articles.toJS()}
          isFetching={isFetching}
          loadArticles={this.updateNews}
        />
      </div>
    );
  }
}

const mapStateToProps = (s) => {
  const state = s.get('viewArticles');

  return {
    articles: state.get('articles'),
    isFetching: state.get('isFetching'),
    edition: s.getIn(['app', 'edition'])
  };
};

const mapDispatchToProps = (dispatch) => ({
  loadArticles(categoryId, total) {
    try {
      dispatch(loadArticles18(categoryId, total));
    } catch (err) {
      console.log('--');
      console.log(err);
    }
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Articles);