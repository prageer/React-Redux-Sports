import { createAction } from 'redux-actions';

import {
  SET_VIEW_CATEGORIES,
  ADD_CATEGORIES_ARTICLES,
  ADD_NEW_CATEGORIES_ARTICLES,
  REQUEST_CATEGORY,
  CATEGORY_RECEIVED,
  ERROR_CATEGORY,
  REQUEST_CATEGORIES_ARTICLES,
  CATEGORIES_ARTICLES_RECEIVED,
  ERROR_CATEGORIES_ARTICLES
} from './actionTypes';


export const setViewCategories = createAction(SET_VIEW_CATEGORIES);
export const addCategoriesArticles = createAction(ADD_CATEGORIES_ARTICLES);
export const addNewCategoriesArticles = createAction(ADD_NEW_CATEGORIES_ARTICLES);

export const requestCategory = createAction(REQUEST_CATEGORY);
export const categoryReceived = createAction(CATEGORY_RECEIVED);
export const errorCategory = createAction(ERROR_CATEGORY);

export const requestCategoriesArticles = createAction(REQUEST_CATEGORIES_ARTICLES);
export const categoriesArticlesReceived = createAction(CATEGORIES_ARTICLES_RECEIVED);
export const errorCategoriesArticles = createAction(ERROR_CATEGORIES_ARTICLES);

/**
 * Get category articles.
 *
 * @param {Object} api and dispatch.
 * @param {String} categoryId Category ID.
 * @param {Number} skip Total articles to skip.
 */
const getCategoryArticles = ({ api, dispatch }, categoryId, skip = 0) => {
  dispatch(requestCategoriesArticles());
  dispatch(addNewCategoriesArticles());

  return api.getCategoryArticles(categoryId, skip)
    .then((articles = []) => {
      dispatch(addCategoriesArticles(articles));
      dispatch(categoriesArticlesReceived());
    })
    .catch((err) => {
      console.warn(`Error on getCategoryArticles, categoryId=${categoryId}`);
      console.error(err);
      dispatch(errorCategoriesArticles(err));
    });
};

/**
 * Update category articles.
 */
export const updateCategoriesArticles = () => {
  return function (dispatch, getState, api) {
    const state = getState().get('viewCategories');

    if (state.get('isFetchingArticles')) return;

    dispatch(requestCategoriesArticles());
    const { category, articles } = state;

    return api.getCategoryArticles(category.id, articles.length, 19)
      .then((articles = []) => {
        dispatch(addCategoriesArticles(articles));
        dispatch(categoriesArticlesReceived());
      })
      .catch((err) => {
        console.warn(`Error on updateCategoriesArticles, categoryId=${category.id}`);
        console.error(err);
        dispatch(errorCategoriesArticles(err));
      });
  }
};

/**
 * Load category articles.
 *
 * @param {String} pathname Pathname.
 */
export const loadCategoriesArticles = (pathname) => {
  return function (dispatch, getState, api) {
    dispatch(requestCategory());

    return api.getCategoryByPath(pathname)
      .then((category = []) => {
        if (category && category.length > 0) {
          category = category[0];
          dispatch(setViewCategories(category));
          dispatch(categoryReceived());

          return getCategoryArticles({ api, dispatch }, category.id);
        }

        throw new Error('Categoría no disponible.');
      })
      .catch((err) => {
        console.warn(`Error on loadCategoriesArticles, pathname=${pathname}`);
        console.error(err);
        dispatch(errorCategory(err));
      });
  }
};