import { fromJS, Map as map, List as list } from 'immutable';
import { handleArticles } from '../../grid-articles';

import {
  SET_VIEW_CATEGORIES,
  ADD_CATEGORIES_ARTICLES,
  ADD_NEW_CATEGORIES_ARTICLES,
  REQUEST_CATEGORY,
  CATEGORY_RECEIVED,
  ERROR_CATEGORY,
  REQUEST_CATEGORIES_ARTICLES,
  CATEGORIES_ARTICLES_RECEIVED,
  ERROR_CATEGORIES_ARTICLES
} from './actionTypes';


const initialState = fromJS({
  category: {},
  articles: [],
  isFetchingArticles: false,
  isFetchingCategory: false
});

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_CATEGORIES_ARTICLES:
      return handleArticles(state, action);
      break;

    case ADD_NEW_CATEGORIES_ARTICLES:
      return state.set('articles', list());
      break;

    case SET_VIEW_CATEGORIES:
      return state.set('category', fromJS(action.payload));
      break;

    case REQUEST_CATEGORY:
      return state.set('isFetchingCategory', true);
      break;

    case CATEGORY_RECEIVED:
      return state.set('isFetchingCategory', false);
      break;

    case ERROR_CATEGORY:
      return state.set('isFetchingCategory', false);
      break;

    case REQUEST_CATEGORIES_ARTICLES:
      return state.set('isFetchingArticles', true);
      break;

    case CATEGORIES_ARTICLES_RECEIVED:
      return state.set('isFetchingArticles', false);
      break;

    case ERROR_CATEGORIES_ARTICLES:
      return state.set('isFetchingArticles', false);
      break;

    default:
      return state;
  }
};