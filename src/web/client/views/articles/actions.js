import {
  requestArticles,
  addArticles,
  articlesReceived,
  errorArticles
} from '../../grid-articles';


/**
 * Load articles.
 *
 * @param {string} categoryId Category ID.
 * @param {Number} total Current articles.
 */
export const loadArticles18 = (categoryId, total = 0) => {
  return function (dispatch, getState, api) {
    dispatch(requestArticles());

    return api.getArticles18ByCategoryId(categoryId, total, 19)
      .then((articles = []) => {
        dispatch(addArticles(articles));
        dispatch(articlesReceived());
      })
      .catch((err) => {
        console.warn(`Error on loadArticles18, categoryId='${categoryId}'`);
        console.error(err);
        dispatch(errorArticles(err));
      });
  }
};