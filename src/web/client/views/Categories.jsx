import React, { Component } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

import {
  loadCategoriesArticles,
  updateCategoriesArticles
} from './categories/actions';
import { TwitterBar } from '../header';
import { isBrowser, Str } from '../utils';
import { GridArticles } from '../grid-articles';


class Categories extends Component {

  static fetchData({ store }) {
    const app = store.getState().get('app');

    const location = isBrowser ? window.location.pathname : app.get('location');

    // We add the edition name if the app is running on subdomain.
    // elsewhere, we assume that the location has a prefix (the edition name).
    const pathname = app.get('withSubdomain') ?
      `/${app.get('editionName')}${location}` :
      location;

    return store.dispatch(loadCategoriesArticles(pathname));
  }

  getMetaElements = (name, metaTitle) => {
    return [
      { name: 'description', content: name },
      { name: 'keywords', content: name },
      { name: 'author', content: name },
      { property: 'og:title', content: metaTitle },
      { property: 'og:image', content: 'https://img.vavel.com/vavel-logo.png' },
      { property: 'og:description', content: name },
    ];
  }

  render() {
    const {
      edition, articles, category, isFetchingArticles, loadArticles
    } = this.props;

    const name = category.get('name');
    const metaTitle = Str.format(edition.get('articleTitle'), { title: name });

    return (
      <div>
        <Helmet title={metaTitle} meta={this.getMetaElements(name, metaTitle)} />
        <TwitterBar category={category.toJS()} edition={edition} />
        <div className="center_home">
          <GridArticles
            articles={articles.toJS()}
            isFetching={isFetchingArticles}
            loadArticles={loadArticles}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (s) => {
  const state = s.get('viewCategories');

  return {
    category: state.get('category'),
    articles: state.get('articles'),
    isFetchingArticles: state.get('isFetchingArticles'),
    edition: s.getIn(['app', 'edition'])
  };
};

const mapDispatchToProps = (dispatch) => ({
  loadArticles() {
    dispatch(updateCategoriesArticles());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Categories);