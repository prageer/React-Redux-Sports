import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Map } from 'immutable';

import { newArticles, loadNextArticle } from './article-view/actions';
import ScrollableArticles from '../components/ScrollableArticles';


class ArticleView extends Component {

  static fetchData({ store, params }) {
    return store.dispatch(newArticles(params));
  }

  render() {
    const { articles, isFetching, loadNext } = this.props;

    return (
      <div>
        <ScrollableArticles
          articles={articles}
          isFetching={isFetching}
          onScroll={loadNext}
        />
      </div>
    );
  }
}

const mapStateToProps = (s) => {
  const state = s.get('viewArticle');

  return {
    articles: state.get('articles'),
    isFetching: state.get('isFetching'),
    latestArticles: state.get('latestArticles')
  };
};

const mapDispatchToProps = (dispatch) => ({
  loadNext() {
    dispatch(loadNextArticle());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ArticleView);