import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';

import { getCanonicalUrl, isBrowser } from '../utils';

import { Header } from '../header';
import ToTopArrow from '../components/ToTopArrow';
import { LeftAside, loadMenu } from '../menu-left';


class AppContainer extends Component {

  static fetchData({ store, params }) {
    const app = store.getState().get('app');

    // inject `editionName` in routes.
    params.editionName = app.get('editionName');

    return store.dispatch(loadMenu(app.get('editionName')));
  }

  componentDidUpdate() {
    if (isBrowser) {
      window.scrollTo(0, 0);
    }
  }

  getLinksElements(edition) {
    return [
      { rel: 'canonical', href: getCanonicalUrl(this.props.router.location.pathname) },
      { rel: 'stylesheet', href: '/public/assets/css/app.css' },
      { rel: 'stylesheet', href: edition.get('css') },
    ];
  }

  getMetaElements() {
    return [
      { charset: 'utf-8' },
      { 'http-equiv': 'X-UA-Compatible', content: 'IE=edge' },
      { name: 'description', content: '' },
      { name: 'keywords', content: '' },
      { name: 'author', content: 'VAVEL' },
      { property: 'og:title', content: '' },
      { property: 'og:type', content: 'website' },
      { property: 'og:url', content: '' },
      { property: 'og:site_name', content: 'VAVEL' },
      { property: 'og:description', content: '' },
      { property: 'og:image', content: 'https://img.vavel.com/vavel-logo.png' },
      { property: 'fb:admins', content: '510371807' },
      { property: 'fb:app_id', content: '169320175188' },
      { name: 'alexaVerifyID', content: 'h2oWbBanS6-3pb0lcfbNd6I3Yxw' },
      { name: 'msvalidate.01', content: '0881C6083E8E659661CAE90814031D25' },
      { name: 'HandheldFriendly', content: 'true' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' },
      { name: 'apple-mobile-web-app-capable', content: 'yes' },
      { name: 'apple-mobile-web-app-status-bar-style', content: 'black' },
      { name: 'google-site-verification', content: '2BMNKARPCJxwyYCCdO4YWbACEmU0P2nxk0j9n_Q-XHI' },
      { name: 'google-site-verification', content: 'rLZgTD4BWTdLokTattaFaRqa_yRwgpEv7SmFrJQCNWE' },
    ];
  }

  render() {
    const { app } = this.props;
    const edition = app.get('edition');
    const hasError = app.get('hasError');

    return (
      <div>
        <Helmet
          link={this.getLinksElements(edition)}
          meta={this.getMetaElements()}
        />

        <LeftAside />

        <div id="sb-site" className="sb-site-container">
          <div id="container_main_page">
            <Header />

            {
              hasError ?
              this.props.loadError() :
              this.props.children
            }
          </div>

          <ToTopArrow />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    app: state.get('app')
  };
};

const mapDispatchToProps = (dispatch) => ({
  loadError() {
    const Error = require('./404').default;
    return <Error />;
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppContainer);