import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';

import { GridArticles } from '../grid-articles';
import { getAuthorImageUrl, getAuthorUrl, Str } from '../utils';
import { loadAuthorArticles, loadAuthorCountArticles } from './author/actions';


class Author extends Component {

  static fetchData({ store, params }) {
    console.log('Fetching data');
    return Promise.all([
      store.dispatch(loadAuthorArticles(params.author, 0)),
      store.dispatch(loadAuthorCountArticles(params.author)),
    ]);
  }

  getMetaElements = (author, metaTitle, imageUrl) => {
    return [
      { name: 'description', content: author.bio || 'default' },
      { name: 'keywords', content: author.name },
      { name: 'author', content: author.name },
      { property: 'og:title', content: metaTitle },
      { property: 'og:image', content: imageUrl },
      { property: 'og:description', content: author.bio && author.bio.length ? author.name + ' ' + author.bio : author.name },
    ];
  }

  loadNews = (total) => {
    const authorUsername = this.props.author.get('username');
    this.props.loadArticles(authorUsername, total);
  };

  render() {
    const {
      author: prevAuthor, articles, edition,
      isFetching, countArticles
    } = this.props;

    const author = prevAuthor.toJS();

    const imageUrl = getAuthorImageUrl([ '1Thumb', author, 'default' ]);
    const metaTitle = Str.format(edition.get('articleTitle'), { title: author.name });

    return (
      <section itemScope="" className="author_zone">
        <Helmet
          title={metaTitle}
          meta={this.getMetaElements(author, metaTitle, imageUrl)}
        />

        {/* TODO: Si author es authorZone no mostrar header */}

        {author && <div className="author_zone_container">
          <div className="author_profile">
            <div className="author_image">
              <a href={imageUrl} title={author.name} target="_blank">
                <img src={imageUrl} alt={author.name} height="120" itemProp="image" />
              </a>
            </div>
            <div className="author">
              <h1 itemProp="name" className="name">
                <a href={getAuthorUrl(author)} dangerouslySetInnerHTML={{ __html: author.name }} />
                {
                  author.ocupation ?
                    <div className="regular_text mgb10" dangerouslySetInnerHTML={{ __html: author.ocupation }} /> :
                    <div className="regular_text mgb10" dangerouslySetInnerHTML={{ __html: edition.get('authorInVavel') }} />
                }
              </h1>
              <br />
            </div>
          </div>

          {author.bio && <div className="author_bio">
            <span itemProp="description" dangerouslySetInnerHTML={{ __html: author.bio }} />
            {author.www && <div className="www_url pdt10 mgb20">
              <a href={author.www} rel="nofollow" target="_blank">
                <b dangerouslySetInnerHTML={{ __html: author.www }} />
              </a>
            </div>}
          </div>}

          <div className="author_bar_full">
            <div className="submenu_liquid">
              <div className="fltl mgr5p">
                <img alt="" src="/public/assets/img/vavel-small-icon.jpg" className="fltl" />
                <div className="author_bar_module" dangerouslySetInnerHTML={{ __html: edition.get('writer') }} />
              </div>
              <div className="fltl mgr5p">
                <img alt="" src="/public/assets/img/vavel-small-icon.jpg" className="fltl" />
                <div className="author_bar_module2">{edition.get('articles')}</div>
                <div className="author_bar_text">{countArticles.get('count')}</div>
              </div>
              <div className="author_bar_module3">
                {author.twitter &&
                  <a
                    href={`http://www.twitter.com/${author.twitter}`}
                    itemProp="url"
                    rel="nofollow"
                    style={{ textDecoration: 'none' }}
                    target="_blank"
                  >
                    <img
                      src="/public/assets/img/tw25.png" alt="Twitter"
                      style={{
                        border: 0,
                        width: '16px',
                        marginTop: '7px',
                        marginLeft: '10px',
                        height: '16px',
                        float: 'left'
                      }}
                    />
                  </a>}
                {author.plus &&
                  <a href={author.plus} itemProp="url" style={{ textDecoration: 'none' }} target="_blank">
                    <img
                      src="/public/assets/img/gplus-16.png"
                      alt="Google+"
                      style={{
                        border: 0,
                        width: '16px',
                        marginTop: '7px',
                        marginLeft: '10px',
                        height: '16px',
                        float: 'left'
                      }}
                    />
                  </a>}
              </div>
            </div>
          </div>

        </div>}

        <div className="main_news_category">
          <div className="center_home">
            <GridArticles
              articles={articles.toJS()}
              isFetching={isFetching}
              loadArticles={this.loadNews}
            />
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (s) => {
  const state = s.get('viewAuthor');

  return {
    author: state.get('author'),
    articles: state.get('articles'),
    countArticles: state.get('countArticles'),
    isFetching: state.get('isFetchingArticles'),
    edition: s.getIn(['app', 'edition'])
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  loadArticles(authorName, total) {
    dispatch(loadAuthorArticles(authorName, total));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Author);