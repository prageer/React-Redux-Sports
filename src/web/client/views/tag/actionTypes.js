export const SET_VIEW_TAG = 'SET_VIEW_TAG';
export const ADD_TAG_ARTICLES = 'ADD_TAG_ARTICLES';

export const REQUEST_TAG_ARTICLES = 'REQUEST_TAG_ARTICLES';
export const TAG_ARTICLES_RECEIVED = 'TAG_ARTICLES_RECEIVED';
export const ERROR_TAG_ARTICLES = 'ERROR_TAG_ARTICLES';