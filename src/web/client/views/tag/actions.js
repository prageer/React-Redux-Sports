import { createAction } from 'redux-actions';

import {
  SET_VIEW_TAG,
  ADD_TAG_ARTICLES,
  REQUEST_TAG_ARTICLES,
  TAG_ARTICLES_RECEIVED,
  ERROR_TAG_ARTICLES
} from './actionTypes';


export const setViewTag = createAction(SET_VIEW_TAG);
export const addTagArticles = createAction(ADD_TAG_ARTICLES);

export const requestTagArticles = createAction(REQUEST_TAG_ARTICLES);
export const tagArticlesReceived = createAction(TAG_ARTICLES_RECEIVED);
export const errorTagArticles = createAction(ERROR_TAG_ARTICLES);

/**
 * Update articles tag.
 *
 * @param {String} lang Lang (editionName).
 */
export const updateArticlesTag = (lang) => {
  return function (dispatch, getState, api) {
    const state = getState().get('viewTag');

    if (state.get('isFetching')) return;

    dispatch(requestTagArticles());

    // We take the tag name from `sefriendly` property.
    const tagSefriendly = state.getIn(['tag', 'sefriendly']);

    return api.getTagArticles(
      tagSefriendly, lang, state.get('articles').size, 20
    )
    .then((articles = []) => {
      dispatch(addTagArticles(articles));
      dispatch((tagArticlesReceived()));
    })
    .catch((err) => {
      console.warn(`Error on updateArticlesTag, tag=${tagSefriendly}, lang=${lang}`);
      dispatch(errorTagArticles(err));
    });
  }
};

/**
 * Load articles tag.
 *
 * @param {String} tag Tag.
 * @param {Lang} lang Lang (editionName).
 */
export const loadArticlesTag = (tag, lang) => {
  return function (dispatch, getState, api) {
    dispatch(requestTagArticles());

    return api.getTagArticles(tag, lang, 0, 20)
      .then((articles = []) => {
        if (articles.length > 0) {
          const currentTag = articles[0].tags.filter((t) => {
            if (!t.sefriendly) return false;
            return t.sefriendly.search(new RegExp(tag, 'i')) !== -1;
          });

          if (currentTag.length > 0) {
            dispatch(setViewTag(currentTag[0]));
          } else {
            console.warn('Tag para artículos no válida.');
            throw new Error('Tag para artículos no válida.');
          }

          dispatch(addTagArticles(articles));
          dispatch(tagArticlesReceived());
        } else {
          console.log('Tag no encontrada.');
          throw new Error('Tag no encontrada.');
        }
      })
      .catch((err) => {
        console.warn(`Error on loadArticlesTag, tag=${tag}, lang=${lang}`);
        dispatch(errorTagArticles(err));
      });
  }
};