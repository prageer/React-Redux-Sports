import { fromJS } from 'immutable';

import { handleArticles } from '../../grid-articles';
import {
  SET_VIEW_TAG,
  ADD_TAG_ARTICLES,
  REQUEST_TAG_ARTICLES,
  TAG_ARTICLES_RECEIVED,
  ERROR_TAG_ARTICLES
} from './actionTypes';


const initialState = fromJS({
  tag: {},
  articles: [],
  isFetching: false
});

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TAG_ARTICLES:
      return handleArticles(state, action);
      break;

    case SET_VIEW_TAG:
      return state.set('tag', fromJS(action.payload));
      break;

    case REQUEST_TAG_ARTICLES:
      return state.set('isFetching', true);
      break;

    case TAG_ARTICLES_RECEIVED:
      return state.set('isFetching', false);
      break;

    case ERROR_TAG_ARTICLES:
      return state.set('isFetching', false);
      break;

    default:
      return state;
  }
};