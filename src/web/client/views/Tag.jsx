import React, { Component } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

import { Str } from '../utils';
import { GridArticles } from '../grid-articles';
import { loadArticlesTag, updateArticlesTag } from './tag/actions';


class Tag extends Component {

  static fetchData({ store, params }) {
    return store.dispatch(loadArticlesTag(params.tag, params.editionName));
  }

  updateNews = () => {
    this.props.loadArticles(this.props.edition.cultureShortcut);
  }

  getMetaElements(tagName, metaTitle) {
    return [
      { name: 'description', content: tagName },
      { name: 'keywords', content: tagName },
      { name: 'author', content: tagName },
      { property: 'og:title', content: metaTitle },
      { property: 'og:image', content: 'https://img.vavel.com/vavel/-logo.png' },
      { property: 'og:description', content: tagName },
    ];
  }

  render() {
    const { edition, tag, articles, isFetching } = this.props;

    const title = edition.get('latestArticlesTaggedNews').replace('{tag}', '');

    const tagName = tag.get('name');
    const metaTitle = Str.format(edition.get('articleTitle'), { title: tagName });

    return (
      <div>
        <Helmet
          title={metaTitle}
          meta={this.getMetaElements(tagName, metaTitle)}
        />
        <div className="tags_bar">
          <div className="tags_bar_container">
            <div className="fltl pdr5 tags_word" dangerouslySetInnerHTML={{ __html: title }} />
            <h1 dangerouslySetInnerHTML={{ __html: tagName }} />
          </div>
        </div>
        <div className="center_home">
          <GridArticles
            articles={articles.toJS()}
            isFetching={isFetching}
            loadArticles={this.updateNews}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (s) => {
  const state = s.get('viewTag');

  return {
    tag: state.get('tag'),
    articles: state.get('articles'),
    isFetching: state.get('isFetching'),
    edition: s.getIn(['app', 'edition'])
  };
};

const mapDispatchToProps = (dispatch) => ({
  loadArticles(lang) {
    dispatch(updateArticlesTag(lang));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tag);