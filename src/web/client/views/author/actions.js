import { createAction } from 'redux-actions';

import { Obj } from '../../utils';

import {
  SET_AUTHOR,
  ADD_AUTHOR_ARTICLES,
  AUTHOR_COUNT_ARTICLES,
  REQUEST_AUTHOR_ARTICLES,
  AUTHOR_ARTICLES_RECEIVED,
  ERROR_AUTHOR_ARTICLES,
  REQUEST_AUTHOR,
  AUTHOR_RECEIVED,
  ERROR_AUTHOR
} from './actionTypes';


export const addAuthorArticles = createAction(ADD_AUTHOR_ARTICLES);
export const authorCountArticles = createAction(AUTHOR_COUNT_ARTICLES);

export const requestAuthorArticles = createAction(REQUEST_AUTHOR_ARTICLES);
export const authorArticlesReceived = createAction(AUTHOR_ARTICLES_RECEIVED);
export const errorAuthorArticles = createAction(ERROR_AUTHOR_ARTICLES);

export const setAuthor = createAction(SET_AUTHOR);

export const requestAuthor = createAction(REQUEST_AUTHOR);
export const authorReceived = createAction(AUTHOR_RECEIVED);
export const errorAuthor = createAction(ERROR_AUTHOR);

/**
 * Load total of articles of a given author.
 *
 * @param {String} authorName Author name.
 */
export const loadAuthorCountArticles = (authorName) => {
  return function (dispatch, getState, api) {
    return api.getAuthorCountArticles(authorName)
      .then((countArticles = {}) => {
        dispatch(authorCountArticles(countArticles));
      });
  }
};

/**
 * Load author articles.
 *
 * @param {String} authorName Author name.
 * @param {Number} total Current articles.
 */
export const loadAuthorArticles = (authorName, total = 0) => {
  return function (dispatch, getState, api) {
    const state = getState().get('viewAuthor');
    const actualAuthorName = state.getIn(['author', 'name']);

    dispatch(requestAuthorArticles());

    return api.getAuthorArticles(authorName, total)
      .then((articles = []) => {
        const author = articles.length > 0 ?
          articles[0].author :
          {};

        if (!Obj.isEmpty(author)) {
          if (actualAuthorName !== author.name) {
            dispatch(setAuthor(author));
          }
        } else {
          throw new Error('Author not found.'); // TODO: Lanzar NotFoundHttpException
        }

        dispatch(addAuthorArticles(articles));
        dispatch(authorArticlesReceived());
      })
      .catch((err) => {
        console.warn(`Error on loadAuthorArticles, authorName='${authorName}'`);
        console.error(err);
        dispatch(errorAuthorArticles(err));
      });
  }
};