import { fromJS } from 'immutable';

import { handleArticles } from '../../grid-articles';

import {
  SET_AUTHOR,
  ADD_AUTHOR_ARTICLES,
  AUTHOR_COUNT_ARTICLES,
  REQUEST_AUTHOR_ARTICLES,
  AUTHOR_ARTICLES_RECEIVED,
  ERROR_AUTHOR_ARTICLES
} from './actionTypes';


const initialState = fromJS({
  author: {},
  edition: {},
  articles: [],
  countArticles: {},
  isFetchingAuthor: false,
  isFetchingArticles: false
});

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_AUTHOR_ARTICLES:
      return handleArticles(state, action);
      break;

    case SET_AUTHOR:
      return state.set('author', fromJS(action.payload));
      break;

    case AUTHOR_COUNT_ARTICLES:
      return state.set('countArticles', fromJS(action.payload));
      break;

    case REQUEST_AUTHOR_ARTICLES:
      return state.set('isFetchingArticles', true);
      break;

    case AUTHOR_ARTICLES_RECEIVED:
      return state.set('isFetchingArticles', false);
      break;

    case ERROR_AUTHOR_ARTICLES:
      return state.set('isFetchingArticles', false);
      break;

    default:
      return state;
  }
};