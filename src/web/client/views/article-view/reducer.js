import { fromJS, List as list, Map as map } from 'immutable';

import {
  REQUEST_MINI_ARTICLES,
  MINI_ARTICLES_RECEIVED,
  ERROR_MINI_ARTICLES,
  SET_MINI_ARTICLES,
  SET_LATEST_ARTICLES as SET_LATEST_ARTICLES_VIEW_ARTICLE,
  SET_CATEGORY_ARTICLE,
  REQUEST_RATING_ARTICLE,
  RATING_ARTICLE_RECEIVED,
  ERROR_RATING_ARTICLE,
  SET_RATING_ARTICLE,
  REQUEST_RATE_ARTICLE,
  RATE_ARTICLE_RECEIVED,
  ERROR_RATE_ARTICLE,
  findArticle,
  reducer as reduceArticle,
  initialState as articleInitialState
} from '../../components/article';
import {
  ADD_NEXT_ARTICLE,
  ADD_NEXT_ARTICLE_CONTENT,
  REQUEST_NEXT_ARTICLE_CONTENT,
  NEXT_ARTICLE_CONTENT_RECEIVED,
  ERROR_NEXT_ARTICLE_CONTENT,
  SET_LATEST_ARTICLES,
  SET_LAST_VISITED_ARTICLE_ID,
  NEW_VIEW_ARTICLES
} from './actionTypes';


const initialState = fromJS({
  lastVisitedArticleId: -1,
  latestArticles: [],
  isFetching: false,
  articles: []
});

export const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case NEW_VIEW_ARTICLES:
      return state.set('articles', list());
      break;

    case ADD_NEXT_ARTICLE:
      return state.update(
        'articles',
        (articles) => {
          const article = articleInitialState.merge(fromJS(payload));
          return articles.push(article);
        }
      );
      console.log('added');
      break;

    case REQUEST_NEXT_ARTICLE_CONTENT:
      return state.set('isFetching', true);
      break;

    case NEXT_ARTICLE_CONTENT_RECEIVED:
      return state.set('isFetching', false);
      break;

    case ERROR_NEXT_ARTICLE_CONTENT:
      return state.set('isFetching', false);
      break;

    case SET_LATEST_ARTICLES:
      return state.set('latestArticles', fromJS(payload));
      break;

    case SET_LAST_VISITED_ARTICLE_ID:
      return state.set('lastVisitedArticleId', payload);
      break;

    case REQUEST_MINI_ARTICLES:
    case MINI_ARTICLES_RECEIVED:
    case ERROR_MINI_ARTICLES:
    case SET_MINI_ARTICLES:
    case SET_LATEST_ARTICLES_VIEW_ARTICLE:
    case SET_CATEGORY_ARTICLE:
    case REQUEST_RATING_ARTICLE:
    case RATING_ARTICLE_RECEIVED:
    case ERROR_RATING_ARTICLE:
    case SET_RATING_ARTICLE:
    case REQUEST_RATE_ARTICLE:
    case RATE_ARTICLE_RECEIVED:
    case ERROR_RATE_ARTICLE:
      return state.update(
        'articles',
        (articles) => articles.update(
          articles.findIndex((article) => article.article_id === payload.article_id),
          (article) => reduceArticle(article, { type, payload })
        )
      );
      break;

    default:
      return state;
  }
};