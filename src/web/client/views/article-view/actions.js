import { createAction } from 'redux-actions';
import { extractArticleId, getArticleUrl } from '../../utils';

import {
  ADD_NEXT_ARTICLE,
  // ADD_NEXT_ARTICLE_CONTENT,
  REQUEST_NEXT_ARTICLE_CONTENT,
  NEXT_ARTICLE_CONTENT_RECEIVED,
  ERROR_NEXT_ARTICLE_CONTENT,
  SET_LATEST_ARTICLES,
  SET_LAST_VISITED_ARTICLE_ID,
  NEW_VIEW_ARTICLES
} from './actionTypes';


export const addNextArticle = createAction(ADD_NEXT_ARTICLE);
// export const addNextArticleContent = createAction(ADD_NEXT_ARTICLE_CONTENT);
export const requestNextArticleContent = createAction(REQUEST_NEXT_ARTICLE_CONTENT);
export const nextArticleContentReceived = createAction(NEXT_ARTICLE_CONTENT_RECEIVED);
export const errorNextArticleContent = createAction(ERROR_NEXT_ARTICLE_CONTENT);

export const setLatestArticles = createAction(SET_LATEST_ARTICLES);
export const setLastVisitedArticleId = createAction(SET_LAST_VISITED_ARTICLE_ID);
export const newViewArticles = createAction(NEW_VIEW_ARTICLES);

/**
 * Get the last (related) article.
 *
 * @param {Array} latests Latest articles.
 */
const getRelatedArticle = (latests) => {
  let relatedArticle = null;
  if (latests.length > 0) relatedArticle = latests.shift();
  return relatedArticle || {};
};

// pointer to have control on array.
let pointer = -1;

/**
 * Load next article.
 */
export const loadNextArticle = () => {
  return function (dispatch, getState, api) {
    const state = getState().get('viewArticle');

    const latests = state.get('latestArticles').toJS();
    const articles = state.get('articles').toJS();

    if (state.get('isFetching') || pointer >= latests.length - 1) {
      return;
    }

    pointer++;
    const nextArticle = latests[pointer];

    if (!nextArticle) {
      return
    };

    dispatch(requestNextArticleContent());
    const totalArticles = articles.length - 1;
    const currentArticle = articles[totalArticles <= 0 ? 0 : totalArticles];

    if (nextArticle._id === currentArticle._id ||
        nextArticle._id === articles[0]._id) {
      pointer++;
      if (pointer > latests.length - 1) {
        dispatch(nextArticleContentReceived());
        return;
      }
    }

    const content = latests[pointer];

    dispatch(addNextArticle({
      content,
      latestArticles: articles[0].latestArticles,
      relatedArticle: getRelatedArticle(latests)
    }));
    dispatch(nextArticleContentReceived());
    history.replaceState({}, null, getArticleUrl(content));
  }
};

/**
 * Load article content.
 *
 * @param {Api} api App API.
 * @param {string} articleId Article ID.
 */
const loadContent = (api, articleId) => {
  return api.getArticleById(articleId)
    .then((content = {}) => {
      if (content && content.category) return content;
      throw new Error(`loadContent: cannot fetch content of articleId=${articleId}`);
    });
};

/**
 * Load new articles.
 *
 * @param {params} routeParams Route params
 */
export const newArticles = ({ articleId: aId, category, editionName, splat }) => {
  return function (dispatch, getState, api) {
    console.log('Adding new article to view...');
    const articleId = extractArticleId(aId);

    const state = getState().get('viewArticle');
    const lastVisitedArticleId = state.get('lastVisitedArticleId');

    if (lastVisitedArticleId === articleId) return;

    dispatch(newViewArticles());
    dispatch(requestNextArticleContent());

    return loadContent(api, articleId)
      .then((content) => {
        return Promise.all([
          api.getContentLatestArticles(content.category.id, articleId)
        ])
        .then((data) => {
          dispatch(addNextArticle({
            content,
            latestArticles: data[0],
            relatedArticle: getRelatedArticle(data[0])
          }));

          dispatch(setLatestArticles(data[0]));
          dispatch(setLastVisitedArticleId(articleId));
          dispatch(nextArticleContentReceived());
        });
      })
      .then(() => {
        pointer = -1;
      })
      .catch((err) => {
        console.warn(`Error on newArticles, articleId=${articleId}`);
        console.error(err);
        dispatch(errorNextArticleContent(err));
      });
  }
};