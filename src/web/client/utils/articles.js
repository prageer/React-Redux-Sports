/**
 *
 * @param category
 * @returns {*}
 */
export function getArticleCategoryUrl(category) {
  if (!category) return '';

  if (category.get) return category.get('parent') + '/' + category.get('sefriendly') + '/';

  return category.parent + '/' + category.sefriendly + '/';
}

/**
 *
 * @param article
 * @returns {string}
 */
export function getArticleUrl(article) {
  if (!article) return '';
  let url = article.get ?
    getArticleCategoryUrl(article.get('category')) :
    getArticleCategoryUrl(article.category);

  if (!url) return null;

  if (article.get) {
    const sefriendly = article.get('sefriendly');
    url += article.get('article_id') + (sefriendly ? '-' + sefriendly : '') + '.html';
  } else {
    url += article.article_id + (article.sefriendly ? '-' + article.sefriendly : '') + '.html';
  }

  return url;
}

/**
 *
 * @param article
 * @returns {string}
 */
export function getArticleAuthorUrl(article) {
  return `/author/${article.author.username}`;
}

/**
 *
 * @param str
 * @returns {string}
 */
export function unescape(str) {
  return str ? str.replace(/&#039;/g, "'") : str;
}

/**
 *
 * @param str
 * @returns {string}
 */
export function rfc3986EncodeURIComponent(str) {
  return encodeURIComponent(str).replace(/[!'()*]/g, escape);
}
/**
 *
 */
const imagesConfig = {
  imgCdn1: 'https://img.vavel.com/',
  imgCdn1Thumb: 'https://img.vavel.com/thumb_',
  imgCdn100: 'https://img.vavel.com/thumb_',
  // imgCdn140: 'https://img.vavel.com/small_',
  imgCdn200: 'https://img.vavel.com/h200_',
  imgCdn210: 'https://img.vavel.com/small_',
  imgCdn300: 'https://img.vavel.com/h170_',
  imgCdn700: 'https://img.vavel.com/c700_373_',
  imgCdnh170: 'https://img.vavel.com/c320_170_',
  //  imgCdn321: 'https://img.vavel.com/h200_',
  imgCdn2C: 'https://img.vavel.com/',
  imgCdnSlider: 'https://img.vavel.com/'
};

/**
 *
 * @param imgData
 * @returns {*}
 */
export function getImgUrl(imgData) {
  if (!imgData[1]) return;

  return imagesConfig['imgCdn' + imgData[0]] + imgData[1];
}

/**
 *
 * @param category
 * @returns {string}
 */
export function getCategoryUrl(category) {
  return `${category.parent}/${category.sefriendly}/`;
}

export function getAuthorImageUrl(imgData) {
  const picture = imgData[1].picture;
  const defaultImg = imgData[2] === 'circle' ?
    '/public/assets/img/vavel-circle-flag4.png' :
    '/public/assets/img/logo-vavel.png';
  return picture && picture.length ? imagesConfig['imgCdn' + imgData[0]] + picture : defaultImg;
}

export function getAuthorUrl(author) {
  return `/author/${author.username}`;
}

export function isAuthorArticleZone(article) {
  return article.status === 90;
}

export function getArticleAuthorImageUrl(imgData) {
  imgData[1] = imgData[1].author;
  return getAuthorImageUrl(imgData);
}

export function getTagUrl(tag, cultureShortcut) {
  let prev = tag.edition ? `/${tag.edition}` : `/${cultureShortcut}`;
  prev += `/tag/${tag.sefriendly}`;
  return prev;
}

/**
 * Extrae el ID de un artículo a partir de un pathname.
 * Usado generalmente desde las rutas.
 *
 * @param {String} pathname Pathname del artículo.
 */
export const extractArticleId = (pathname = '') => pathname.split('-')[0];