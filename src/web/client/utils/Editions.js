export const SPANISH = Symbol('spanish');
export const ENGLISH = Symbol('english');
export const PORTUGUESE = Symbol('portuguese');
export const ITALIAN = Symbol('italian');
export const FRENCH = Symbol('french');
export const ARABIAN = Symbol('arabian');
export const GERMAN = Symbol('german');
export const RUSIAN = Symbol('rusian');

export class Editions {

  static _editions = {
    [SPANISH]: [
      'es', 'lat', 'ar', 'mx',
    ],
    [ENGLISH]: [
      'en', 'en-us',
    ],
    [PORTUGUESE]: [
      'br', 'pt',
    ],
    [ITALIAN]: [
      'it',
    ],
    [FRENCH]: [
      'fr',
    ],
    [ARABIAN]: [
      'arb',
    ],
    [GERMAN]: [
      'de',
    ]
  };

  _subdomains = {
    es: { culture: 'es', newsName: 'VAVEL España', translationFile: 'es', language: SPANISH },
    mx: { culture: 'es-419', newsName: 'VAVEL México', translationFile: 'mx', language: SPANISH },
    ar: { culture: 'es-419', newsName: 'VAVEL Argentina', translationFile: 'ar', language: SPANISH },
    lat: { culture: 'es-419', newsName: 'VAVEL Latinoamérica', translationFile: 'lat', language: SPANISH },
    // col: {
    //   culture: 'es-419',
    //   newsName: 'VAVEL Colombia',
    //   translationFile: 'col',
    //   language: SPANISH
    // },

    // United Kingdom
    en: { culture: 'en', newsName: 'Vavel', translationFile: 'en', language: ENGLISH },
    // United States
    'en-us': { culture: 'en-us', newsName: 'VAVEL USA', translationFile: 'en-us', language: ENGLISH },

    br: { culture: 'br', newsName: 'VAVEL Brasil', translationFile: 'br', language: PORTUGUESE },
    pt: { culture: 'pt', newsName: 'VAVEL Portugal', translationFile: 'pt', language: PORTUGUESE },

    it: { culture: 'it', newsName: 'VAVEL Italia', translationFile: 'it', language: ITALIAN },

    fr: { culture: 'fr', newsName: 'VAVEL Francia', translationFile: 'fr', language: FRENCH },

    arb: { culture: 'arb', newsName: 'VAVEL Arabia', translationFile: 'arb', language: ARABIAN },

    de: { culture: 'de', newsName: 'VAVEL Alemania', translationFile: 'de', language: GERMAN }

    // TODO: Agregar Rusia.
  };

  /**
   * Determina si un subdominio existe.
   *
   * @param {String} subdomain Subdominio.
   */
  has(subdomain = '') {
    return !subdomain ?
      false :
      this._subdomains[String(subdomain).toString().toLowerCase()] !== undefined;
  }

  /**
   * Obtiene el objeto completo de un subdominio.
   *
   * @param {String} subdomain Subdominio.
   */
  getEdition = subdomain => this._subdomainProp(subdomain, true);

  /**
   * Obtiene la cultura de un subdominio.
   *
   * @param {String} subdomain Subdominio.
   */
  getEditionCulture = subdomain => this._subdomainProp(subdomain, 'culture');

  /**
   * Obtiene el nombre del noticiero de un subdominio.
   *
   * @param {String} subdomain Subdominio.
   */
  getEditionsNewsName = subdomain => this._subdomainProp(subdomain, 'newsName');

  /**
   * Obtiene el nombre del archivo de traduccion de un subdominio.
   *
   * @param {String} subdomain Subdominio.
   */
  getEditionTranslationFile = subdomain => this._subdomainProp(subdomain, 'translationFile');

  /**
   * Obtiene el lenguaje de un subdominio.
   *
   * @param {String} subdomain Subdominio.
   */
  getEditionLanguage = subdomain => this._subdomainProp(subdomain, 'language');

  /**
   * Obtiene las ediciones para un subdominio.
   *
   * @param {String} subdomain Subdominio.
   */
  getEditionsTo(subdomain) {
    const edition = this.getEdition(subdomain);

    if (!edition) return [];

    return Editions._editions[edition.language];
  }

  /**
   * Obtiene una propiedad de un subdominio.
   *
   * @param {String} subdomain Subdomain.
   * @param {String|Boolean} propName Nombre de la propiedad en `_subdomains`.
   * Si `propName` es booleano y verdadero se retorna el objeto completo.
   */
  _subdomainProp(subdomain, propName) {
    subdomain = this._normalizeSubdomain(subdomain);

    const wholeObject = typeof propName === 'boolean' && propName;

    if (!subdomain) return;

    const edition = this._subdomains[subdomain];

    return wholeObject ? edition : edition[propName];
  }

  /**
   * Normaliza el nombre del subdominio.
   *
   * @param {String} subdomain Subdomain.
   */
  _normalizeSubdomain(subdomain = '') {
    try {
      return subdomain.toLowerCase();
    } catch (err) {
      return '';
    }
  }
}

export default new Editions;