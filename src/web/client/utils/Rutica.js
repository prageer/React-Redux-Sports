export class Rutica {

  /**
   * Partes de la ruta, aquí se almacena el
   * Array devuelto por el método split.
   *
   * @type {Array}
   * @private
   */
  _parts = [];

  /**
   * Determina si la ruta es o no absoluta.
   *
   * @type {boolean}
   * @private
   */
  _isAbsolute = false;

  /**
   * Pathname de la ruta.
   *
   * @type {string}
   * @private
   */
  _pathname = '';

  /**
   * Separador para dividir y unir las URLs y los pathnames.
   *
   * @type {string}
   */
  static SEPARATOR = '/';

  constructor(url) {
    if (!url || typeof url !== 'string') {
      throw new TypeError(`[Rutica] ${url} incorrecta...`);
    }
    this.init(url);
  }

  init(url) {
    const parts = url.split(Rutica.SEPARATOR);
    this._isAbsolute = this._checkAbsolute(parts[0]);

    if (this.isAbsolute) {
      this._parts = parts;
      this.pathname = this._getSubpaths(parts, 3);
    } else {
      this.pathname = url;
    }
  }

  /**
   * Partes de la ruta.
   *
   * @returns {Array}
   */
  get parts() { return this._parts; }

  /**
   * Indica si la ruta es absoluta.
   *
   * @returns {boolean}
   */
  get isAbsolute() { return this._isAbsolute; }

  /**
   * Pathname de la ruta.
   *
   * @returns {string}
   */
  get pathname() {
    const url = this._build(this.isAbsolute);

    return this._checkAbsolute(url) ?
      this._extractPathname(url) :
      this._prefixPathname(url);
  }

  /**
   * Establece el pathname de la ruta.
   *
   * @param newPathname
   */
  set pathname(newPathname) {
    this._pathname = this._prefixPathname(Rutica._normalize(newPathname));
  }

  extractSubpath(index = 1, overwrite = true) {
    const subpaths = this.pathname.split(Rutica.SEPARATOR);
    const subpath = subpaths.splice(index, 1)[0];

    if (overwrite) {
      this._parts = subpaths;
      this.pathname = this._prefixPathname(this._getSubpaths(subpaths, 0));
    }

    return subpath;
  }

  /**
   * Convierte la ruta actual en una ruta con un host.
   *
   * @param host Nuevo host para la ruta.
   * @returns {Rutica}
   */
  toAbsolute(host) {
    if (!host) {
      console.warn('[Rutica] Debe proporcionar un host para obtener la ruta absoluta');
      return;
    }

    if (!this._checkAbsolute(host)) {
      console.warn('[Rutica] El host debe comenzar por http(s)');
      return;
    }

    return Rutica.create(`${Rutica._normalize(host)}${this.pathname}`);
  }

  /**
   * Convierte la ruta actual en una ruta relativa (solo el pathname).
   *
   * @returns {Rutica}
   */
  toRelative() { return Rutica.create(this.pathname); }

  /**
   * Obtiene una representación en cadena de la ruta actual.
   *
   * @returns {string|string|void|any|string}
   */
  toString() { return this._build(this.isAbsolute); }

  /**
   * Obtiene el pathname de una ruta.
   *
   * @param url URL a extraer el pathname.
   * @returns {string} Pathname.
   * @private
   */
  _extractPathname(url) {
    const pathname = this.isAbsolute ?
      this._getSubpaths(url, 3) :
      url;

    return this._prefixPathname(pathname);
  }

  /**
   * Obtiene una lista de subdirectorios de un
   * pathname a partir de un índice.
   *
   * @param url URL
   * @param index Índice de inicio.
   * @returns {string}
   * @private
   */
  _getSubpaths(url, index = 3) {
    return Array.isArray(url) ?
      url.slice(index).join(Rutica.SEPARATOR) :
      url.split(Rutica.SEPARATOR).slice(index).join(Rutica.SEPARATOR);
  }

  /**
   * Agrega un slash como prefijo a un pathname.
   *
   * @param pathname Pathname.
   * @returns {string}
   * @private
   */
  _prefixPathname(pathname) {
    return pathname.startsWith(Rutica.SEPARATOR) ? pathname : `/${pathname}`;
  }

  /**
   * Genera la URL o el pathname correspondiente para la ruta.
   *
   * @param absolute Indica si la ruta es absoluta.
   * @returns {string|string|void|any|string}
   * @private
   */
  _build(absolute) {
    return absolute ?
      Rutica._normalize(this.parts.join(Rutica.SEPARATOR)) :
      this._extractPathname(this._pathname);
  }

  /**
   * Crea una instancia de una ruta.
   *
   * @param url URL de la ruta.
   * @returns {Rutica}
   */
  static create(url) { return new Rutica(url); }

  /**
   * Determina si una URL es absoluta.
   *
   * @param url URL
   * @returns {boolean}
   * @private
   */
  _checkAbsolute(url) {
    return url.toLowerCase().match(/^https?:/) !== null;
  }

  /**
   * Normaliza la ruta/pathname eliminando los
   * caracteres `/` en ambos lados de la cadena.
   *
   * @param str Cadena a normalizar.
   * @returns {String} Cadena normalizada.
   * @private
   */
  static _normalize(str) {
    if (!str) return '';
    // taken from: https://www.w3schools.com/jsref/jsref_trim_string.asp
    return str.replace(/^\/+|\/+$/gm, '') || '/';
  }
}

export default Rutica;