import { Route } from './Route';
import { Rutica} from './Rutica';

import { WITH_SUBDOMAIN } from '../../rules';


export const isBrowser = typeof window !== 'undefined';

export const isServer = !isBrowser;

/**
 * Prefix the route path. Allowing us
 * to work optionally with subdomains.
 *
 * @param {String} pathname Path to prefix.
 */
export const prefixRoute = (pathname) => {
  let routePath = WITH_SUBDOMAIN ?
    pathname :
    `/:editionName`;

  // cut slash if pathname is root.
  routePath += pathname === '/' ? '' : pathname;

  return routePath;
};

/**
 * Load a view in a async way.
 *
 * @param {String} viewName - Nombre de vista.
 */
export function view(viewName) {
  if (isServer) {
    // Polyfill
    if(typeof require.ensure !== 'function') require.ensure = function(d, c) { c(require) };
  }

  return (nextState, cb) => {
    require.ensure([], function asyncLoad() {
      cb(null, require(`../views/${viewName}`).default);
    });
  };
}

/**
 * Obtiene un elemento de una array de manera aleatoria.
 *
 * @param {Array} arr - Array del cual se extraerá el elemento.
 */
export const randomElement = arr => arr[Math.floor(Math.random() * arr.length)];

export const route = function route(...args) {
  return new Route(...args);
};

/**
 * Obtiene una URL para los artículos en formato AMP.
 *
 * @param {String} pathname Pathname de la ruta.
 * @param {String} lang Prefijo para usarse como subdominio.
 */
export const ampUrl = (pathname, lang) => {
  const _ = new Rutica(pathname);
  _.extractSubpath(1, true);

  const r = route(`/${lang}/amp${_.toString()}`);
  r.subpathAsSubdomain(1, true);

  return r.toString();
};

export const vvDateTimeFormat = (val, locale) =>
  new Date(val).toLocaleString(
    locale,
    {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric'
    }
  );

/**
 * Obtiene la URL canónica de una pathname dado.
 *
 * @param {String} pathname Pathname
 */
export const getCanonicalUrl = (pathname) => {
  const url = `https://www.vavel.com${pathname}`;
  return url.endsWith('/') ? url : url + '/';
};