import { Obj } from './index';

export class Rating {

  /**
   * Total de decimales a redondear.
   */
  static DECIMALS = 2;

  /**
   * Pesos para cada puntuación.
   */
  static WEIGTHS = { one: 1, two: 2, three: 3, four: 4, five: 5 };

  /**
   * Determina si el nuevo rating solicitado está habilitado
   * para calificar un artículo. Basándose en los códigos de
   * error que devuelve el endpoint de rating de artículos.
   *
   * @param {Object} ratingArticle Objeto que devuelve
   * el endpoint de rating de artículos.
   */
  static isAllowedToRate(ratingArticle = {}) {
    if (Obj.isEmpty(ratingArticle)) return false;

    return ratingArticle.err === -2 || ratingArticle.err === -3;
  }

  /**
   * Calcula el promedio y el total de votos.
   *
   * @param {Object} keys Rating de artículo.
   */
  static parse(keys = {}) {
    let acum = 0;
    let total = 0;

    for (const key of Reflect.ownKeys(keys)) {
      const rating = keys[key];
      const star = Rating.WEIGTHS[key];
      acum += rating * star;
      total += rating;
    }

    try {
      const average = Math.round(acum / total * Rating.conversionFactor()) /
        Rating.conversionFactor();

      return {
        average: isNaN(average) ? 0 : average,
        total
      };
    } catch (err) {
      console.warn('[RatingService] Invalid values to average or total.');
      console.warn(err);
      return { average: 0, total: 0 };
    }
  }

  /**
   * Obtiene un factor de conversión para el redondeo del promedio.
   * Usado para saber por cuanto se debe multiplicar y dividir un
   * número para redondear sus decimales.
   *
   * @param {Number} decimals Total de decimales.
   */
  static conversionFactor(decimals = Rating.DECIMALS) {
    return Math.pow(10, decimals) / 10;
  }
}

export default Rating;