export class Str {

  /**
   * Formatea una cadena.
   *
   * @param {String} str Cadena a formatear.
   * @param {Object} params - Valores a reemplazar en la cadena.
   * La llave del objeto se buscará en la cadena, y se reemplazará
   * con su valor correspondiente.
   *
   * @example
   * Str.format('Hola {nombre}.', {nombre: 'vavel'})
   * retornará `Hola vavel.`
   */
  static format(str, params) {
    return str.replace(/{(\w+)}/g, (match, name) =>
      typeof params[name] !== 'undefined' ? params[name] : match
    );
  }

  /**
   * Limita una cadena.
   *
   * @param {String} str - String a limitar.
   * @param {Number} numOfChars - Número de caracteres a limitar.
   * @param {String} suffix - Sufijo.
   */
  static limit(str = '', numOfChars = 145, suffix = '...') {
    if (!str || numOfChars < 0) return '';
    return str.substr(str, numOfChars) + suffix;
  }

  /**
   * Genera un slug de una cadena.
   *
   * @param {String} str - String a convtir.
   */
  static slug(str) {
    str = str.toLowerCase();
    str = str.replace(/[\u00C0-\u00C5]/ig, 'a');
    str = str.replace(/[\u00C8-\u00CB]/ig, 'e');
    str = str.replace(/[\u00CC-\u00CF]/ig, 'i');
    str = str.replace(/[\u00D2-\u00D6]/ig, 'o');
    str = str.replace(/[\u00D9-\u00DC]/ig, 'u');
    str = str.replace(/[\u00D1]/ig, 'n');
    str = str.replace(/[^a-z0-9 ]+/gi, '');
    str = str.trim().replace(/ /g, '-');
    str = str.replace(/[-]{2}/g, '');

    return (str.replace(/[^a-z\- ]*/gi, ''));
  }

  /**
   * Escapa de una los caracteres pertenenecientes
   * a una expresión regular dada.
   *
   * @param {String} str - Cadena a escapar.
   */
  static escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }
}

export default Str;