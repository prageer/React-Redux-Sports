import { Rutica } from './Rutica';


export class Route {
  /**
   * Ruta.
   *
   * @type {Rutica}
   * @private
   */
  _route;

  /**
   * Determina si la ruta es segura, determinando
   * el uso del protocolo HTTPS o HTTP.
   *
   * @type {boolean}
   * @private
   */
  _secure = true;

  /**
   * Host de la ruta.
   *
   * @type {string}
   * @private
   */
  _host = '';

  /**
   * Subdomino de la ruta.
   *
   * @type {string}
   * @private
   */
  _subdomain = '';

  constructor(url, host = 'vavel.com', subdomain = '', secure = true) {
    this._route = Rutica.create(url);

    if (!host) throw Error('Debe proporcionar un host para la ruta.');

    this.host = host;
    this._subdomain = subdomain;
    this._secure = secure;
  }

  get subdomain() { return this._subdomain; }

  set subdomain(newSubdomain) {
    this._subdomain = newSubdomain;
    this._rebuildRoute();
  }

  get host() { return this._host; }

  set host(newHost) {
    this._host = newHost;
    this._rebuildRoute();
  }

  get secure() { return this._secure; }

  set secure(newSecure) {
    this._secure = newSecure;
    this._rebuildRoute();
  }

  get parts() { return this._route.parts; }

  get pathname() { return this._route.pathname; }

  get isAbsolute() { return this._route.isAbsolute; }

  /**
   * Genera una URL solo con el host de la ruta.
   *
   * @returns {string}
   */
  buildHost() {
    const p = this.buildProtocol();
    const s = this._buildSubdomain();

    return `${p}://${s}${this.host}`;
  }

  /**
   * Genera el protocolo a usarse en la URL.
   * Puede ser HTTP o HTTPS.
   *
   * @returns {string}
   */
  buildProtocol() { return 'http' + (this.secure ? 's' : ''); }

  /**
   * Obtiene el subdominio del pathname.
   *
   * @param index Índice del subdirectorio del pathname.
   * @param save Booleano o función para determinar si el
   *             subdominio será substituido en la ruta.
   * @returns {string|undefined} Subdomino en cuestión.
   */
  subpathAsSubdomain(index = 1, save = false) {
    const subdomain = this.posibleSubdomain(index);

    if (!subdomain) return;

    if (typeof save === 'function') save = save(subdomain);

    if (save) this.subdomain = this._route.extractSubpath(index, true);

    return subdomain;
  }

  /**
   * Obtiene un posible subdomino del pathname.
   *
   * @param index Índice del subdirectorio del pathname.
   * @returns {string|undefined}
   */
  posibleSubdomain(index = 1) {
    return this._route.extractSubpath(index, false) || undefined;
  }

  /**
   * Obtiene una representación en cadena de la ruta actual.
   *
   * @returns {string}
   */
  toString() {
    this._rebuildRoute();
    return this._route.toString();
  }

  /**
   * 'Recompila' la ruta para actualizar el atributo `_route`.
   * Éste se debe ejecutar cada vez que se actualiza un atributo.
   *
   * @private
   */
  _rebuildRoute() {
    this._route = this._route.toAbsolute(this.buildHost());
  }

  /**
   * Genera la cadena necesaria para concatenar
   * el subdominio con el host y el protocolo
   * correctamente.
   *
   * @returns {string}
   * @private
   */
  _buildSubdomain() {
    return this.subdomain ?
      this.subdomain + '.' :
      '';
  }
}

export default Route;