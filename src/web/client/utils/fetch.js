import { isBrowser } from './common';
import { clearErrorApp } from '../reducers/appReducer';

/**
 * Fetch data from components mapping "static fetchData()"
 * and injecting store, router params and query.
 * Used on the server-side. It returns a Promise.
 */
export const fetchData = (store, components, params, query) =>
  Promise.all(components
    .map((component) => {
      if (component &&
        'fetchData' in component &&
        typeof component.fetchData === 'function') {
        return component.fetchData({ store, params, query });
      }
      return false;
    }))
    .then(() => { if (isBrowser) store.dispatch(clearErrorApp()); });

/**
 * Fetch data from components when the router matches the browser location.
 * It also prevent the first page to re-fetch data already fetched from the server.
 * Used on the client-side.
 */
export const fetchDataOnLocationMatch = (history, routes, match, store) => {
  let ssrLocation = store.getState().getIn(['app', 'location']);

  history.listen((e) => {
    if (e.pathname !== ssrLocation) {
      match({ routes, location: e.pathname }, (error, redirect, props) => {
        if (props) {
          fetchData(
            store,
            props.components,
            props.params,
            props.location.query
          ).catch((err) => {
            console.warn('-----fetchDataOnLocationMatch-----');
            console.error(err);
            console.warn('----------------------------------');
          });
        }
      });
    }

    // enable subsequent fetches.
    ssrLocation = false;
  });
};