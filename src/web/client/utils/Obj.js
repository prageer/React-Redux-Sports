import { isEmpty as isObjectEmpty } from 'lodash';


/**
 * Clase Obj, operaciones comunes con objetos.
 */
export class Obj {
  /**
   * Determina si un objeto pasado es o no vacío.
   *
   * {Object} obj - Objeto a evaluar.
   */
  static isEmpty = obj => isObjectEmpty(obj);
}

export default Obj;