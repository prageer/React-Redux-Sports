import escape from 'jsesc';


/**
 * Serializa el estado de la aplicación.
 */
export const dehydrate = store => escape(
  JSON.stringify(store),
  { wrap: true, isScriptContext: true, json: true }
);

/**
 * Deserializa el estado de la aplicación.
 */
export const rehydrate = () => {
  try {
    const initialState = window.__STATE__ && JSON.parse(window.__STATE__);
    return initialState || {};
  } catch (err) {
    console.error('Error al tratar de cargar el estado inicial de la aplicación:');
    console.error(err);
  }
};