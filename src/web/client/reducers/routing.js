import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';


const initialState = fromJS({
  locationBeforeTransitions: null
});

export const reducer = function routerReducer(state = initialState, { type, payload }) {
  switch (type) {
    case LOCATION_CHANGE:
      return state.merge({ locationBeforeTransitions: payload });

    default:
      return state;
  }
};