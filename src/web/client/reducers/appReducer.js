import { createAction } from 'redux-actions';
import { fromJS, Map as map } from 'immutable';

import { WITH_SUBDOMAIN } from '../utils';


export const SET_LOCATION = 'SET_LOCATION';
export const SET_EDITION = 'SET_EDITION';
export const SET_EDITION_NAME = 'SET_EDITION_NAME';
export const ERROR_APP = 'ERROR_APP';
export const CLEAR_ERROR_APP = 'CLEAR_ERROR_APP';

export const setLocation = createAction(SET_LOCATION);
export const setEdition = createAction(SET_EDITION);
export const setEditionName = createAction(SET_EDITION_NAME);
export const errorApp = createAction(ERROR_APP);
export const clearErrorApp = createAction(CLEAR_ERROR_APP);

const initialState = fromJS({
  location: '/',
  edition: {},
  editionName: '',
  withSubdomain: WITH_SUBDOMAIN,
  actionType: '',
  err: '',
  hasError: false,
  description: '',
});

export default (state = initialState, { type, payload }) => {
  switch(type) {
    case ERROR_APP:
      // Some actions dispatch an article_id too.
      // const WITH_ARTICLE_ID = payload.article_id;
      // const error = WITH_ARTICLE_ID ?
      //   payload.error :
      //   payload;

      // TODO: update all of this.
      // return state.merge({
      //   description: error.message,
      //   actionType: action.type,
      //   err: error.toString(),
      //   hasError: true
      // });
      return state.set('hasError', true);
      break;

    case CLEAR_ERROR_APP:
      return state.set('hasError', false);
      break;

    case SET_LOCATION:
      return state.set('location', payload);
      break;

    case SET_EDITION:
      return state.set('edition', map(fromJS(payload)));
      break;

    case SET_EDITION_NAME:
      return state.set('editionName', payload);
      break;


    default:
      return state;
  }
};