import { combineReducers } from 'redux-immutable';

import app from './appReducer';
import menu from '../menu-left/reducer';

import { reducer as viewCategories } from '../views/categories/reducer';
import { reducer as viewArticle } from '../views/article-view/reducer';
import { reducer as viewArticles } from '../views/articles/reducer';
import { reducer as viewAuthor } from '../views/author/reducer';
import { reducer as viewTag } from '../views/tag/reducer';
import { reducer as routing } from './routing';


const RootReducer = combineReducers({
  routing,
  viewCategories,
  viewArticle,
  viewArticles,
  viewAuthor,
  viewTag,
  menu,
  app
});

export default RootReducer;