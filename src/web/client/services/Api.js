import { merge as mergeObj } from 'lodash';
import { format as formatUrl, parse as parseUrl } from 'url';


require('isomorphic-fetch');

/**
 * Clase Api, define el servicio usado tanto por el
 * cliente como por el servidor para comunicarse con
 * la API.
 */
export class Api {

  /**
   * Lista de estados de artículos que se
   * excluirán por defecto en cada petición.
   */
  static EXCLUDED_ARTICLES_STATUS = [
    -3, -2, -1, 0, 90,
  ];

  constructor(host = 'https://api.vavel.com') {
    this.host = host;
    console.log(`Creating API Service to ${host}`);
  }

  /**
   * Determina la URL para el endpoint de la API.
   *
   * @param {String} relativeUrl Pathname relativo al host de la API.
   */
  apiUrl(relativeUrl) { return this.host + relativeUrl; }

  /**
   *
   * @param {String} relativeUrl Pathname relativo al host de la API.
   * @param {Object} query Claves llave-valor para contruir la consulta a la API.
   */
  apiUrlForFetch(relativeUrl, query = {}) {
    const urlObj = parseUrl(this.apiUrl(relativeUrl));
    urlObj.query = mergeObj(urlObj.query, query);

    return formatUrl(urlObj);
  }

  /**
   * Middleware para controlar cualquier tipo de error
   * que la API pueda devolver.
   *
   * @param {Promise} response Respuesta recibida del endpoint de la API.
   */
  async handleResponseError(response) {
    if (!response.ok) {
      let json;
      let errorMessage = response.statusText;

      try {
        json = await response.json();
        errorMessage = json.error || errorMessage;
      } catch (e) {
        throw new Error(errorMessage);
      }

      throw new Error(errorMessage);
    }
  }

  /**
   * Realiza una petición tipo GET a la API.
   *
   * @param {String} relativeUrl Pathname relativo al host de la API.
   * @param {Object} query Claves llave-valor para contruir la consulta a la API.
   */
  async get(relativeUrl, query = {}) {
    const defaultHeaders = {};

    if (relativeUrl === '/articles') {
      query = this.prepareParamsForArticles(query);
    }

    try {
      const response = await fetch(
        this.apiUrlForFetch(relativeUrl, query),
        {
          credentials: 'same-origin',
          headers: defaultHeaders
        },
      );

      await this.handleResponseError(response);

      return response;
    } catch (err) {
      return { json() { return {}; } };
    }
  }

  /**
   * Convierte la respuesta en JSON.
   *
   * @param {Promise} r Respuesta de la API.
   */
  async prepare(r) { return r.json(); }

  /**
   * Establece los valores por defecto para los
   * parámetros para cada petición en la API.
   *
   * @param {Object} params Claves llave-valor para construir la consulta a la API.
   */
  prepareParamsForArticles(params) {
    if (!params.not_statuses) {
      params.not_statuses = Api.EXCLUDED_ARTICLES_STATUS.join(',');
    }
    return params;
  }

  /**
   * Obtiene los artículos de la API.
   *
   * @param {Object} params Claves llave-valor para construir la consulta a la API.
   */
  async getArticles(params) {
    const response = await this.get('/articles', params);
    return response.json();
  }

  /**
   * Hace una búsqueda de artículos.
   */
  async search(params) {
    const response = await this.get('/search', params);
    return response.json();
  }

  /**
   * Obtiene los artículos que están en la top zone.
   */
  async getArticlesTopZone(categoryId, take, skip, fields = []) {
    const params = {
      parent: categoryId,
      statuses: 17,
      sort: '-orderNumber',
      skip,
      take,
      fields: fields.join(',')
    };

    if (process.env.NODE_ENV === 'local') {
      params.mock = `articles_tz_${categoryId}.json`;
    }

    const response = await this.get('/articles', params);

    return this.prepare(response);
  }

  /**
   * Obtiene un determinado número de artículos traducidos por categoría.
   *
   * @param {Number} categoryId Id de la categoría.
   * @param {Number} skip Número de artículos a omitir.
   * @param {Number} take Número de artíuclos a tomar.
   */
  async getArticles18ByCategoryId(categoryId, skip, take, fields = []) {
    const params = {
      parent: categoryId,
      skip,
      take,
      statuses: 18,
      sort: '-orderNumber',
      fields: fields.join(',')
    };

    if (process.env.NODE_ENV === 'local') {
      params.mock = `articles_cat_${categoryId}.json`;
    }

    const response = await this.get('/articles', params);

    return this.prepare(response);
  }

  /**
   * Obtiene un artículo por su id.
   *
   * @param {Number} articleId Id de artículo.
   */
  async getArticleById(articleId) {
    const response = await this.get(`/articles/${articleId}`);

    return this.prepare(response);
  }

  /**
   * Obtiene los últimos artículos.
   *
   * @param {Number} categoryId Id de categoría.
   */
  async getTrendingArticles(categoryId, take = 1) {
    const params = {
      parent: categoryId,
      not_statuses: [
        0, -1, -2, 90,
      ],
      sort: '-orderNumber',
      take
    };

    if (process.env.NODE_ENV === 'local') {
      params.mock = `articles_tr_${categoryId}.json`;
    }

    const response = await this.get('/articles', params);

    return this.prepare(response);
  }

  /**
   * Obtiene una categoría dado un path.
   *
   * @param {String} path Pathname.
   */
  async getCategoryByPath(path) {
    if (path && path.substring(0, 1) !== '/') {
      path = `/${path}`;
    }

    const response = await this.get(`/categoryByPath${path}`);

    return this.prepare(response);
  }

  /**
   * Obtiene el menú de la aplicación.
   *
   * @param {String} path Pathname.
   */
  async getCategoriesMenu(path) {
    const response = await this.get('/menus', { depth: 3, path });

    return this.prepare(response);
  }

  /**
   * Obtiene un determinado número de artículos por categoría.
   *
   * @param {Number} catId Id de la categoría.
   * @param {Number} skip Número de artículos a omitir.
   * @param {Number} take Número de artíuclos a tomar.
   */
  async getCategoryArticles(catId, skip, take = 19) {
    const param = {
      parent: catId,
      skip,
      take,
      sort: '-orderNumber'
    };

    if (process.env.NODE_ENV === 'local') {
      param.mock = `cat_articles${catId}.json`;
    }

    const response = await this.get('/articles', param);

    return this.prepare(response);
  }

  /**
   * Obtiene un determinado número de artículos de un autor.
   *
   * @param {Number} skip Número de artículos a omitir.
   * @param {Number} take Número artículos a obtener.
   */
  async getAuthorArticles(author, skip, take = 19) {
    const param = {
      author,
      skip,
      not_statuses: [
        0, -1, -2,
      ],
      sort: '-orderNumber',
      take
    };

    if (process.env.NODE_ENV === 'local') {
      param.mock = `author_${author}.json`;
    }

    const response = await this.get('/articles', param);

    return this.prepare(response);
  }

  /**
   * Cuenta el número de artículos de un autor.
   *
   * @param {String} author Nombre de autor.
   */
  async getAuthorCountArticles(author) {
    const param = { author };
    const response = await this.get('/articles/count', param);

    return this.prepare(response);
  }

  /**
   * Obtiene un determinado número de artículos por etiqueta.
   *
   * @param {String} tag Nombre de la etiqueta.
   * @param {String} lang Idioma para mostrar los artículos.
   * @param {Number} skip Número de artículos a omitir.
   * @param {Number} take Número de artíuclos a tomar.
   */
  async getTagArticles(tag, lang, skip, take) {
    const param = {
      tag,
      cat_sefriendly: lang,
      sort: '-orderNumber',
      skip,
      take
    };

    if (process.env.NODE_ENV === 'local') {
      param.mock = `articles_tag_${tag}.json`;
    }

    const response = await this.get('/articles', param);

    return this.prepare(response);
  }

  /**
   * Obtiene los "mini" artículos para un artículo.
   *
   * @param {Number} catId Id de categoría.
   * @param {Number} articleId Id de artículo.
   */
  async getContentMiniArticles(catId, articleId) {
    const param = {
      parent: catId,
      ids_excluded: articleId,
      not_statuses: [
        0, -1, -2, 90,
      ],
      sort: '-orderNumber',
      skip: 0,
      take: 6
    };

    if (process.env.NODE_ENV === 'local') {
      param.mock = `articles_mini_${catId}.json`;
    }

    const response = await this.get('/articles', param);

    return this.prepare(response);
  }

  /**
   * Obtiene el contenido de los últimos artículos.
   *
   * @param {Number} catId Id de categoría.
   */
  async getContentLatestArticles(catId) {
    const param = {
      parent: catId,
      ids_excluded: 0,
      not_statuses: [
        0, -1, -2, 90,
      ],
      sort: '-orderNumber',
      skip: 0,
      take: 12
    };

    if (process.env.NODE_ENV === 'local') {
      param.mock = `articles_latest_${catId}.json`;
    }

    const response = await this.get('/articles', param);

    return this.prepare(response);
  }

  /**
   * Califica un artículo.
   *
   * @param {Object} params Claves llave-valor para construir la consulta a la API.
   */
  async addRatingArticle(params) {
    const response = await this.get('/rating-article', params);
    return this.prepare(response);
  }
}

export default Api;