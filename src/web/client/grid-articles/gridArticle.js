import {
  getArticleUrl,
  getImgUrl,
  randomElement
} from '../utils';


/**
 * Posibles valores que puede tomar
 * la propiedad flex-basis para un
 * elemento, este determina su ancho.
 */
export const BREAKPOINTS_FOR_BASIS = [
  270, 280, 245, 310, 320, 350,
];

/**
 * Posibles valores para limitar el
 * número de caracteres que tendrá
 * la descripción de cada artículo
 * para los dispositivos móviles.
 */
export const DESCRIPTION_LIMITS = [
  145, 160, 173, 190,
];

export const randomFlexboxBasisProperty = () => randomElement(BREAKPOINTS_FOR_BASIS);

export const randomLimitForDescription = () => randomElement(DESCRIPTION_LIMITS);

/**
 * Agrega y/o modifica un artículo para su correcto uso en las vistas.
 *
 * @param {Immutable.Map} article - Artículo.
 * @param {Number} index - Index del artículo, determina si es el primer
 * artículo del total en `articles`.
 */
export const customize = (article, index = -1) => {
  const imgUrl = getImgUrl([ // Posible propiedad modificada.
    index === 0 ? '700' : '200',
    article.get('image'),
  ]);

  return article.merge({
    customBasis: randomFlexboxBasisProperty(),
    limitDescription: randomLimitForDescription(),
    article_url: getArticleUrl(article),
    image: imgUrl
  });
};