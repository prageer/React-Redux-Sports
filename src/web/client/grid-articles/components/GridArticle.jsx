import React from 'react';

import Link from '../../components/Link';

import { Str } from '../../utils';


export class CacheableGridArticle extends React.Component {
  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }

  render() {
    const {
      title, image, image_caption, article_url,
      authorName, abstract, description, customStyle,
      altImage
    } = this.props;

    return (
      <article style={customStyle} className="grid-article">
        <div className="grid-article__container">
          {
            !image ?
              null :
              <img src={image} alt={altImage} />
          }
          <Link to={article_url} className="over-bg" />
          <div className="grid-article--link">
            <div className="grid-article--title">
              <Link to={article_url}>
                <h2 dangerouslySetInnerHTML={{ __html: title }} />
              </Link>
              <span dangerouslySetInnerHTML={{ __html: authorName }} />
            </div>
          </div>
        </div>
        {
          !abstract ?
            null :
            <p
              className="grid-article--desc"
              dangerouslySetInnerHTML={{ __html: description }}
            />
        }
      </article>
    );
  }
}

const GridArticle = ({ article: a, withFlexBasis }) => {
  return (
    <CacheableGridArticle
      customStyle={withFlexBasis ? { flexBasis: a.customBasis } : {}}
      title={a.title}
      image={a.image}
      image_caption={a.image_caption}
      article_url={a.article_url}
      authorName={a.author ? a.author.name : null}
      abstract={a.abstract}
      altImage={a.image_caption || a.title}
      description={Str.limit(a.abstract, a.limitDescription)}
    />
  );
};

GridArticle.defaultProps = {
  withFlexBasis: true
};

export default GridArticle;