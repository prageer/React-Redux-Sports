import React, { Component } from 'react';

import LoadingScreen from '../../components/LoadingScreen';
import GridArticle from './GridArticle';

import { isBrowser } from '../../utils';


class GridArticles extends Component {

  componentDidMount() {
    if (isBrowser) window.addEventListener('scroll', this.onScroll);
  }

  componentWillUnmount() {
    if (isBrowser) window.removeEventListener('scroll', this.onScroll);
  }

  onScroll = () => {
    const bottom = window.document.body.getBoundingClientRect().bottom;

    if (Math.floor(bottom) === window.innerHeight && !this.props.isFetching) {
      this.props.loadArticles(this.props.articles.length);
    }
  }

  render() {
    const articles = this.props.articles || [];
    const firstArticle = articles[0];
    const rightArticles = articles.slice(1, 5);

    return (
      <div id="infinte">
        <div className="grid-articles__top">
          {
            !firstArticle ?
              null :
              <div className="grid-articles grid-articles__top--left">
                <GridArticle article={firstArticle} withFlexBasis={false} />
              </div>
          }
          <div className="grid-articles grid-articles__top--right">
            {
              rightArticles.length > 0 && rightArticles.map(
                (article, index) =>
                  <GridArticle
                    article={article}
                    withFlexBasis={false}
                    key={article._id + index}
                  />
              )
            }
          </div>
        </div>

        <div className="grid-articles grid-articles__normal">
          {
            articles.slice(5).length > 0 && articles.slice(5).map(
              (article, index) =>
                <GridArticle article={article} key={article._id + index} />
            )
          }
        </div>
        {
          !this.props.isFetching ?
            null :
            <LoadingScreen />
        }
      </div>
    );
  }
}

export default GridArticles;