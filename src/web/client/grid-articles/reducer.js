import { fromJS, List as list } from 'immutable';

import {
  ADD_ARTICLES,
  REQUEST_ARTICLES,
  ARTICLES_RECEIVED,
  ERROR_ARTICLES
} from './actionTypes';

import { customize as customizeArticle } from './gridArticle';


/**
 * Apply a custom method to every article.
 *
 * @param {Immutable.List} articles articles.
 * @param {Immutable.list} payload new articles.
 */
const parseArticles = (articles = [], payload) => {
  payload = payload.map(customizeArticle);

  return articles.size > 0 ? articles.concat(payload) : payload;
};

const initialState = fromJS({
  articles: [],
  isFetching: false
});

const handleArticlesRequest = (state, { type }) => {
  switch (type) {
    case REQUEST_ARTICLES:
      return state.set('isFetching', true);
      break;

    case ARTICLES_RECEIVED:
      return state.set('isFetching', false);
      break;

    case ERROR_ARTICLES:
      return state.set('isFetching', false);
      break;
  }
};

/**
 * Turns articles into immutable records.
 *
 * @param {Immutable.Map} state App state.
 * @param {Object} action Redux action.
 */
export const handleArticles = (state, action) => {
  const articles = parseArticles(state.get('articles'), fromJS(action.payload));
  return state.set('articles', articles);
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ARTICLES:
      return handleArticles(state, action);
      break;

    case REQUEST_ARTICLES:
    case ARTICLES_RECEIVED:
    case ERROR_ARTICLES:
      return handleArticlesRequest(state, action);
      break;

    default:
      return state;
  }
}