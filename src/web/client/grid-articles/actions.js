import { createAction } from 'redux-actions';

import {
  LOAD_ARTICLES_18,
  LOAD_ARTICLES_TOP_ZONE,
  ADD_ARTICLES,
  REQUEST_ARTICLES,
  ARTICLES_RECEIVED,
  ERROR_ARTICLES
} from './actionTypes';


export const requestArticles = createAction(REQUEST_ARTICLES);
export const articlesReceived = createAction(ARTICLES_RECEIVED);
export const errorArticles = createAction(ERROR_ARTICLES);

export const addArticles = createAction(ADD_ARTICLES);