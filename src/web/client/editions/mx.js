/**
 * Archivo de traducción para México.
 */
module.exports = {
  categoryId: 232,
  cultureShortcut: 'mx',
  cultureHref: '/mx/',
  cultures: [{
    href: '/mx/',
    title: 'VAVEL México',
    title_short: 'VAVEL México',
    class: 'flag-mx'
  },
  {
    href: '/en-us/',
    title: 'VAVEL USA',
    title_short: 'VAVEL USA',
    class: 'flag-usa'
  },
  {
    href: '/es/',
    title: 'VAVEL España',
    title_short: 'España',
    class: 'flag-es'
  },
  {
    href: '/ar/',
    title: 'VAVEL Argentina',
    title_short: 'ARG',
    class: 'flag-ar'
  },
  {
    href: '/lat/',
    title: 'VAVEL Latinoamerica',
    title_short: 'LAT',
    class: 'flag-lat'
  },
  {
    href: '/en/',
    title: 'VAVEL UK',
    title_short: 'UK',
    class: 'flag-uk'
  },
  {
    href: '/br/',
    title: 'VAVEL Brasil',
    title_short: 'Brasil',
    class: 'flag-br'
  },
  {
    href: '/it/',
    title: 'VAVEL Italia',
    title_short: 'IT',
    class: 'flag-it'
  },
  {
    href: '/fr/',
    title: 'VAVEL Francia',
    title_short: 'FR',
    class: 'flag-fr'
  },
  {
    href: '/arb/',
    title: 'VAVEL Árabe',
    title_short: 'Árabe',
    class: 'flag-arb'
  },
  ],
  latest: 'lo último',
  trendingTopics: 'Temas del momento:',
  writeInVavel: 'Write in Vavel',
  writerPanel: 'Writer panel',
  connectWithFacebook: 'Connect with facebook',
  news: 'Noticias',
  homeTitle: 'VAVEL México - The international sports newspaper',
  facebookFrameUrl: '//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FVAVELMexico&width=300&height=62&show_faces=false&colorscheme=light&stream=false&border_color&header=false&appId=169320175188',
  locale: 'es-mx',
  css: '/public/assets/css/cultures/mexico.css',
  homeDescription: 'Las últimas noticias deportivas y artículos analizados en profundidad y calidad, fotos, vídeos, resultados y partidos en vivo minuto a minuto en VAVEL, un periódico pensando diferente.',
  homeKeywords: 'noticias, última hora, actualidad, nacional, España, Latinoamérica, México, Argentina, vavel, periodismo, fútbol, tenis, cine, sociedad, política, televisión, TV, música, arte, baloncesto, internacional, calcio, premier league, bundesliga, liga, la liga, prensa deportiva, periódicos, deportes, deporte, prensa, opinión, moda, blogs, firmas, especiales, vídeos, fotos, audios, gráficos, entrevistas',
  contact: 'contacto',
  facebookCommunityUrl: 'http://www.facebook.com/VAVELMexico/',
  facebookLikeFrameUrl: 'http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FVAVELMexico&amp;send=false&amp;layout=button_count&amp;width=140&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=169320175188',
  twitterButtonUrl: 'https://twitter.com/VAVEL_Mexico',
  twitterButtonText: 'Follow @VAVEL_Mexico',
  googlePlusCommunityUrl: 'https://plus.google.com/106666950204710493832',
  googlePlusLikeFrameUrl: '//plus.google.com/106666950204710493832',
  categoryTitle: '{categoryName} Noticias - VAVEL.com',
  categoryDescription: 'Noticias, fotos, actualidad, información y artículos de última hora sobre {categoryName} en VAVEL',
  categoryKeywords: '{categoryName},VAVEL México - The international sports newspaper',
  articleTitle: '{title} - VAVEL.com',
  articleDescription: '{description}',
  articleKeywords: '{keywords}',
  taboolaUrl: '//cdn.taboola.com/libtrc/vaveleu-es/loader.js',
  androidWidgetTitle: 'Noticias deportivas',
  androidWidgetAuthor: 'the VAVEL España app',
  androidWidgetPrice: 'GRATIS',
  androidWidgetLanguage: 'espanol',
  androidWidgetInAppStore: 'en App Store',
  androidWidgetInGooglePlay: 'en Google Play',
  facebookSdkCulture: 'es_ES',
  twitterSite: '@VAVEL_MEXICO',
  twitterLikeVia: 'VAVEL_Mexico',
  lang: 'es',
  flag: 'mx',
  goals: 'MARCADOR',
  referee: 'ÁRBITRO',
  incidents: 'INCIDENCIAS',
  latestArticlesTaggedAs: 'Más noticias sobre',
  latestArticlesTaggedNews: 'Las últimas noticias sobre {tag}',
  authorTitle: '{title} - VAVEL.com',
  authorDescriptionWithBio: 'The articles portfolio of {author_name}. {author_bio} in VAVEL platform.',
  authorDescriptionWithoutBio: 'The articles portfolio of {author_name} on the Author Zone in VAVEL platform.',
  authorKeywords: '{keywords}',
  authorInVavel: 'Autor en VAVEL',
  writer: 'Escritor',
  articles: 'Articulos',
  tagTitle: '{tag} - Las últimas noticias sobre {tag} en VAVEL México',
  tagDescription: 'Lo último sobre {tag}. Noticias, fotos, actualidad, información y artículos de última hora sobre {tag} en VAVEL México.',
  tagKeywords: '{keywords}',
  homepage: 'Homepage',

  clubs: [{
    icon: 'submenu_1',
    href: '/mx/futbol-mexicano/liga-mx/',
    title: 'Liga MX',
    name: 'LIGA MX'
  },
  {
    icon: 'submenu_3',
    href: '/mx/futbol-mexicano/liga-mx/america/',
    title: 'Club América',
    name: 'AME'
  },
  {
    icon: 'submenu_4',
    href: '/mx/futbol-mexicano/liga-mx/atlas/',
    title: 'Club Atlas',
    name: 'ATL'
  },
  {
    icon: 'submenu_5',
    href: '/mx/futbol-mexicano/liga-mx/cruz-azul/',
    title: 'Cruz Azul',
    name: 'CAZ'
  },
  {
    icon: 'submenu_6',
    href: '/mx/futbol-mexicano/liga-mx/chiapas/',
    title: 'Chiapas',
    name: 'CHI'
  },
  {
    icon: 'submenu_7',
    href: '/mx/futbol-mexicano/liga-mx/chivas/',
    title: 'CD Guadalajara',
    name: 'GUA'
  },
  {
    icon: 'submenu_8',
    href: '/mx/futbol-mexicano/liga-mx/leon/',
    title: 'Club León',
    name: 'LEO'
  },
  {
    icon: 'submenu_9',
    href: '/mx/futbol-mexicano/liga-mx/dorados/',
    title: 'Dorados de Sinaloa',
    name: 'DOR'
  },
  {
    icon: 'submenu_10',
    href: '/mx/futbol-mexicano/liga-mx/morelia/',
    title: 'Monarcas Morelia',
    name: 'MON'
  },
  {
    icon: 'submenu_11',
    href: '/mx/futbol-mexicano/liga-mx/pachuca/',
    title: 'CF Pachuca',
    name: 'PAC'
  },
  {
    icon: 'submenu_12',
    href: '/mx/futbol-mexicano/liga-mx/puebla/',
    title: 'Puebla FC',
    name: 'PUE'
  },
  {
    icon: 'submenu_13',
    href: '/mx/futbol-mexicano/liga-mx/pumas/',
    title: 'Pumas de la UNAM',
    name: 'PUM'
  },
  {
    icon: 'submenu_14',
    href: '/mx/futbol-mexicano/liga-mx/queretaro/',
    title: 'Gallos Blancos de Querétaro',
    name: 'QRO'
  },
  {
    icon: 'submenu_15',
    href: '/mx/futbol-mexicano/liga-mx/rayados-monterrey/',
    title: 'Rayados Monterrey',
    name: 'MTY'
  },
  {
    icon: 'submenu_16',
    href: '/mx/futbol-mexicano/liga-mx/santos-laguna/',
    title: 'Santos Laguna',
    name: 'SAN'
  },
  {
    icon: 'submenu_17',
    href: '/mx/futbol-mexicano/liga-mx/veracruz/',
    title: 'Tiburones Rojos de Veracruz',
    name: 'VER'
  },
  {
    icon: 'submenu_18',
    href: '/mx/futbol-mexicano/liga-mx/tigres/',
    title: 'Club Tigres',
    name: 'TIG'
  },
  {
    icon: 'submenu_19',
    href: '/mx/futbol-mexicano/liga-mx/toluca/',
    title: 'Deportivo Toluca',
    name: 'TOL'
  },
  {
    icon: 'submenu_20',
    href: '/mx/futbol-mexicano/liga-mx/xolos/',
    title: 'Club Tijuana',
    name: 'XOL'
  },
  ]
};