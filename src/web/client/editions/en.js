/**
 * Archivo de traducción para Reino Unido.
 */
module.exports = {
  categoryId: 301,
  cultureShortcut: 'en',
  cultureHref: '/en/',
  cultures: [{
    href: '/en/',
    title: 'VAVEL UK',
    title_short: 'VAVEL UK',
    class: 'flag-uk'
  },
  {
    href: '/en-us/',
    title: 'VAVEL USA',
    title_short: 'USA',
    class: 'flag-usa'
  },
  {
    href: '/es/',
    title: 'VAVEL Spain',
    title_short: 'VAVEL Spain',
    class: 'flag-es'
  },
  {
    href: '/lat/',
    title: 'VAVEL Latin America',
    title_short: 'LatAm',
    class: 'flag-lat'
  },
  {
    href: '/br/',
    title: 'VAVEL Brazil',
    title_short: 'VAVEL Brazil',
    class: 'flag-br'
  },
  {
    href: '/it/',
    title: 'VAVEL Italia',
    title_short: 'VAVEL Italia',
    class: 'flag-it'
  },
  {
    href: '/fr/',
    title: 'VAVEL France',
    title_short: 'France',
    class: 'flag-fr'
  },
  {
    href: '/arb/',
    title: 'VAVEL Arabic',
    title_short: 'Arabic',
    class: 'flag-arb'
  },
  ],
  latest: 'latest',
  trendingTopics: 'Trending topics:',
  writeInVavel: 'Write in Vavel',
  writerPanel: 'Writer panel',
  connectWithFacebook: 'Connect with facebook',
  news: 'News',
  homeTitle: 'VAVEL UK - The international sports newspaper',
  facebookFrameUrl: 'http://www.facebook.com/plugins/likebox.php?href=http://www.facebook.com/vavelcom&width=300&colorscheme=light&show_faces=false&border_color&stream=false&header=true&height=62&appId=169320175188',
  locale: 'en-gb',
  css: '/public/assets/css/cultures/english.css',
  homeDescription: 'Sports latest news and articles analyzed in depth and quality, photos, videos, results and live scores comementaries from VAVEL, a  media thinking different.',
  homeKeywords: 'News, latest news, current affairs, national, english, uk, united kingdom, vavel, journalism, soccer, tennis, cinema, society, politics, television, TV, music, art, basketball, international, calcium, premier league, bundesliga, league, league, press sports, newspapers, sports, sports, news, opinion, fashion, blogs, signatures, special, video, photos, audio, graphics, interviews',
  contact: 'contact us',
  facebookCommunityUrl: 'http://www.facebook.com/VAVELcom/',
  facebookLikeFrameUrl: 'http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FVAVELcom&amp;send=false&amp;layout=button_count&amp;width=140&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=169320175188',
  twitterButtonUrl: 'https://twitter.com/vavel',
  twitterButtonText: 'Follow @VAVEL',
  googlePlusCommunityUrl: 'https://plus.google.com/102771328195442871577',
  googlePlusLikeFrameUrl: '//plus.google.com/102771328195442871577',
  categoryTitle: '{categoryName} News - VAVEL.com',
  categoryDescription: 'Breaking news, photos and articles about {categoryName} in VAVEL',
  categoryKeywords: '{categoryName},VAVEL UK - The international sports newspaper',
  articleTitle: '{title} - VAVEL.com',
  articleDescription: '{description}',
  articleKeywords: '{keywords}',
  taboolaUrl: '//cdn.taboola.com/libtrc/vaveleu-en/loader.js',
  androidWidgetTitle: 'Sport News and Scores',
  androidWidgetAuthor: 'the VAVEL app',
  androidWidgetPrice: 'FREE',
  androidWidgetLanguage: 'all languages',
  androidWidgetInAppStore: 'on the App Store',
  androidWidgetInGooglePlay: 'in Google Play',
  facebookSdkCulture: 'en_EN',
  twitterSite: '@VAVEL',
  twitterLikeVia: 'VAVEL',
  lang: 'en',
  flag: 'en',
  goals: 'SCORE',
  referee: 'REFEREE',
  incidents: 'INCIDENTS',
  latestArticlesTaggedAs: 'More news about',
  latestArticlesTaggedNews: 'News about {tag}',
  authorTitle: '{title} - VAVEL.com',
  authorDescriptionWithBio: 'The articles portfolio of {author_name}. {author_bio} in VAVEL platform.',
  authorDescriptionWithoutBio: 'The articles portfolio of {author_name} on the Author Zone in VAVEL platform.',
  authorKeywords: '{keywords}',
  authorInVavel: 'Author in VAVEL',
  writer: 'Writer',
  articles: 'Articles',
  tagTitle: '{tag} - News about {tag} in VAVEL in English',
  tagDescription: 'The latest about {tag}. Breaking news, photos and articles about {tag} in VAVEL in English.',
  tagKeywords: '{keywords}',
  homepage: 'Homepage',

  clubs: [{
    icon: 'submenu_4',
    href: '/en/football/premier-league/chelsea-fc/',
    title: 'Chelsea',
    name: 'CHE'
  },
  {
    icon: 'submenu_9',
    href: '/en/football/premier-league/manchester-city/',
    title: 'Manchester City',
    name: 'MCI'
  },
  {
    icon: 'submenu_1',
    href: '/en/football/premier-league/arsenal/',
    title: 'Arsenal',
    name: 'ARS'
  },
  {
    icon: 'submenu_10',
    href: '/en/football/premier-league/manchester-united/',
    title: 'Manchester United',
    name: 'MUN'
  },
  {
    icon: 'submenu_17',
    href: '/en/football/premier-league/tottenham-hotspur/',
    title: 'Tottenham',
    name: 'TOT'
  },
  {
    icon: 'submenu_8',
    href: '/en/football/premier-league/liverpool-fc/',
    title: 'Liverpool',
    name: 'LIV'
  },
  {
    icon: 'submenu_13',
    href: '/en/football/premier-league/southampton/',
    title: 'Southampton',
    name: 'SOU'
  },
  {
    icon: 'submenu_16',
    href: '/en/football/premier-league/swansea-city/',
    title: 'Swansea',
    name: 'SWA'
  },
  {
    icon: 'submenu_14',
    href: '/en/football/premier-league/stoke-city/',
    title: 'Stoke City',
    name: 'STK'
  },
  {
    icon: 'submenu_5',
    href: '/en/football/premier-league/crystal-palace/',
    title: 'Crystal Palace',
    name: 'CRY'
  },
  {
    icon: 'submenu_6',
    href: '/en/football/premier-league/everton/',
    title: 'Everton',
    name: 'EVE'
  },
  {
    icon: 'submenu_20',
    href: '/en/football/premier-league/west-ham/',
    title: 'West Ham United',
    name: 'WHU'
  },
  {
    icon: 'submenu_19',
    href: '/en/football/premier-league/west-bromwich-albion/',
    title: 'West Bromwich Albion',
    name: 'WBA'
  },
  {
    icon: 'submenu_7',
    href: '/en/football/premier-league/leicester-city/',
    title: 'Leicester City',
    name: 'LEI'
  },
  {
    icon: 'submenu_11',
    href: '/en/football/premier-league/newcastle-united/',
    title: 'Newcastle United',
    name: 'NEW'
  },
  {
    icon: 'submenu_15',
    href: '/en/football/premier-league/sunderland/',
    title: 'Sunderland',
    name: 'SUN'
  },
  {
    icon: 'submenu_2',
    href: '/en/football/premier-league/aston-villa/',
    title: 'Aston Villa',
    name: 'VIL'
  },
  {
    icon: 'submenu_3',
    href: '/en/football/premier-league/bournemouth/',
    title: 'AFC Bournemouth',
    name: 'BOU'
  },
  {
    icon: 'submenu_18',
    href: '/en/football/premier-league/watford-fc/',
    title: 'Watford FC',
    name: 'WAT'
  },
  {
    icon: 'submenu_12',
    href: '/en/football/premier-league/norwich-city/',
    title: 'Norwich City',
    name: 'NOR'
  },
  ]
};