/**
 * Archivo de traducción para Alemania.
 */
module.exports = {
  categoryId: 1283,
  cultureShortcut: 'de',
  cultureHref: '/de/',
  cultures: [{
    href: '/de/',
    title: 'VAVEL Deutschland',
    title_short: 'VAVEL Deutschland',
    class: 'flag-de'
  },
  {
    href: '/en-us/',
    title: 'VAVEL USA',
    title_short: 'USA',
    class: 'flag-usa'
  },
  {
    href: '/en/',
    title: 'VAVEL UK',
    title_short: 'UK',
    class: 'flag-uk'
  },
  {
    href: '/es/',
    title: 'VAVEL Spain',
    title_short: 'Spain',
    class: 'flag-es'
  },
  {
    href: '/mx/',
    title: 'VAVEL Mexico',
    title_short: 'Mexico',
    class: 'flag-mx'
  },
  {
    href: '/lat/',
    title: 'VAVEL Latin America',
    title_short: 'LAT',
    class: 'flag-lat'
  },
  {
    href: '/br/',
    title: 'VAVEL Brasilien',
    title_short: 'Brasilien',
    class: 'flag-br'
  },
  {
    href: '/it/',
    title: 'VAVEL Italien',
    title_short: 'Italien',
    class: 'flag-it'
  },
  {
    href: '/fr/',
    title: 'VAVEL France',
    title_short: 'France',
    class: 'flag-fr'
  },
  {
    href: '/arb/',
    title: 'VAVEL Arabic',
    title_short: 'Arabic',
    class: 'flag-arb'
  },
  ],
  latest: 'latest',
  trendingTopics: 'Trendthemen',
  writeInVavel: 'Write in Vavel',
  writerPanel: 'Writer panel',
  connectWithFacebook: 'Connect with facebook',
  news: 'Nachrichten',
  homeTitle: 'VAVEL.com - The international newspaper',
  facebookFrameUrl: '',
  locale: 'de-de',
  css: '/public/assets/css/cultures/germany.css',
  homeDescription: 'Sport-Inhalte, Geschehnisse und Hintergründe! Vavel verbindet Journalisten, Schriftsteller und Blogger mit seinen LeserInnen.',
  homeKeywords: 'News, neueste Nachricht, aktuelles Geschehen, national, Englisch, UK, vereinigtes Königreich, vavel, Journalismus, Fußball, Tennis, Kino, Gesellschaft, Politik, Fernsehen, TV, Musik, Kunst, Basketball, international, Calcio, premier league, bundesliga, Liga, league, Sportpresse, Zeitung, Sport, Sport, Neuigkeiten, Meinung, Kleidung, Blogs, Unterschriften, spezial, Video, Fotos, audio, Graphiken, Interviews',
  contact: 'kontakt',
  facebookCommunityUrl: 'http://www.facebook.com/VAVELDeutschland/',
  facebookLikeFrameUrl: 'http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FVAVELMexico&amp;send=false&amp;layout=button_count&amp;width=140&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=169320175188',
  twitterButtonUrl: 'https://twitter.com/VAVEL_Deutsch',
  twitterButtonText: 'Follow @VAVEL_Deutsch',
  googlePlusCommunityUrl: '',
  googlePlusLikeFrameUrl: '',
  categoryTitle: '{categoryName} Nachrichten - VAVEL.com',
  categoryDescription: 'Letzter Artikel über {categoryName} in VAVEL.com',
  categoryKeywords: '{categoryName},VAVEL.com - The international newspaper',
  articleTitle: '{title} - VAVEL.com',
  articleDescription: '{description}',
  articleKeywords: '{keywords}',
  taboolaUrl: '',
  facebookSdkCulture: 'de_DE',
  twitterSite: '@VAVEL_Deutsch',
  googlePlusCommunity: '',
  twitterLikeVia: 'VAVEL_Deutsch',
  lang: 'de',
  flag: 'de',
  goals: 'Ergebnis',
  referee: 'Schiedsrichter',
  incidents: 'Ereignisse',
  latestArticlesTaggedAs: 'Weitere Nachrichten über',
  latestArticlesTaggedNews: 'News about {tag}',
  authorTitle: '{title} - VAVEL.com',
  authorDescriptionWithBio: 'The articles portfolio of {author_name}. {author_bio} in VAVEL platform.',
  authorDescriptionWithoutBio: 'The articles portfolio of {author_name} on the Author Zone in VAVEL platform.',
  authorKeywords: '{keywords}',
  authorInVavel: 'Autor in VAVEL',
  writer: 'Schriftsteller',
  articles: 'Artikel',
  tagTitle: '{tag} - News about {tag} in VAVEL.com ',
  tagDescription: 'The latest about {tag}. Letzter Artikel über {tag} in VAVEL.com.',
  tagKeywords: '{keywords}',
  homepage: 'Homepage',

  clubs: [{
    icon: 'submenu_1',
    href: '/de/fusball/bayern-muenchen/',
    title: 'FC Bayern München',
    name: 'Bayern'
  },
  {
    icon: 'submenu_2',
    href: '/de/fusball/borussia-dortmund/',
    title: 'Borussia Dortmund',
    name: 'BVB'
  },
  {
    icon: 'submenu_3',
    href: '/de/fusball/bayer-leverkusen/',
    title: 'Bayer Leverkusen',
    name: 'BAY LEV'
  },
  {
    icon: 'submenu_4',
    href: '/de/fusball/schalke-04/',
    title: 'FC Schalke 04',
    name: 'Schalke'
  },
  {
    icon: 'submenu_5',
    href: '/de/fusball/fc-ingolstadt-04/',
    title: 'FC Ingolstadt',
    name: 'Ingol'
  },
  {
    icon: 'submenu_8',
    href: '/de/fusball/hsv/',
    title: 'Hamburger SV',
    name: 'Hamb'
  },
  {
    icon: 'submenu_7',
    href: '/de/fusball/eintracht-frankfurt/',
    title: 'Eintracht Frankfurt',
    name: 'Eint'
  },
  {
    icon: 'submenu_9',
    href: '/de/fusball/borussia-moenchengladbach/',
    title: 'Borussia Mönchengladbach',
    name: 'Gladb'
  },
  {
    icon: 'submenu_10',
    href: '/de/fusball/hannover-96/',
    title: 'Hannover 96',
    name: 'Han'
  },
  {
    icon: 'submenu_13',
    href: '/de/fusball/vfl-wolfsburg/',
    title: 'VFL Wolfsburg',
    name: ' Wol'
  },
  {
    icon: 'submenu_12',
    href: '/de/fusball/hoffenheim/',
    title: 'TSG 1899 Hoffenheim',
    name: 'Hof'
  },
  {
    icon: 'submenu_6',
    href: '/de/fusball/werder-bremen/',
    title: 'SV Werder Bremen',
    name: 'WBrem'
  },
  {
    icon: 'submenu_11',
    href: '/de/fusball/fc-koln/',
    title: '1. FC Köln',
    name: 'K&#246;ln'
  },
  {
    icon: 'submenu_17',
    href: '/de/fusball/sv-darmstadt-98/',
    title: 'SV Darmstadt 98',
    name: 'Darm'
  },
  {
    icon: 'submenu_14',
    href: '/de/fusball/vfb-stuttgart/',
    title: 'VFB Stuttgart',
    name: 'Stuttgart'
  },
  {
    icon: 'submenu_15',
    href: '/de/fusball/mainz-05/',
    title: 'FSV Mainz 05',
    name: 'Mainz'
  },
  {
    icon: 'submenu_16',
    href: '/de/fusball/hertha-berlin/',
    title: 'Hertha BSC Berlin',
    name: 'Hertha'
  },
  {
    icon: 'submenu_18',
    href: '/de/fusball/fc-augsburg/',
    title: 'FC Augsburg',
    name: 'Augsburg'
  },
  ]
};