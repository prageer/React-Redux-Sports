/**
 * Archivo de traducción para Portugal.
 */
module.exports = {
  categoryId: 1353,
  cultureShortcut: 'pt',
  cultureHref: '/pt/',
  cultures: [{
    href: '/pt/',
    title: 'VAVEL Portugal',
    title_short: 'VAVEL Portugal',
    class: 'flag-pt'
  },
  {
    href: '/br/',
    title: 'VAVEL Brasil',
    title_short: 'Brasil',
    class: 'flag-br'
  },
  {
    href: '/en/',
    title: 'VAVEL UK',
    title_short: 'English',
    class: 'flag-uk'
  },
  {
    href: '/en-us/',
    title: 'VAVEL USA',
    title_short: 'USA',
    class: 'flag-usa'
  },
  {
    href: '/es/',
    title: 'VAVEL Espanha',
    title_short: 'Espanha',
    class: 'flag-es'
  },
  {
    href: '/ar/',
    title: 'VAVEL Argentina',
    title_short: 'Argentina',
    class: 'flag-ar'
  },
  {
    href: '/mx/',
    title: 'VAVEL México',
    title_short: 'MX',
    class: 'flag-mx'
  },
  {
    href: '/lat/',
    title: 'VAVEL América latina',
    title_short: 'LAT',
    class: 'flag-lat'
  },
  {
    href: '/it/',
    title: 'VAVEL Itália',
    title_short: 'Itália',
    class: 'flag-it'
  },
  {
    href: '/fr/',
    title: 'VAVEL França',
    title_short: 'França',
    class: 'flag-fr'
  },
  {
    href: '/arb/',
    title: 'VAVEL Árabe',
    title_short: 'Árabe',
    class: 'flag-arb'
  },
  ],
  latest: 'últimas',
  trendingTopics: 'Temas do momento:',
  writeInVavel: 'Write in Vavel',
  writerPanel: 'Writer panel',
  connectWithFacebook: 'Connect with facebook',
  news: 'Notícias',
  homeTitle: 'VAVEL Portugal - The international newspaper',
  facebookFrameUrl: '',
  locale: 'pt-pt',
  css: '/public/assets/css/cultures/portugal.css',
  homeDescription: 'As últimas notícias esportivas e artigos analisados em profundidade e qualidade, fotos, vídeos, resultados e jogos ao vivo no VAVEL, um jornal pensando diferente.',
  homeKeywords: 'noticias, última hora, atualidades, nacional, Brasil, vavel, periodismo, futebol, tênis, cinema, sociedade, política, televisão, TV, música, arte, basquete, internacional, calcio, premier league, bundesliga, liga, la liga, imprensa esportiva, jornais, esportes, esporte, imprensa, opinião, moda, blogs, especiais, vídeos, fotos, áudios, gráficos, entrevistas',
  contact: 'fale conosco',
  facebookCommunityUrl: '',
  facebookLikeFrameUrl: '',
  twitterButtonUrl: '',
  twitterButtonText: '',
  googlePlusCommunityUrl: '',
  googlePlusLikeFrameUrl: '',
  categoryTitle: '{categoryName} Notícias - VAVEL.com',
  categoryDescription: 'Últimas noticias, fotos e artigos sobre {categoryName} na VAVEL',
  categoryKeywords: '{categoryName},VAVEL Portugal - The international newspaper',
  articleTitle: '{title} - VAVEL.com',
  articleDescription: '{description}',
  articleKeywords: '{keywords}',
  taboolaUrl: '//cdn.taboola.com/libtrc/vaveleu-pt/loader.js',
  androidWidgetTitle: 'Notícias esportivas',
  androidWidgetAuthor: 'the VAVEL Brasil app',
  androidWidgetPrice: 'GRATIS',
  androidWidgetLanguage: 'portuguese',
  androidWidgetInAppStore: 'en App Store',
  androidWidgetInGooglePlay: 'en Google Play',
  facebookSdkCulture: 'pt_PT',
  twitterSite: '',
  googlePlusCommunity: '',
  twitterLikeVia: 'VAVEL_Brasil',
  lang: 'pt',
  flag: 'pt',
  goals: 'Placar',
  referee: 'ÁRBITRO',
  incidents: 'INCIDENCIAS',
  latestArticlesTaggedAs: 'Mais notícias de',
  latestArticlesTaggedNews: 'Notícias sobre {tag}',
  authorTitle: '{title} - VAVEL.com',
  authorDescriptionWithBio: 'The articles portfolio of {author_name}. {author_bio} in VAVEL platform.',
  authorDescriptionWithoutBio: 'The articles portfolio of {author_name} on the Author Zone in VAVEL platform.',
  authorKeywords: '{keywords}',
  authorInVavel: 'Autor na VAVEL',
  writer: 'Escritor',
  articles: 'Artigos',
  tagTitle: '{tag} - Notícias sobre {tag} na VAVEL Portugal',
  tagDescription: 'A mais recente sobre {tag}. Últimas noticias, fotos e artigos sobre {tag} na VAVEL Portugal.',
  tagKeywords: '{keywords}',
  homepage: 'Homepage',

  clubs: [{
    icon: 'submenu_1',
    href: '/pt/tag/sl-benfica/',
    title: 'Benfica',
    name: 'Benfica'
  },
  {
    icon: 'submenu_2',
    href: '/pt/tag/sporting-de-portugal/',
    title: 'Sporting de Portugal',
    name: 'Sporting de Portugal'
  },
  {
    icon: 'submenu_3',
    href: '/pt/tag/fc-porto/',
    title: 'FC Porto',
    name: 'FC Porto'
  },
  {
    icon: 'submenu_4',
    href: '/pt/tag/estoril/',
    title: 'Botafogo',
    name: 'Catania'
  },
  {
    icon: 'submenu_5',
    href: '/pt/tag/nacional/',
    title: 'Corinthians',
    name: 'Chievo'
  },
  {
    icon: 'submenu_6',
    href: '/pt/tag/maritimo/',
    title: 'Coritiba',
    name: 'Coritiba'
  },
  {
    icon: 'submenu_8',
    href: '/pt/tag/vitoria-setubal/',
    title: 'Criciúma',
    name: 'Crici&#250;ma'
  },
  {
    icon: 'submenu_7',
    href: '/pt/tag/academica/',
    title: 'Cruzeiro',
    name: 'Cruzeiro'
  },
  {
    icon: 'submenu_9',
    href: '/pt/tag/braga/',
    title: 'Flamengo',
    name: 'Flamengo'
  },
  {
    icon: 'submenu_10',
    href: '/pt/tag/vitoria-guimaraes/',
    title: 'Fluminense',
    name: 'Fluminense'
  },
  {
    icon: 'submenu_11',
    href: '/pt/tag/rio-ave',
    title: 'Goias',
    name: 'Goi&#225;s'
  },
  {
    icon: 'submenu_12',
    href: '/pt/tag/arouca/',
    title: 'Gremio',
    name: 'Gremio'
  },
  {
    icon: 'submenu_13',
    href: '/pt/tag/gil-vicente/',
    title: 'Internacional',
    name: 'Internacional'
  },
  {
    icon: 'submenu_14',
    href: '/pt/tag/belenenses/',
    title: 'Nautico',
    name: 'Nautico'
  },
  {
    icon: 'submenu_15',
    href: '/pt/tag/pacos-de-ferreira/',
    title: 'Ponte Preta',
    name: 'Ponte Preta'
  },
  {
    icon: 'submenu_16',
    href: '/pt/tag/moreirense/',
    title: 'Portuguesa',
    name: 'Portuguesa'
  },
  {
    icon: 'submenu_17',
    href: '/pt/tag/penafiel/',
    title: 'Santos',
    name: 'Santos'
  },
  {
    icon: 'submenu_18',
    href: '/pt/tag/boavista/',
    title: 'Sao Paulo',
    name: 'Sao Paulo'
  },
  ]
};