/**
 * Archivo de traducción para Estados Unidos.
 */
module.exports = {
  categoryId: 1013,
  cultureShortcut: 'en-us',
  cultureHref: '/en-us/',
  cultures: [{
    href: '/en-us/',
    title: 'VAVEL USA',
    title_short: 'VAVEL USA',
    class: 'flag-usa'
  },
  {
    href: '/en/',
    title: 'VAVEL UK',
    title_short: 'UK',
    class: 'flag-uk'
  },
  {
    href: '/es/',
    title: 'VAVEL Spain',
    title_short: 'Spain',
    class: 'flag-es'
  },
  {
    href: '/mx/',
    title: 'VAVEL Mexico',
    title_short: 'Mexico',
    class: 'flag-mx'
  },
  {
    href: '/lat/',
    title: 'VAVEL Latin America',
    title_short: 'Latin America',
    class: 'flag-lat'
  },
  {
    href: '/br/',
    title: 'VAVEL Brazil',
    title_short: 'Brazil',
    class: 'flag-br'
  },
  {
    href: '/it/',
    title: 'VAVEL Italia',
    title_short: 'Italia',
    class: 'flag-it'
  },
  {
    href: '/fr/',
    title: 'VAVEL France',
    title_short: 'France',
    class: 'flag-fr'
  },
  {
    href: '/arb/',
    title: 'VAVEL Arabic',
    title_short: 'Arabic',
    class: 'flag-arb'
  },
  ],
  latest: 'latest',
  trendingTopics: 'Trending topics:',
  writeInVavel: 'Write in Vavel',
  writerPanel: 'Writer panel',
  connectWithFacebook: 'Connect with facebook',
  news: 'News',
  homeTitle: 'VAVEL USA - The international newspaper',
  facebookFrameUrl: 'http://www.facebook.com/plugins/likebox.php?href=http://www.facebook.com/pages/VAVELcom-USA/280989918702764&width=300&colorscheme=light&show_faces=false&border_color&stream=false&header=true&height=62&appId=169320175188',
  locale: 'en-us',
  css: '/public/assets/css/cultures/usa.css',
  homeDescription: 'Sports latest news and articles analyzed in depth and quality, photos, videos, results and live scores comementaries from VAVEL, a  media thinking different.',
  homeKeywords: 'News, latest news, current affairs, national, english, uk, united kingdom, vavel, journalism, soccer, tennis, cinema, society, politics, television, TV, music, art, basketball, international, calcium, premier league, bundesliga, league, league, press sports, newspapers, sports, sports, news, opinion, fashion, blogs, signatures, special, video, photos, audio, graphics, interviews',
  contact: 'contact us',
  facebookCommunityUrl: 'http://www.facebook.com/VAVELcom/',
  facebookLikeFrameUrl: 'http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FVAVELcom&amp;send=false&amp;layout=button_count&amp;width=140&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=169320175188',
  twitterButtonUrl: 'https://twitter.com/VAVEL_USA',
  twitterButtonText: 'Follow @VAVEL_USA',
  googlePlusCommunityUrl: 'https://plus.google.com/110337344199993581140',
  googlePlusLikeFrameUrl: '//plus.google.com/110337344199993581140',
  categoryTitle: '{categoryName} News - VAVEL.com',
  categoryDescription: 'Breaking news, photos and articles about {categoryName} in VAVEL',
  categoryKeywords: '{categoryName},VAVEL USA - The international newspaper',
  articleTitle: '{title} - VAVEL.com',
  articleDescription: '{description}',
  articleKeywords: '{keywords}',
  taboolaUrl: '//cdn.taboola.com/libtrc/vaveleu-en/loader.js',
  androidWidgetTitle: 'Sport News and Scores',
  androidWidgetAuthor: 'the VAVEL app',
  androidWidgetPrice: 'FREE',
  androidWidgetLanguage: 'all languages',
  androidWidgetInAppStore: 'on the App Store',
  androidWidgetInGooglePlay: 'in Google Play',
  facebookSdkCulture: 'en_EN',
  twitterSite: '@VAVEL_USA',
  twitterLikeVia: 'VAVEL_USA',
  lang: 'en',
  flag: 'us',
  goals: 'SCORE',
  referee: 'REFEREE',
  incidents: 'INCIDENTS',
  latestArticlesTaggedAs: 'More news about',
  latestArticlesTaggedNews: 'News about {tag}',
  authorTitle: '{title} - VAVEL.com',
  authorDescriptionWithBio: 'The articles portfolio of {author_name}. {author_bio} in VAVEL platform.',
  authorDescriptionWithoutBio: 'The articles portfolio of {author_name} on the Author Zone in VAVEL platform.',
  authorKeywords: '{keywords}',
  authorInVavel: 'Author in VAVEL',
  writer: 'Writer',
  articles: 'Articles',
  tagTitle: '{tag} - News about {tag} in VAVEL USA',
  tagDescription: 'The latest about {tag}. Breaking news, photos and articles about {tag} in VAVEL USA.',
  tagKeywords: '{keywords}',
  homepage: 'Homepage',

  clubs: [{
    icon: 'submenu_1',
    href: '/en-us/tag/buffalo-bills/',
    title: 'Buffalo Bills',
    name: 'Buffalo Bills'
  },
  {
    icon: 'submenu_2',
    href: '/en-us/tag/miami-dolphins/',
    title: 'Miami Dolphins',
    name: 'Miami Dolphins'
  },
  {
    icon: 'submenu_3',
    href: '/en-us/tag/new-england-patriots/',
    title: 'New England Patriots',
    name: 'New England Patriots'
  },
  {
    icon: 'submenu_4',
    href: '/en-us/tag/new-york-jets/',
    title: 'New York Jets',
    name: 'New York Jets'
  },
  {
    icon: 'submenu_5',
    href: '/en-us/tag/baltimore-ravens/',
    title: 'Baltimore Ravens',
    name: 'Baltimore Ravens'
  },
  {
    icon: 'submenu_6',
    href: '/en-us/tag/cincinnati-bengals/',
    title: 'Cincinnati Bengals',
    name: 'Cincinnati Bengals'
  },
  {
    icon: 'submenu_7',
    href: '/en-us/tag/cleveland-browns/',
    title: 'Cleveland Browns',
    name: 'Cleveland Browns'
  },
  {
    icon: 'submenu_8',
    href: '/en-us/tag/pittsburgh-steelers/',
    title: 'Pittsburgh Steelers',
    name: 'Pittsburgh Steelers'
  },
  {
    icon: 'submenu_9',
    href: '/en-us/tag/houston-texans/',
    title: 'Houston Texans',
    name: 'Houston Texans'
  },
  {
    icon: 'submenu_10',
    href: '/en-us/tag/indianapolis-colts/',
    title: 'Indianapolis Colts',
    name: 'Indianapolis Colts'
  },
  {
    icon: 'submenu_11',
    href: '/en-us/tag/jacksonville- jaguars/',
    title: 'Jacksonville Jaguars',
    name: 'Jacksonville Jaguars'
  },
  {
    icon: 'submenu_12',
    href: '/en-us/tag/tennessee-titans/',
    title: 'Tennessee Titans',
    name: 'Tennessee Titans'
  },
  {
    icon: 'submenu_13',
    href: '/en-us/tag/denver-broncos/',
    title: 'Denver Broncos',
    name: 'Denver Broncos'
  },
  {
    icon: 'submenu_14',
    href: '/en-us/tag/kansas-city-chiefs/',
    title: 'Kansas City Chiefs',
    name: 'Kansas City Chiefs'
  },
  {
    icon: 'submenu_15',
    href: '/en-us/tag/oakland-raiders/',
    title: 'Oakland Raiders',
    name: 'Oakland Raiders'
  },
  {
    icon: 'submenu_16',
    href: '/en-us/tag/san-diego-chargers/',
    title: 'S.Diego Chargers',
    name: 'San Diego Chargers'
  },
  {
    icon: 'submenu_17',
    href: '/en-us/tag/dallas-cowboys/',
    title: 'Dallas Cowboys',
    name: 'Dallas Cowboys'
  },
  {
    icon: 'submenu_18',
    href: '/en-us/tag/ny-giants/',
    title: 'New York Giants',
    name: 'New York Giants'
  },
  {
    icon: 'submenu_19',
    href: '/en-us/tag/philadelphia-eagles/',
    title: 'Philadelphia Eagles',
    name: 'Philadelphia Eagles'
  },
  {
    icon: 'submenu_20',
    href: '/en-us/tag/washington-redskins/',
    title: 'Washington Redskins',
    name: 'Washington Redskins'
  },
  {
    icon: 'submenu_21',
    href: '/en-us/tag/chicago-bears/',
    title: 'Chicago Bears',
    name: 'Chicago Bears'
  },
  {
    icon: 'submenu_22',
    href: '/en-us/tag/detroit-lions/',
    title: 'Detroit Lions',
    name: 'Detroit Lions'
  },
  {
    icon: 'submenu_23',
    href: '/en-us/tag/green-bay-packers/',
    title: 'Green Bay Packers',
    name: 'Green Bay Packers'
  },
  {
    icon: 'submenu_24',
    href: '/en-us/tag/minnnesota-vikings/',
    title: 'Minnesota Vikings',
    name: 'Minnesota Vikings'
  },
  {
    icon: 'submenu_25',
    href: '/en-us/tag/atlanta-falcons/',
    title: 'Atlanta Falcons',
    name: 'Atlanta Falcons'
  },
  {
    icon: 'submenu_26',
    href: '/en-us/tag/carolina-panthers/',
    title: 'Carolina Panthers',
    name: 'Carolina Panthers'
  },
  {
    icon: 'submenu_27',
    href: '/en-us/tag/new-orleans-saints/',
    title: 'New Orleans Saints',
    name: 'New Orleans Saints'
  },
  {
    icon: 'submenu_28',
    href: '/en-us/tag/tampa-bay-bucaneers/',
    title: 'Tampa Bay Bucaneers',
    name: 'Tampa Bay Bucaneers'
  },
  {
    icon: 'submenu_29',
    href: '/en-us/tag/arizona-cardinals/',
    title: 'Arizona Cardinals',
    name: 'Arizona Cardinals'
  },
  {
    icon: 'submenu_30',
    href: '/en-us/tag/st-louis-rams/',
    title: 'St.Louis Rams',
    name: 'St. Louis Rams'
  },
  {
    icon: 'submenu_31',
    href: '/en-us/tag/san-francisco-49ers/',
    title: 'S.Francisco 49ers',
    name: 'San Francisco 49ers'
  },
  {
    icon: 'submenu_32',
    href: '/en-us/tag/seattle-seahawks/',
    title: 'Seattle Seahawks',
    name: 'Seattle Seahawks'
  },
  ]
};