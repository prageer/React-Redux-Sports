/**
 * Archivo de traducción para Francia.
 */
module.exports = {
  categoryId: 218,
  cultureShortcut: 'fr',
  cultureHref: '/fr/',
  cultures: [{
    href: '/fr/',
    title: 'VAVEL France',
    title_short: 'VAVEL France',
    class: 'flag-fr'
  },
  {
    href: '/en-us/',
    title: 'VAVEL USA',
    title_short: 'USA',
    class: 'flag-usa'
  },
  {
    href: '/en/',
    title: 'VAVEL UK',
    title_short: 'UK',
    class: 'flag-uk'
  },
  {
    href: '/es/',
    title: 'VAVEL Espagne',
    title_short: 'Espagne',
    class: 'flag-es'
  },
  {
    href: '/lat/',
    title: 'VAVEL Amérique latine',
    title_short: 'Latinoamerica',
    class: 'flag-lat'
  },
  {
    href: '/it/',
    title: 'VAVEL Italia',
    title_short: 'VAVEL Italia',
    class: 'flag-it'
  },
  {
    href: '/br/',
    title: 'VAVEL Brésil',
    title_short: 'Brésil',
    class: 'flag-br'
  },
  {
    href: '/arb/',
    title: 'VAVEL Arabe',
    title_short: 'Arabe',
    class: 'flag-arb'
  },
  ],
  latest: 'dernier',
  trendingTopics: 'Sujets du jour:',
  writeInVavel: 'Write in Vavel',
  writerPanel: 'Writer panel',
  connectWithFacebook: 'Connect with facebook',
  news: 'Actualités',
  homeTitle: 'VAVEL France - The international sports newspaper',
  facebookFrameUrl: 'http://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/pages/VAVELcom-France/213876765345952&width=300&colorscheme=light&show_faces=false&border_color&stream=false&header=true&height=62&appId=169320175188',
  locale: 'fr-fr',
  css: '/public/assets/css/cultures/france.css',
  homeDescription: 'Le meilleur contenu sportif analysé dans profondeur et qualité.',
  homeKeywords: 'Nouvelles, actualités, nationale, France, Vavel, le journalisme, le soccer, tennis, cinéma, société, politique, télévision, TV, musique, art, basket-ball, internationale, de calcium, Premier League, Bundesliga, Ligue, ligue, le sport, sports, opinion, mode, blogs, vidéo, photos, audio, graphiques, interviews',
  contact: 'contactez-nous',
  facebookCommunityUrl: 'http://www.facebook.com/VAVELfrance/',
  facebookLikeFrameUrl: 'http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FVAVELfrance&amp;send=false&amp;layout=button_count&amp;width=140&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=169320175188',
  twitterButtonUrl: 'https://twitter.com/vavel_france',
  twitterButtonText: 'Follow @VAVEL_France',
  googlePlusCommunityUrl: 'https://plus.google.com/113255918873085772341',
  googlePlusLikeFrameUrl: '//plus.google.com/113255918873085772341',
  categoryTitle: '{categoryName} Actualités - VAVEL.com',
  categoryDescription: 'Actualités, photos et articles sur {categoryName} dans VAVEL',
  categoryKeywords: '{categoryName},VAVEL France - The international sports newspaper',
  articleTitle: '{title} - VAVEL.com',
  articleDescription: '{description}',
  articleKeywords: '{keywords}',
  taboolaUrl: '//cdn.taboola.com/libtrc/vaveleu-fr/loader.js',
  androidWidgetTitle: 'Actualité sportive',
  androidWidgetAuthor: 'the VAVEL France app',
  androidWidgetPrice: 'GRATIS',
  androidWidgetLanguage: 'french',
  androidWidgetInAppStore: 'en App Store',
  androidWidgetInGooglePlay: 'en Google Play',
  facebookSdkCulture: 'fr_FR',
  twitterSite: '@VAVEL_France',
  twitterLikeVia: 'VAVEL_france',
  lang: 'fr',
  flag: 'fr',
  goals: 'SCORE',
  referee: 'ARBITRE',
  incidents: 'ÈVÉNEMENTS',
  latestArticlesTaggedAs: 'Sujets',
  latestArticlesTaggedNews: 'Actualités sur {tag}',
  authorTitle: '{title} - VAVEL.com',
  authorDescriptionWithBio: 'The articles portfolio of {author_name}. {author_bio} in VAVEL platform.',
  authorDescriptionWithoutBio: 'The articles portfolio of {author_name} on the Author Zone in VAVEL platform.',
  authorKeywords: '{keywords}',
  authorInVavel: 'Auteur sur VAVEL',
  writer: 'Écrivain',
  articles: 'Articles',
  tagTitle: '{tag} - Actualités sur {tag} dans VAVEL France',
  tagDescription: 'La dernière du {tag}. Actualités, photos et articles sur {tag} dans VAVEL France.',
  tagKeywords: '{keywords}',
  homepage: 'Homepage',

  clubs: [{
    icon: 'submenu_1',
    href: '/fr/football/psg/',
    title: 'Paris Saint Germain',
    name: 'PSG'
  },
  {
    icon: 'submenu_2',
    href: '/fr/football/as-monaco/',
    title: 'AS Monaco',
    name: 'ASM'
  },
  {
    icon: 'submenu_3',
    href: '/fr/football/lille/',
    title: 'Lille',
    name: 'LOSC'
  },
  {
    icon: 'submenu_4',
    href: '/fr/football/saint-etienne/',
    title: 'Saint-Etienne',
    name: 'ASSE'
  },
  {
    icon: 'submenu_5',
    href: '/fr/football/lyon/',
    title: 'Olympique Lyonnais',
    name: 'OL'
  },
  {
    icon: 'submenu_6',
    href: '/fr/football/marseille/',
    title: 'Olympique Marseille',
    name: 'OM'
  },
  {
    icon: 'submenu_7',
    href: '/fr/football/girondinsbordeaux/',
    title: 'Girondins Bordeaux',
    name: 'FCGB'
  },
  {
    icon: 'submenu_8',
    href: '/fr/football/lorient/',
    title: 'Lorient',
    name: 'FCL'
  },
  {
    icon: 'submenu_9',
    href: '/fr/football/toulouse/',
    title: 'Toulouse',
    name: 'TFC'
  },
  {
    icon: 'submenu_10',
    href: '/fr/football/bastia/',
    title: 'Bastia',
    name: 'SCB'
  },
  {
    icon: 'submenu_11',
    href: '/fr/football/stade-de-reims//',
    title: 'Stade de Reims',
    name: 'SDR'
  },
  {
    icon: 'submenu_12',
    href: '/fr/football/rennes/',
    title: 'Rennes',
    name: 'SRFC'
  },
  {
    icon: 'submenu_13',
    href: '/fr/football/nantes/',
    title: 'FC Nantes',
    name: 'FCN'
  },
  {
    icon: 'submenu_14',
    href: '/fr/football/scoangers/',
    title: 'SCO Angers',
    name: 'SCO'
  },
  {
    icon: 'submenu_15',
    href: '/fr/football/montpellier/',
    title: 'Montpellier',
    name: 'MHSC'
  },
  {
    icon: 'submenu_16',
    href: '/fr/football/guingamp/',
    title: 'Guingamp',
    name: 'EAG'
  },
  {
    icon: 'submenu_17',
    href: '/fr/football/nice/',
    title: 'Nice',
    name: 'OGCN'
  },
  {
    icon: 'submenu_18',
    href: '/fr/tag/gazélec',
    title: 'Gazélec Ajaccio ',
    name: 'GFCA'
  },
  {
    icon: 'submenu_19',
    href: '/fr/tag/troyes',
    title: 'Troyes',
    name: 'ESTAC'
  },
  {
    icon: 'submenu_20',
    href: '/fr/football/caen/',
    title: 'Caen',
    name: 'SMC'
  },
  ]
};