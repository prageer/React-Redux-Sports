/*eslint no-useless-concat: "off"*/

/**
 * Archivo de traducción para Arabia.
 */
module.exports = {
  categoryId: 829,
  cultureShortcut: 'arb',
  cultureHref: '/arb/',
  cultures: [{
    href: '/arb/',
    title: 'VAVEL عربي',
    title_short: 'عربي',
    class: 'flag-arb'
  },
  {
    href: '/en/',
    title: 'VAVEL المملكة المتحدة',
    title_short: 'English',
    class: 'flag-uk'
  },
  {
    href: '/en-us/',
    title: 'VAVEL المملكة المتحدة',
    title_short: 'USA',
    class: 'flag-usa'
  },
  {
    href: '/es/',
    title: 'VAVEL اسبانيا',
    title_short: 'اسبانيا',
    class: 'flag-es'
  },
  {
    href: '/fr/',
    title: 'VAVEL فرنسا',
    title_short: 'فرنسا',
    class: 'flag-fr'
  },
  {
    href: '/br/',
    title: 'VAVEL البرازيل',
    title_short: 'البرازيل',
    class: 'flag-br'
  },
  {
    href: '/it/',
    title: 'VAVEL إيطاليا',
    title_short: 'إيطاليا ',
    class: 'flag-it'
  },
  {
    href: '/lat/',
    title: 'VAVEL أمريكا اللاتينية',
    title_short: 'أمريكا اللاتينية',
    class: 'flag-lat'
  },
  ],
  latest: 'أخير',
  trendingTopics: 'تتجه مواضيع',
  writeInVavel: 'يكتب Vavel',
  writerPanel: 'أكتب مقال',
  connectWithFacebook: 'Connect with facebook',
  news: 'أخبار',
  homeTitle: 'بالعربية - الصحيفة العالمية VAVEL',
  facebookFrameUrl: 'http://www.facebook.com/plugins/like_box.php?app_id=169320175188&channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2FwjDNIDNrTQG.js%3Fversion%3D41%23cb%3Df9f74631c%26domain%3Dwww.vavel.com%26origin%3Dhttp%253A%252F%252Fwww.vavel.com%252Ffcd6a16dc%26relation%3Dparent.parent&container_width=300&header=false&href=http%3A%2F%2Fwww.facebook.com%2FVAVELArabic&locale=ar_AR&sdk=joey&show_faces=false&stream=false&width=300',
  locale: 'ar',
  css: '/public/assets/css/cultures/arabic.css',
  homeDescription: 'هو وسيلة إعلامية تربط الصحفيين ، المحررين و المدونين مع القراء VAVEL .',
  homeKeywords: 'أخبار,أخر الأخبار ,حديث الساعة, وطني, إسبانيا,أمريكا اللاتينية,المكسيك,الأرجنتين, vavel, الإعلام ,كرة القدم ,التنس,السينما,المجتمع,السياسة, التلفزيون, الموسيقى,الفن,كرة السلة,العالمية, الدوري الايطالي ,الدوري الانجليزي,الدوري الالماني,الدوري الاسباني, الصحافة الرياضية,الصحف, الرياضة, الرياضة,الصحافة, اراء, الموضة, المدونات, تقارير خاصة, vídeos, الصور, السمعيات, الرسوم البيانية, المقابلات الصحفية',
  contact: 'إتصل بنا',
  facebookCommunityUrl: 'http://www.facebook.com/VAVELArabic/',
  facebookLikeFrameUrl: 'http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FVAVELArabic&amp;send=false&amp;layout=button_count&amp;width=140&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=169320175188',
  twitterButtonUrl: 'https://twitter.com/VAVEL_Arabic',
  twitterButtonText: 'Follow @VAVEL_Arabic',
  googlePlusCommunityUrl: 'https://plus.google.com/101875740215383359029',
  googlePlusLikeFrameUrl: '//plus.google.com/101875740215383359029',
  categoryTitle: '{categoryName} | VAVEL.com',
  categoryDescription: 'اخر الاخبار والمقالات عن' + '{categoryName}' + 'VAVEL.com في',
  categoryKeywords: '{categoryName},بالعربية - الصحيفة العالمية VAVEL',
  articleTitle: '{title} - VAVEL.com',
  articleDescription: '{description}',
  articleKeywords: '{keywords}',
  facebookSdkCulture: 'en_EN',
  twitterSite: '@VAVEL_ARABIC',
  twitterLikeVia: 'VAVEL_Arabic',
  lang: 'arb',
  flag: 'arb',
  goals: 'مسجل الأهداف',
  referee: 'الحكم',
  incidents: 'الأحداث',
  latestArticlesTaggedAs: 'كلماته الدلالية',
  latestArticlesTaggedNews: 'News about {tag}',
  authorTitle: '{title} - VAVEL.com',
  authorDescriptionWithBio: 'The articles portfolio of {author_name}. {author_bio} in VAVEL platform.',
  authorDescriptionWithoutBio: 'The articles portfolio of {author_name} on the Author Zone in VAVEL platform.',
  authorKeywords: '{keywords}',
  authorInVavel: 'المؤلف في VAVEL',
  writer: 'الكاتب',
  articles: 'مقالات',
  tagTitle: '{tag} - News about {tag} VAVEL.com في Arabic',
  tagDescription: 'The latest about {tag}. اخر الاخبار والمقالات عن {tag} VAVEL.com في Arabic.',
  tagKeywords: '{keywords}',
  homepage: 'Homepage',

  clubs: [{
    icon: 'submenu_1',
    href: '/arb/',
    title: 'VAVEL Arabic',
    name: ''
  },
  {
    icon: 'submenu_24',
    href: '/arb/tag/البرازيل/',
    title: 'Brazil',
    name: 'البرازيل'
  },
  {
    icon: 'submenu_23',
    href: '/arb/tag/فرنسا/',
    title: 'France',
    name: 'فرنسا'
  },
  {
    icon: 'submenu_22',
    href: '/arb/tag/ايطاليا/',
    title: 'Italy',
    name: 'ايطاليا'
  },
  {
    icon: 'submenu_21',
    href: '/arb/tag/المانيا/',
    title: 'Germany',
    name: 'المانيا'
  },
  {
    icon: 'submenu_20',
    href: '/arb/tag/انجلترا/',
    title: 'England',
    name: 'انجلترا'
  },
  {
    icon: 'submenu_19',
    href: '/arb/tag/اسبانيا/',
    title: 'Spain',
    name: 'اسبانيا'
  },
  {
    icon: 'submenu_5',
    href: '/arb/bahrain/',
    title: 'Bahrain',
    name: 'البحرين'
  },
  {
    icon: 'submenu_18',
    href: '/arb/emirates/',
    title: 'Emirates',
    name: 'الامارات'
  },
  {
    icon: 'submenu_12',
    href: '/arb/irak/',
    title: 'Irak',
    name: 'العراق'
  },
  {
    icon: 'submenu_14',
    href: '/arb/egypt/',
    title: 'Egypt',
    name: 'مصر'
  },
  {
    icon: 'submenu_13',
    href: '/arb/qatar/',
    title: 'Qatar',
    name: 'قطر'
  },
  {
    icon: 'submenu_9',
    href: '/arb/tunisia/',
    title: 'Tunisia',
    name: 'تونس'
  },
  {
    icon: 'submenu_15',
    href: '/arb/morocco/',
    title: 'Morocco',
    name: 'مغربي'
  },
  {
    icon: 'submenu_16',
    href: '/arb/algeria/',
    title: 'Algeria',
    name: 'الجزائر'
  },
  {
    icon: 'submenu_11',
    href: '/arb/kuwait/',
    title: 'Kuwait',
    name: 'الكويت'
  },
  {
    icon: 'submenu_17',
    href: '/arb/saudi/',
    title: 'Saudi Arabia',
    name: 'السعودية'
  },
  ]
};