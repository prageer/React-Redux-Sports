/**
 * Archivo de traducción para Argentinda.
 */
module.exports = {
  categoryId: 107,
  cultureShortcut: 'ar',
  cultureHref: '/ar/',
  cultures: [{
    href: '/ar/',
    title: 'VAVEL Argentina',
    title_short: 'VAVEL Argentina',
    class: 'flag-ar'
  },
  {
    href: '/es/',
    title: 'VAVEL España',
    title_short: 'España',
    class: 'flag-es'
  },
  {
    href: '/mx/',
    title: 'VAVEL México',
    title_short: 'México',
    class: 'flag-mx'
  },
  {
    href: '/lat/',
    title: 'VAVEL Latinoamerica',
    title_short: 'LAT',
    class: 'flag-lat'
  },
  {
    href: '/en-us/',
    title: 'VAVEL USA',
    title_short: 'USA',
    class: 'flag-usa'
  },
  {
    href: '/en/',
    title: 'VAVEL UK',
    title_short: 'UK',
    class: 'flag-uk'
  },
  {
    href: '/br/',
    title: 'VAVEL Brasil',
    title_short: 'Brasil',
    class: 'flag-br'
  },
  {
    href: '/it/',
    title: 'VAVEL Italia',
    title_short: 'IT',
    class: 'flag-it'
  },
  {
    href: '/fr/',
    title: 'VAVEL Francia',
    title_short: 'FR',
    class: 'flag-fr'
  },
  {
    href: '/arb/',
    title: 'VAVEL Árabe',
    title_short: 'Árabe',
    class: 'flag-arb'
  },
  ],
  latest: 'lo último',
  trendingTopics: 'Temas del momento:',
  writeInVavel: 'Write in Vavel',
  writerPanel: 'Writer panel',
  connectWithFacebook: 'Connect with facebook',
  news: 'Noticias',
  homeTitle: 'VAVEL Argentina - The international sports newspaper',
  facebookFrameUrl: 'http://www.facebook.com/plugins/likebox.php?href=http://www.facebook.com/pages/VAVELcom-Argentina/126618257487106&width=300&colorscheme=light&show_faces=false&border_color&stream=false&header=true&height=62&appId=169320175188',
  locale: 'es-ar',
  css: '/public/assets/css/cultures/argentina.css',
  homeDescription: 'Las últimas noticias deportivas y artículos analizados en profundidad y calidad, fotos, vídeos, resultados y partidos en vivo minuto a minuto en VAVEL, un periódico pensando diferente.',
  homeKeywords: 'noticias, última hora, actualidad, nacional, España, Latinoamérica, México, Argentina, vavel, periodismo, fútbol, tenis, cine, sociedad, política, televisión, TV, música, arte, baloncesto, internacional, calcio, premier league, bundesliga, liga, la liga, prensa deportiva, periódicos, deportes, deporte, prensa, opinión, moda, blogs, firmas, especiales, vídeos, fotos, audios, gráficos, entrevistas',
  contact: 'contacto',
  facebookCommunityUrl: 'http://www.facebook.com/VAVELArgentina/',
  facebookLikeFrameUrl: 'http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FVAVELArgentina&send=false&layout=button_count&width=140&show_faces=false&action=like&colorscheme=light&font&height=21&appId=169320175188',
  twitterButtonUrl: 'https://twitter.com/VAVEL_Argentina',
  twitterButtonText: 'Follow @VAVEL_Argentina',
  googlePlusCommunityUrl: 'https://plus.google.com/101330325480501409183',
  googlePlusLikeFrameUrl: '//plus.google.com/101330325480501409183',
  categoryTitle: '{categoryName} Noticias - VAVEL.com',
  categoryDescription: 'Noticias, fotos, actualidad, información y artículos de última hora sobre {categoryName} en VAVEL',
  categoryKeywords: '{categoryName},VAVEL Argentina - The international sports newspaper',
  articleTitle: '{title} - VAVEL.com',
  articleDescription: '{description}',
  articleKeywords: '{keywords}',
  taboolaUrl: '//cdn.taboola.com/libtrc/vaveleu-es/loader.js',
  androidWidgetTitle: 'Noticias deportivas',
  androidWidgetAuthor: 'the VAVEL España app',
  androidWidgetPrice: 'GRATIS',
  androidWidgetLanguage: 'espanol',
  androidWidgetInAppStore: 'en App Store',
  androidWidgetInGooglePlay: 'en Google Play',
  facebookSdkCulture: 'es_ES',
  twitterSite: '@VAVEL_ARGENTINA',
  twitterLikeVia: 'VAVEL_Argentina',
  lang: 'es',
  flag: 'arg',
  goals: 'MARCADOR',
  referee: 'ÁRBITRO',
  incidents: 'INCIDENCIAS',
  latestArticlesTaggedAs: 'Más noticias sobre',
  latestArticlesTaggedNews: 'Las últimas noticias sobre {tag}',
  authorTitle: '{title} - VAVEL.com',
  authorDescriptionWithBio: 'The articles portfolio of {author_name}. {author_bio} in VAVEL platform.',
  authorDescriptionWithoutBio: 'The articles portfolio of {author_name} on the Author Zone in VAVEL platform.',
  authorKeywords: '{keywords}',
  authorInVavel: 'Autor en VAVEL',
  writer: 'Escritor',
  articles: 'Articulos',
  tagTitle: '{tag} - Las últimas noticias sobre {tag} en VAVEL Argentina',
  tagDescription: 'Lo último sobre {tag}. Noticias, fotos, actualidad, información y artículos de última hora sobre {tag} en VAVEL Argentina.',
  tagKeywords: '{keywords}',
  homepage: 'Homepage',
  clubs: []
};