/**
 * Archivo de traducción para Italia.
 */
module.exports = {
  categoryId: 162,
  cultureShortcut: 'it',
  cultureHref: '/it/',
  cultures: [{
    href: '/it/',
    title: 'VAVEL Italia',
    title_short: 'VAVEL Italia',
    class: 'flag-it'
  },
  {
    href: '/en-us/',
    title: 'VAVEL USA',
    title_short: 'USA',
    class: 'flag-usa'
  },
  {
    href: '/en/',
    title: 'VAVEL UK',
    title_short: 'UK',
    class: 'flag-uk'
  },
  {
    href: '/es/',
    title: 'VAVEL Spagna',
    title_short: 'Spagna',
    class: 'flag-es'
  },
  {
    href: '/mx/',
    title: 'VAVEL México',
    title_short: 'México',
    class: 'flag-mx'
  },
  {
    href: '/lat/',
    title: 'VAVEL Latinoamerica',
    title_short: 'Latinoamerica',
    class: 'flag-lat'
  },
  {
    href: '/br/',
    title: 'VAVEL Brasile',
    title_short: 'Brasile',
    class: 'flag-br'
  },
  {
    href: '/fr/',
    title: 'VAVEL Francia',
    title_short: 'Francia',
    class: 'flag-fr'
  },
  {
    href: '/arb/',
    title: 'VAVEL Arabo',
    title_short: 'Arabo',
    class: 'flag-arb'
  },
  ],
  latest: 'ultime',
  trendingTopics: 'Temi del giorno:',
  writeInVavel: 'Write in Vavel',
  writerPanel: 'Writer panel',
  connectWithFacebook: 'Connect with facebook',
  news: 'Notizie',
  homeTitle: 'VAVEL Italia - The international sports newspaper',
  facebookFrameUrl: 'http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FVAVELcom-Italia%2F174423659289617&width=300&colorscheme=light&show_faces=false&border_color&stream=false&header=true&height=62&appId=169320175188',
  locale: 'it-it',
  css: '/public/assets/css/cultures/italia.css',
  homeDescription: 'Il miglior contenuto sportivo analizzato in profondità e qualità.',
  homeKeywords: 'News, Ultime notizie, attualità, nazionali, italia, vavel, giornalismo, calcio, tennis, cinema, società, politica, televisione, tv, musica, arte, basket, internazionale, calcio, Premier League, Bundesliga, campionato, campionato, premere sport, giornali, sport, sport, notizie, opinioni, moda, blog, le firme, speciali, video, foto, audio, grafica, interviste',
  contact: 'contatto',
  facebookCommunityUrl: 'http://www.facebook.com/VAVELItalia/',
  facebookLikeFrameUrl: 'http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FVAVELItalia&amp;send=false&amp;layout=button_count&amp;width=140&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=169320175188',
  twitterButtonUrl: 'https://twitter.com/VAVEL_italia',
  twitterButtonText: 'Follow @VAVEL_Italia',
  googlePlusCommunityUrl: 'https://plus.google.com/102427881809042940580',
  googlePlusLikeFrameUrl: '//plus.google.com/102427881809042940580',
  categoryTitle: '{categoryName} Notizie - VAVEL.com',
  categoryDescription: 'Notizie, foto e articoli su {categoryName} su VAVEL',
  categoryKeywords: '{categoryName},VAVEL Italia - The international sports newspaper',
  articleTitle: '{title} - VAVEL.com',
  articleDescription: '{description}',
  articleKeywords: '{keywords}',
  taboolaUrl: '',
  androidWidgetTitle: 'Notizie Sportive',
  androidWidgetAuthor: 'the VAVEL Italia app',
  androidWidgetPrice: 'GRATIS',
  androidWidgetLanguage: 'italian',
  androidWidgetInAppStore: 'su App Store',
  androidWidgetInGooglePlay: 'su Google Play',
  facebookSdkCulture: 'it_IT',
  twitterSite: '@VAVEL_Italia',
  twitterLikeVia: 'VAVEL_Italia',
  lang: 'it',
  flag: 'it',
  goals: 'SCORE',
  referee: 'ARBITRO',
  incidents: 'NOT',
  latestArticlesTaggedAs: 'Piú notizie su',
  latestArticlesTaggedNews: 'Notizie su {tag}',
  authorTitle: '{title} - VAVEL.com',
  authorDescriptionWithBio: 'The articles portfolio of {author_name}. {author_bio} in VAVEL platform.',
  authorDescriptionWithoutBio: 'The articles portfolio of {author_name} on the Author Zone in VAVEL platform.',
  authorKeywords: '{keywords}',
  authorInVavel: 'Autori su VAVEL',
  writer: 'Scrittore',
  articles: 'Articoli',
  tagTitle: '{tag} - Notizie su {tag} su VAVEL Italia',
  tagDescription: 'Le ultime su {tag}. Notizie, foto e articoli su {tag} su VAVEL Italia.',
  tagKeywords: '{keywords}',
  homepage: 'Homepage',

  clubs: [{
    icon: 'submenu_1',
    href: '/it/calcio/juventus/',
    title: 'Juventus',
    name: 'JUV'
  },
  {
    icon: 'submenu_2',
    href: '/it/calcio/roma/',
    title: 'Roma',
    name: 'ROM'
  },
  {
    icon: 'submenu_20',
    href: '/it/calcio/lazio/',
    title: 'SS Lazio',
    name: 'LAZ'
  },
  {
    icon: 'submenu_3',
    href: '/it/calcio/fiorentina/',
    title: 'Fiorentina',
    name: 'FIO'
  },
  {
    icon: 'submenu_4',
    href: '/it/calcio/napoli/',
    title: 'Napoli',
    name: 'NAP'
  },
  {
    icon: 'submenu_5',
    href: '/it/calcio/genoa/',
    title: 'Genoa',
    name: 'GEN'
  },
  {
    icon: 'submenu_6',
    href: '/it/calcio/sampdoria/',
    title: 'Sampdoria',
    name: 'SAM'
  },
  {
    icon: 'submenu_7',
    href: '/it/calcio/inter/',
    title: 'Inter',
    name: 'INT'
  },
  {
    icon: 'submenu_8',
    href: '/it/calcio/torino/',
    title: 'Torino',
    name: 'TOR'
  },
  {
    icon: 'submenu_9',
    href: '/it/calcio/milan/',
    title: 'AC Milan',
    name: 'MIL'
  },
  {
    icon: 'submenu_10',
    href: '/it/calcio/palermo/',
    title: 'Parlermo',
    name: 'PAL'
  },
  {
    icon: 'submenu_11',
    href: '/it/tag/sassuolo/',
    title: 'Sassuolo',
    name: 'SAS'
  },
  {
    icon: 'submenu_12',
    href: '/it/tag/hellas-verona/',
    title: 'Hellas Verona',
    name: 'VER'
  },
  {
    icon: 'submenu_13',
    href: '/it/calcio/chievo/',
    title: 'Chievo',
    name: 'CHI'
  },
  {
    icon: 'submenu_14',
    href: '/it/calcio/empoli/',
    title: 'Empoli',
    name: 'EMP'
  },
  {
    icon: 'submenu_15',
    href: '/it/calcio/udinese/',
    title: 'Udinese',
    name: 'UDI'
  },
  {
    icon: 'submenu_16',
    href: '/it/calcio/atalanta/',
    title: 'Atalanta',
    name: 'ATA'
  },
  {
    icon: 'submenu_17',
    href: '/it/tag/carpi/',
    title: 'Carpi',
    name: 'CAR'
  },
  {
    icon: 'submenu_18',
    href: '/it/tag/frosinone/',
    title: 'Frosinone',
    name: 'FRO'
  },
  {
    icon: 'submenu_19',
    href: '/it/calcio/bologna/',
    title: 'Bologna',
    name: 'BOL'
  },
  ]
};