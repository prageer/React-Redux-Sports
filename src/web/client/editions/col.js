/**
 * Archivo de traducción para Colombia.
 */
module.exports = {
  parentCategoryId: 196,
  cultureShortcut: 'colombia',
  cultureHref: '/colombia/',
  cultures: [{
    href: '/colombia/',
    title: 'VAVEL Colombia',
    title_short: 'Colombia',
    class: 'flag-co'
  },
  {
    href: '/es/',
    title: 'VAVEL España',
    title_short: 'VAVEL España',
    class: 'flag-es'
  },
  {
    href: '/en/',
    title: 'VAVEL UK',
    title_short: 'English',
    class: 'flag-uk'
  },
  {
    href: '/en-us/',
    title: 'VAVEL USA',
    title_short: 'USA',
    class: 'flag-usa'
  },
  {
    href: '/mx/',
    title: 'VAVEL México',
    title_short: 'México',
    class: 'flag-mx'
  },
  {
    href: '/ar/',
    title: 'VAVEL Argentina',
    title_short: 'ARG',
    class: 'flag-ar'
  },
  {
    href: '/lat/',
    title: 'VAVEL Latinoamerica',
    title_short: 'LAT',
    class: 'flag-lat'
  },
  {
    href: '/br/',
    title: 'VAVEL Brasil',
    title_short: 'Brasil',
    class: 'flag-br'
  },
  {
    href: '/pt/',
    title: 'VAVEL Portugal',
    title_short: 'PT',
    class: 'flag-pt'
  },
  {
    href: '/it/',
    title: 'VAVEL Italia',
    title_short: 'IT',
    class: 'flag-it'
  },
  {
    href: '/fr/',
    title: 'VAVEL France',
    title_short: 'France',
    class: 'flag-fr'
  },
  {
    href: '/arb/',
    title: 'VAVEL Arabic',
    title_short: 'ARB',
    class: 'flag-arb'
  },
  ],
  categoryId: 228,
  homeTitle: 'VAVEL Colombia | VAVEL Latinoamérica - The international sports newspaper',
  locale: 'es-co',
  css: '/public/assets/css/cultures/colombia.css',
  facebookCommunityUrl: '',
  facebookLikeFrameUrl: '',
  twitterButtonUrl: '',
  twitterButtonText: '',
  googlePlusCommunityUrl: '',
  googlePlusLikeFrameUrl: '',
  lang: 'es',
  flag: 'col',

  clubs: [{
    icon: 'submenu_1',
    href: '/lat/tag/aguilas-doradas',
    title: 'Águilas Doradas',
    name: 'AGUI'
  },
  {
    icon: 'submenu_2',
    href: '/lat/tag/alianza-petrolera',
    title: 'Alianza Petrolera',
    name: 'ALI'
  },
  {
    icon: 'submenu_3',
    href: '/colombia/futbol-colombiano/deportivo-cali/',
    title: 'Deportivo Cali',
    name: 'DCA'
  },
  {
    icon: 'submenu_4',
    href: '/lat/tag/boyaca-chico',
    title: 'Boyacá Chicó',
    name: 'BOY'
  },
  {
    icon: 'submenu_5',
    href: '/lat/tag/envigado',
    title: 'Envigado FC',
    name: 'ENV'
  },
  {
    icon: 'submenu_6',
    href: '/lat/tag/equidad-seguros',
    title: 'Equidad Seguros',
    name: 'EQU'
  },
  {
    icon: 'submenu_7',
    href: '/lat/tag/cortulua',
    title: 'Cortuluá',
    name: 'COR'
  },
  {
    icon: 'submenu_8',
    href: '/lat/tag/atletico-huila',
    title: 'Atlético Huila',
    name: 'ATH'
  },
  {
    icon: 'submenu_9',
    href: '/colombia/futbol-colombiano/atletico-junior/',
    title: 'Atlético Junior',
    name: 'JUN'
  },
  {
    icon: 'submenu_10',
    href: '/colombia/futbol-colombiano/independiente-medellin/',
    title: 'Deportivo Independiente Medellín',
    name: 'DIM'
  },
  {
    icon: 'submenu_11',
    href: '/colombia/futbol-colombiano/millonarios/',
    title: 'Millonarios FC',
    name: 'MIL'
  },
  {
    icon: 'submenu_12',
    href: '/colombia/futbol-colombiano/nacional/',
    title: 'Atlético Nacional',
    name: 'NAC'
  },
  {
    icon: 'submenu_13',
    href: '/colombia/futbol-colombiano/once-caldas/',
    title: 'Once Caldas',
    name: 'OCA'
  },
  {
    icon: 'submenu_14',
    href: '/lat/tag/deportivo-pasto',
    title: 'Deportivo Pasto',
    name: 'DPA'
  },
  {
    icon: 'submenu_15',
    href: '/lat/tag/patriotas',
    title: 'Patriotas FC',
    name: 'PAT'
  },
  {
    icon: 'submenu_16',
    href: '/colombia/futbol-colombiano/santa-fe/',
    title: 'Independiente Santa Fe',
    name: 'SFE'
  },
  {
    icon: 'submenu_17',
    href: '/lat/tag/tolima',
    title: 'Corporación Club Deportes Tolima',
    name: 'TOL'
  },
  {
    icon: 'submenu_18',
    href: '/lat/tag/atletico-bucaramanga',
    title: 'Atlético Bucaramanga',
    name: 'BUC'
  },
  {
    icon: 'submenu_19',
    href: '/lat/tag/jaguares',
    title: 'Jaguares',
    name: 'JAG'
  },
  {
    icon: 'submenu_20',
    href: '/lat/tag/fortaleza',
    title: 'Cúcuta Deportivo',
    name: 'FOR'
  },
  ]
};