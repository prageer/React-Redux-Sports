/**
 * Archivo de traducción para Brasil.
 */
module.exports = {
  categoryId: 677,
  cultureShortcut: 'br',
  cultureHref: '/br/',
  cultures: [{
    href: '/br/',
    title: 'VAVEL Brasil',
    title_short: 'VAVEL Brasil',
    class: 'flag-br'
  },
  {
    href: '/pt/',
    title: 'VAVEL Portugal',
    title_short: 'Portugal',
    class: 'flag-pt'
  },
  {
    href: '/en/',
    title: 'VAVEL UK',
    title_short: 'English',
    class: 'flag-uk'
  },
  {
    href: '/en-us/',
    title: 'VAVEL USA',
    title_short: 'USA',
    class: 'flag-usa'
  },
  {
    href: '/es/',
    title: 'VAVEL Espanha',
    title_short: 'Espanha',
    class: 'flag-es'
  },
  {
    href: '/ar/',
    title: 'VAVEL Argentina',
    title_short: 'Argentina',
    class: 'flag-ar'
  },
  {
    href: '/mx/',
    title: 'VAVEL México',
    title_short: 'MX',
    class: 'flag-mx'
  },
  {
    href: '/lat/',
    title: 'VAVEL América latina',
    title_short: 'LAT',
    class: 'flag-lat'
  },
  {
    href: '/it/',
    title: 'VAVEL Itália',
    title_short: 'Itália',
    class: 'flag-it'
  },
  {
    href: '/fr/',
    title: 'VAVEL França',
    title_short: 'França',
    class: 'flag-fr'
  },
  {
    href: '/arb/',
    title: 'VAVEL Árabe',
    title_short: 'Árabe',
    class: 'flag-arb'
  },
  ],
  latest: 'últimas',
  trendingTopics: 'Temas do momento:',
  writeInVavel: 'Write in Vavel',
  writerPanel: 'Writer panel',
  connectWithFacebook: 'Connect with facebook',
  news: 'Notícias',
  homeTitle: 'VAVEL Brasil - The international newspaper',
  facebookFrameUrl: 'http://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FVAVELcom-Brasil%2F278222775632800%3Fref%3Dts%26fref%3Dts&width=300&height=62&colorscheme=light&show_faces=false&border_color&stream=false&header=false&appId=169320175188',
  locale: 'pt-br',
  css: '/public/assets/css/cultures/brasil.css',
  homeDescription: 'As últimas notícias esportivas e artigos analisados em profundidade e qualidade, fotos, vídeos, resultados e jogos ao vivo no VAVEL, um jornal pensando diferente.',
  homeKeywords: 'noticias, última hora, atualidades, nacional, Brasil, vavel, periodismo, futebol, tênis, cinema, sociedade, política, televisão, TV, música, arte, basquete, internacional, calcio, premier league, bundesliga, liga, la liga, imprensa esportiva, jornais, esportes, esporte, imprensa, opinião, moda, blogs, especiais, vídeos, fotos, áudios, gráficos, entrevistas',
  contact: 'fale conosco',
  facebookCommunityUrl: 'http://www.facebook.com/VAVELBrasil/',
  facebookLikeFrameUrl: 'http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FVAVELBrasil&amp;send=false&amp;layout=button_count&amp;width=140&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=169320175188',
  twitterButtonUrl: 'https://twitter.com/VAVEL_Brasil',
  twitterButtonText: 'Follow @VAVEL_Brasil',
  googlePlusCommunityUrl: 'https://plus.google.com/118411984539219238219',
  googlePlusLikeFrameUrl: '//plus.google.com/118411984539219238219',
  categoryTitle: '{categoryName} Notícias - VAVEL.com',
  categoryDescription: 'Últimas noticias, fotos e artigos sobre {categoryName} na VAVEL',
  categoryKeywords: '{categoryName},VAVEL Brasil - The international newspaper',
  articleTitle: '{title} - VAVEL.com',
  articleDescription: '{description}',
  articleKeywords: '{keywords}',
  taboolaUrl: '//cdn.taboola.com/libtrc/vaveleu-pt/loader.js',
  androidWidgetTitle: 'Notícias esportivas',
  androidWidgetAuthor: 'the VAVEL Brasil app',
  androidWidgetPrice: 'GRATIS',
  androidWidgetLanguage: 'portuguese',
  androidWidgetInAppStore: 'en App Store',
  androidWidgetInGooglePlay: 'en Google Play',
  facebookSdkCulture: 'pt_PT',
  twitterSite: '@VAVEL_Brasil',
  twitterLikeVia: 'VAVEL_Brasil',
  lang: 'pt',
  flag: 'br',
  goals: 'Placar',
  referee: 'ÁRBITRO',
  incidents: 'INCIDENCIAS',
  latestArticlesTaggedAs: 'Mais notícias de',
  latestArticlesTaggedNews: 'Notícias sobre {tag}',
  authorTitle: '{title} - VAVEL.com',
  authorDescriptionWithBio: 'The articles portfolio of {author_name}. {author_bio} in VAVEL platform.',
  authorDescriptionWithoutBio: 'The articles portfolio of {author_name} on the Author Zone in VAVEL platform.',
  authorKeywords: '{keywords}',
  authorInVavel: 'Autor na VAVEL',
  writer: 'Escritor',
  articles: 'Artigos',
  tagTitle: '{tag} - Notícias sobre {tag} na VAVEL Brasil',
  tagDescription: 'A mais recente sobre {tag}. Últimas noticias, fotos e artigos sobre {tag} na VAVEL Brasil.',
  tagKeywords: '{keywords}',
  homepage: 'Homepage',

  clubs: [{
    icon: 'submenu_1',
    href: '/br/tag/atletico-go',
    title: 'Atlético-GO',
    name: 'AGO'
  },
  {
    icon: 'submenu_2',
    href: '/br/futebol/atletico-mg/',
    title: 'Atlético-MG',
    name: 'AMG'
  },
  {
    icon: 'submenu_3',
    href: '/br/futebol/atletico-pr/',
    title: 'Atlético-PR',
    name: 'APR'
  },
  {
    icon: 'submenu_16',
    href: '/br/futebol/avai/',
    title: 'Avaí',
    name: 'AVA'
  },
  {
    icon: 'submenu_9',
    href: '/br/futebol/bahia/',
    title: 'Bahia',
    name: 'BAH'
  },
  {
    icon: 'submenu_4',
    href: '/br/futebol/botafogo/',
    title: 'Botafogo',
    name: 'BOT'
  },
  {
    icon: 'submenu_5',
    href: '/br/futebol/chapecoense/',
    title: 'Chapecoense',
    name: 'CHA'
  },
  {
    icon: 'submenu_6',
    href: '/br/futebol/corinthians/',
    title: 'Corinthians',
    name: 'COR'
  },
  {
    icon: 'submenu_7',
    href: '/br/futebol/coritiba/',
    title: 'Coritiba',
    name: 'CTB'
  },
  {
    icon: 'submenu_8',
    href: '/br/futebol/cruzeiro/',
    title: 'Cruzeiro',
    name: 'CRU'
  },
  {
    icon: 'submenu_10',
    href: '/br/futebol/flamengo/',
    title: 'Flamengo',
    name: 'FLA'
  },
  {
    icon: 'submenu_11',
    href: '/br/futebol/fluminense/',
    title: 'Fluminense',
    name: 'FLU'
  },
  {
    icon: 'submenu_12',
    href: '/br/futebol/gremio/',
    title: 'Gremio',
    name: 'GRE'
  },
  {
    icon: 'submenu_14',
    href: '/br/futebol/palmeiras/',
    title: 'Palmeiras',
    name: 'PAL'
  },
  {
    icon: 'submenu_15',
    href: '/br/futebol/ponte-preta/',
    title: 'Ponte Preta',
    name: 'PON'
  },
  {
    icon: 'submenu_17',
    href: '/br/futebol/santos/',
    title: 'Santos',
    name: 'SAN'
  },
  {
    icon: 'submenu_18',
    href: '/br/futebol/sao-paulo/',
    title: 'Sao Paulo',
    name: 'SPO'
  },
  {
    icon: 'submenu_19',
    href: '/br/futebol/sport/',
    title: 'Sport',
    name: 'SPT'
  },
  {
    icon: 'submenu_13',
    href: '/br/futebol/vasco/',
    title: 'Vasco da Gama',
    name: 'VAS'
  },
  {
    icon: 'submenu_20',
    href: '/br/futebol/vitoria/',
    title: 'Vitória',
    name: 'VIT'
  },
  ]
};