/**
 * Archivo de traducción para España.
 */
module.exports = {
  categoryId: 558,
  cultureShortcut: 'es',
  cultureHref: '/es/',
  cultures: [{
    href: '/es/',
    title: 'VAVEL España',
    title_short: 'VAVEL España',
    class: 'flag-es'
  },
  {
    href: '/en/',
    title: 'VAVEL UK',
    title_short: 'English',
    class: 'flag-uk'
  },
  {
    href: '/en-us/',
    title: 'VAVEL USA',
    title_short: 'USA',
    class: 'flag-usa'
  },
  {
    href: '/mx/',
    title: 'VAVEL México',
    title_short: 'México',
    class: 'flag-mx'
  },
  {
    href: '/ar/',
    title: 'VAVEL Argentina',
    title_short: 'ARG',
    class: 'flag-ar'
  },
  {
    href: '/lat/colombia/',
    title: 'VAVEL Colombia',
    title_short: 'Colombia',
    class: 'flag-co'
  },
  {
    href: '/lat/',
    title: 'VAVEL Latinoamerica',
    title_short: 'LAT',
    class: 'flag-lat'
  },
  {
    href: '/br/',
    title: 'VAVEL Brasil',
    title_short: 'Brasil',
    class: 'flag-br'
  },
  {
    href: '/pt/',
    title: 'VAVEL Portugal',
    title_short: 'PT',
    class: 'flag-pt'
  },
  {
    href: '/it/',
    title: 'VAVEL Italia',
    title_short: 'IT',
    class: 'flag-it'
  },
  {
    href: '/fr/',
    title: 'VAVEL France',
    title_short: 'France',
    class: 'flag-fr'
  },
  {
    href: '/arb/',
    title: 'VAVEL Arabic',
    title_short: 'ARB',
    class: 'flag-arb'
  },
  ],
  latest: 'lo último',
  trendingTopics: 'Temas del momento:',
  writeInVavel: 'Write in Vavel',
  writerPanel: 'Writer panel',
  connectWithFacebook: 'Connect with facebook',
  news: 'Noticias',
  homeTitle: 'VAVEL España - The international newspaper',
  facebookFrameUrl: 'http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fvavel.espana&width=300&colorscheme=light&show_faces=false&border_color&stream=false&header=true&height=62&appId=169320175188',
  locale: 'es-es',
  css: '',
  homeDescription: 'Las últimas noticias deportivas y artículos analizados en profundidad y calidad, fotos, vídeos, resultados y partidos en vivo minuto a minuto en VAVEL, un periódico pensando diferente.',
  homeKeywords: 'noticias, última hora, actualidad, nacional, España, Latinoamérica, México, Argentina, vavel, periodismo, fútbol, tenis, cine, sociedad, política, televisión, TV, música, arte, baloncesto, internacional, calcio, premier league, bundesliga, liga, la liga, prensa deportiva, periódicos, deportes, deporte, prensa, opinión, moda, blogs, firmas, especiales, vídeos, fotos, audios, gráficos, entrevistas',
  contact: 'contacto',
  facebookCommunityUrl: 'http://www.facebook.com/vavel.espana/',
  facebookLikeFrameUrl: 'http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FVAVEL.espana&amp;send=false&amp;layout=button_count&amp;width=160&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=169320175188',
  twitterButtonUrl: 'https://twitter.com/VAVELcom',
  twitterButtonText: 'Follow @VAVELcom',
  googlePlusCommunityUrl: 'https://plus.google.com/+vavel',
  googlePlusLikeFrameUrl: '//plus.google.com/+vavel',
  categoryTitle: '{categoryName} Noticias - VAVEL.com',
  categoryDescription: 'Noticias, fotos, actualidad, información y artículos de última hora sobre {categoryName} en VAVEL',
  categoryKeywords: '{categoryName},VAVEL España - The international newspaper',
  articleTitle: '{title} - VAVEL.com',
  articleDescription: '{description}',
  articleKeywords: '{keywords}',
  taboolaUrl: '//cdn.taboola.com/libtrc/vaveleu-es/loader.js',
  androidWidgetTitle: 'Noticias deportivas',
  androidWidgetAuthor: 'the VAVEL España app',
  androidWidgetPrice: 'GRATIS',
  androidWidgetLanguage: 'espanol',
  androidWidgetInAppStore: 'en App Store',
  androidWidgetInGooglePlay: 'en Google Play',
  facebookSdkCulture: 'es_ES',
  twitterSite: '@VAVELcom',
  twitterLikeVia: 'VAVELcom',
  lang: 'es',
  flag: 'es',
  goals: 'MARCADOR',
  referee: 'ÁRBITRO',
  incidents: 'INCIDENCIAS',
  latestArticlesTaggedAs: 'Más noticias sobre',
  latestArticlesTaggedNews: 'Las últimas noticias sobre {tag}',
  authorTitle: '{title} - VAVEL.com',
  authorDescriptionWithBio: 'The articles portfolio of {author_name}. {author_bio} in VAVEL platform.',
  authorDescriptionWithoutBio: 'The articles portfolio of {author_name} on the Author Zone in VAVEL platform.',
  authorKeywords: '{keywords}',
  authorInVavel: 'Autor en VAVEL',
  writer: 'Escritor',
  articles: 'Artículos',
  tagTitle: '{tag} - Las últimas noticias sobre {tag} en VAVEL España',
  tagDescription: 'Lo último sobre {tag}. Noticias, fotos, actualidad, información y artículos de última hora sobre {tag} en VAVEL España.',
  tagKeywords: '{keywords}',
  homepage: 'Homepage',

  clubs: [{
    icon: 'submenu_2',
    href: '/es/futbol/fc-barcelona/',
    title: 'FC Barcelona',
    name: 'FCB'
  },
  {
    icon: 'submenu_1',
    href: '/es/futbol/real-madrid/',
    title: 'Real Madrid',
    name: 'RMA'
  },
  {
    icon: 'submenu_5',
    href: '/es/futbol/atletico-de-madrid/',
    title: 'Atlético de Madrid',
    name: 'ATM'
  },
  {
    icon: 'submenu_18',
    href: '/es/futbol/villarreal/',
    title: 'Villarreal CF',
    name: 'VIL'
  },
  {
    icon: 'submenu_10',
    href: '/es/futbol/athletic-de-bilbao/',
    title: 'Athletic de Bilbao',
    name: 'ATH'
  },
  {
    icon: 'submenu_19',
    href: '/es/futbol/celta-de-vigo/',
    title: 'Celta de Vigo',
    name: 'CEL'
  },
  {
    icon: 'submenu_8',
    href: '/es/futbol/sevilla-fc/',
    title: 'Sevilla FC',
    name: 'SFC'
  },
  {
    icon: 'submenu_4',
    href: '/es/futbol/malaga/',
    title: 'Málaga CF',
    name: 'MAL'
  },
  {
    icon: 'submenu_12',
    href: '/es/futbol/real-sociedad/',
    title: 'Real Sociedad',
    name: 'RSO'
  },
  {
    icon: 'submenu_20',
    href: '/es/futbol/betis/',
    title: 'Betis',
    name: 'BET'
  },
  {
    icon: 'submenu_13',
    href: '/es/futbol/espanyol/',
    title: 'Espanyol',
    name: 'ESP'
  },
  {
    icon: 'submenu_3',
    href: '/es/futbol/valencia-cf/',
    title: 'Valencia CF',
    name: 'VCF'
  },
  {
    icon: 'submenu_7',
    href: '/es/futbol/ud-las-palmas/',
    title: 'UD Las Palmas',
    name: 'LPA'
  },
  {
    icon: 'submenu_9',
    href: '/es/futbol/eibar/',
    title: 'SD Eibar',
    name: 'EIB'
  },
  {
    icon: 'submenu_14',
    href: '/es/futbol/deportivo-de-la-coruna/',
    title: 'Deportivo de la Coruña',
    name: 'DEP'
  },
  {
    icon: 'submenu_17',
    href: '/es/futbol/granada-cf/',
    title: 'Granada CF',
    name: 'GCF'
  },
  {
    icon: 'submenu_16',
    href: '/es/futbol/sporting-de-gijon/',
    title: 'Sporting de Gijón',
    name: 'SPO'
  },
  {
    icon: 'submenu_15',
    href: '/es/futbol/alaves/',
    title: 'Alavés',
    name: 'ALA'
  },
  {
    icon: 'submenu_11',
    href: '/es/futbol/leganes/',
    title: 'CD Leganés',
    name: 'LEG'
  },
  {
    icon: 'submenu_6',
    href: '/es/futbol/osasuna/',
    title: 'Osasuna',
    name: 'OSA'
  },
  ]
};