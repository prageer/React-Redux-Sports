import { errorApp } from '../reducers/appReducer'

/**
 * Captura los errores en los actions
 * activando el error en el store.
 *
 * @param {} store
 */
export const errorViewMiddleware = store => next => action => {
  try {
    if (action.error) {
      next(errorApp(action.payload));
    } else {
      next(action);
    }
  } catch (err) {

  }
};