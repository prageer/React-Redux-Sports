import { createAction } from 'redux-actions';

import {
  TOGGLE_MENU,
  ADD_MENU,
  TOGGLE_MENU_ITEM,
  REQUEST_MENU,
  MENU_RECEIVED,
  ERROR_MENU_REQUEST
} from './actionTypes';


export const toggleMenu = createAction(TOGGLE_MENU);
export const addMenu = createAction(ADD_MENU);

export const requestMenu = createAction(REQUEST_MENU);
export const menuReceived = createAction(MENU_RECEIVED);
export const errorMenuRequest = createAction(ERROR_MENU_REQUEST);

/**
 * Load the menu app.
 *
 * @param {string} pathname Pathname to get menu.
 */
export const loadMenu = (pathname) => {
  return (dispatch, getState, api) => {
    dispatch(requestMenu());

    return api.getCategoriesMenu(pathname)
      .then((menu = []) => {
        const children = menu.length > 0 ?
          menu[0].children :
          [];

        dispatch(addMenu(children));
        dispatch(menuReceived());
      })
      .catch((err) => {
        console.warn(`Error on requestMenu, pathname='${pathname}'`);
        dispatch(errorMenuRequest(err));
      });
  };
};