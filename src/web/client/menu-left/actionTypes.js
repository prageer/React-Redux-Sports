export const ADD_MENU = 'ADD_MENU';
export const TOGGLE_MENU = 'TOGGLE_MENU';

export const REQUEST_MENU = 'REQUEST_MENU';
export const MENU_RECEIVED = 'MENU_RECEIVED';
export const ERROR_MENU_REQUEST = 'ERROR_MENU_REQUEST';