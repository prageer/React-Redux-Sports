import { fromJS, List as list } from 'immutable';

import {
  ADD_MENU,
  TOGGLE_MENU,
  REQUEST_MENU,
  MENU_RECEIVED,
  ERROR_MENU_REQUEST
} from './actionTypes';


const initialState = fromJS({
  isOpen: false,
  isFetching: false,
  items: []
});

export default (state = initialState, { type, payload }) => {
  switch(type) {
    case ADD_MENU:
      return state.set('items', list(fromJS(payload)));
      break;

    case TOGGLE_MENU:
      return state.set('isOpen', !state.get('isOpen'));
      break;

    case REQUEST_MENU:
      return state.set('isFetching', true);
      break;

    case MENU_RECEIVED:
      return state.set('isFetching', false);
      break;

    case ERROR_MENU_REQUEST:
      return state.set('isFetching', false);
      break;

    default:
      return state;
  }
};