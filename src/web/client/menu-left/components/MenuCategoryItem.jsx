import React, { Component } from 'react';

import MenuCategoryContainer from './MenuCategoryContainer';
import Link from '../../components/Link';


class MenuCategoryItem extends Component {

  onClick = (hasChildren) => {
    if (hasChildren) {
      this.refs.node.classList.toggle('expanded');
    } else {
      this.props.onClickWithoutChildren();
    }
  }

  render() {
    const { category } = this.props;
    const hasChildren = category.children.length > 0;

    return (
      <li ref="node">
        <Link
          to={hasChildren ? null : category.redirect || category.path}
          title={category.name}
          rel="nofollow"
          onClick={() => this.onClick(hasChildren)}
        >
          <span dangerouslySetInnerHTML={{ __html: category.alias || category.name }} />
          {hasChildren && <i className="triangle" />}
        </Link>

        {hasChildren &&
        <MenuCategoryContainer
          onClickWithoutChildren={this.onClick}
          items={category.children}
        />}
      </li>
    );
  }
}

export default MenuCategoryItem;