import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';

import MenuClubs from './MenuClubs';
import { toggleMenu } from '../actions';
import MenuCategoryContainer from './MenuCategoryContainer';


const LeftAside = ({ clubs, items, isOpen, toggleMenu }) => {
  const classname = classNames('new-sidemenu', {
    active: isOpen
  });

  return (
    <div>
      {
        isOpen ?
        <div className="modal-fullscreen" onClick={toggleMenu} /> :
        null
      }
      <div className={classname}>
        <MenuClubs clubs={clubs.toJS()} onChildClicked={toggleMenu} />

        <div className="divider" />

        <nav>
          <MenuCategoryContainer
            onClickWithoutChildren={toggleMenu}
            items={items.toJS()}
          />
        </nav>
      </div>
    </div>
  );
};

const mapStateToProps = (s) => {
  const state = s.get('menu');

  return {
    isOpen: state.get('isOpen'),
    clubs: s.getIn(['app', 'edition', 'clubs']),
    items: state.get('items')
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggleMenu() {
      dispatch(toggleMenu());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LeftAside);