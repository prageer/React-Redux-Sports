import React from 'react';
import PropTypes from 'prop-types';

import MenuCategoryItem from './MenuCategoryItem';


const MenuCategoryContainer = ({ items: categories, onClickWithoutChildren }) => {
  return (
    <ul className="new-sidemenu--categories">
      {
        categories.map((category, index) =>
          <MenuCategoryItem
            onClickWithoutChildren={onClickWithoutChildren}
            category={category} key={index}
          />)
      }
    </ul>
  );
};

MenuCategoryContainer.propTypes = {
  items: PropTypes.array.isRequired,
  onClickWithoutChildren: PropTypes.func.isRequired
};

export default MenuCategoryContainer;