import React from 'react';

import { ClubItem } from '../../header';


export const MenuClubs = ({ clubs, onChildClicked }) => (
  <nav className="new-sidemenu--clubs">
    <ul>
      {
        clubs.map((club, index) =>
          <ClubItem
            onChildClicked={onChildClicked}
            {...club}
            key={index}
          />
          )
      }
    </ul>
  </nav>
);

export default MenuClubs;