import React from 'react';

import ClubItem from './ClubItem';


const ClubsBar = ({ clubs }) => {
  return (
    <nav className="submenu_vavel">
      <div className="submenu_liquid">
        <ul id="submenu_holder">
          {
            clubs.map((club, index) => <ClubItem {...club} key={index} />)
          }
        </ul>
      </div>
    </nav>
  );
}

export default ClubsBar;