import React, { Component } from 'react';

import Link from '../../components/Link';


class ClubItem extends Component {

  onClick = () => {
    if (this.props.onChildClicked) {
      this.props.onChildClicked();
    }
  }

  render() {
    const { icon, href, title, name } = this.props;

    return (
      <li className={icon}>
        <Link
          onClick={this.onClick}
          to={href}
          title={title}
          dangerouslySetInnerHTML={{ __html: name }}
          className="submenu_holder"
        />
      </li>
    );
  }
}

export default ClubItem;