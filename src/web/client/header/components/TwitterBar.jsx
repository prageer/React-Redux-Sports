import React from 'react';

import { getImgUrl, getCategoryUrl } from '../../utils';


const TwitterBar = ({ category, edition }) => {
  const { twitterButtonText, twitterButtonUrl, news } = edition;

  return (
    <div className="box-white-cat">
      <div className="submenu_liquid">
        {
          !category.image ?
            null :
            <img
              src={getImgUrl([
                '1',
                category.image,
              ])}
              alt="Twitter"
              style={{ maxWidth: 40, maxHeight: 40, float: 'left', marginRight: 15 }}
            />
        }
        <h2 className="category_title_bar">
          <a href={getCategoryUrl(category)} title={category.name + 'hola1'} itemProp="url">
            <span itemProp="title">{category.name}</span>
          </a>
        </h2>
        <span className="category_title_bar">{news}</span>
        <div className="tw_button_bar">
          <a href={twitterButtonUrl} className="twitter-follow-button">{twitterButtonText}</a>
        </div>
      </div>
    </div>
  );
};

export default TwitterBar;