import React, { Component } from 'react';
import { connect } from 'react-redux';

import ClubsBar from './ClubsBar';
import Link from '../../components/Link';
import { toggleMenu } from '../../menu-left';
import LocalesBar from '../../components/LocalesBar';


class Header extends Component {
  render() {
    const edition = this.props.edition.toJS();

    return (
      <header>
        <div id="menuholder_top">
          <div className="menu_vavel menu_vavel_fixed">
            <div className="sb-toggle-left" onClick={this.props.toggleMenu}>
              <div className="navicon-line" />
              <div className="navicon-line" />
              <div className="navicon-line" />
            </div>
            <div className="menu-background">
              <div className="logo-vavel-menu mgr10">
                <Link to="/">
                  <img src="/assets/img/vavel_logo_png.png" alt="VAVEL logo" title="VAVEL logo" />
                </Link>
              </div>
              {/*<SearchBox />*/}
              <div className="sb-toggle-right">
                <LocalesBar edition={edition} />
              </div>
            </div>
          </div>

        </div>
        <ClubsBar clubs={edition.clubs} />
      </header>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    edition: state.get('app').get('edition')
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    toggleMenu() {
      dispatch(toggleMenu());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);