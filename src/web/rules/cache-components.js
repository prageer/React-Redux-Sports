export default {
  components: {
    "CacheableGridArticle": {
      strategy: 'template',
      enable: true
    },
    "LeftAside": {
      strategy: "simple",
      enable: true
    },
    "Header": {
      strategy: "simple",
      enable: true
    }
  }
};