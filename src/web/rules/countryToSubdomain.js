/**
 * Subdominio por defecto. Los valores posibles
 * son las llaves presentes en `conversionTable`.
 */
export const DEFAULT_SUBDOMAIN = 'es';

/**
 * Tabla de conversión, define la relación que hay
 * entre un lenguaje y el subdominio para la app.
 */
const conversionTable = {
  es: 'es',
  gb: 'en', // Reino unido
  en: 'en',
  mx: 'mx',
  us: 'us',
  ar: 'ar',
  lat: 'lat',
  co: 'lat',
  ve: 'lat',
  ec: 'lat',
  br: 'br',
  pt: 'pt',
  it: 'it',
  fr: 'fr',
  de: 'de', // Alemania
  ru: 'ru'
};

/**
 * Obtiene un subdominio para la aplicación dado un lenguaje.
 *
 * @param {String} lang Lenguaje para obtener subdominio.
 * @returns {String} subdominio. Si el lenguaje no está
 * en la tabla, se devolverá el subdominio por defecto.
 * @see `DEFAULT_SUBDOMAIN`.
 */
export const getSubdomain = (lang = '') => {
  if (!lang) return getSubdomain(DEFAULT_SUBDOMAIN);

  try {
    return Reflect.get(conversionTable, lang.toLowerCase()) ||
      getSubdomain(DEFAULT_SUBDOMAIN);
  } catch (err) {
    return getSubdomain(DEFAULT_SUBDOMAIN);
  }
};