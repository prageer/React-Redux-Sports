export const WITH_SUBDOMAIN = process.env.WITH_SUBDOMAIN === 'true';

/**
 * Mount point for react app.
 * Represents a HTML identifier.
 */
export const MOUNT_POINT = 'root';